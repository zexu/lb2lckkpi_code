void massFit(){

  using namespace RooFit;
  
  // Define range
  const Double_t MassMin = 5550 ;
  const Double_t MassMax = 5690 ;

  //...mass resolution
  const Double_t MsigmaAve = 10. ;
  const Double_t MsigmaMin =  1. ;
  const Double_t MsigmaMax = 30. ;

  //====================================================================
  // Input root files, Use TChain, easy to merge files, like MagUp and MagDown  
  //====================================================================
  TChain *chain = new TChain("DecayTree");
  chain->Add("/projects/lhcb/users/xuzh/PentaQuark/Lb2Lc/RootFiles/root_file_2012/Lb2LcKKPi/data/Lb2LcKKPi_pre_clone_mul_tmva.root");
  // chain->Add("...");

  //====================================================================
  // Apply cuts first 
  //====================================================================
  TCut totCut = "BDTG>-0.26 && Lb_Con_Mass<5690 && Lb_Con_Mass>5550";
 
  TTree *selTree = chain->CopyTree(totCut);

  cout << "[INFO] Total candidates after selection: " << selTree->GetEntries() << endl;


  //====================================================================
  // RooDataSet  
  //====================================================================
  // Note, Var name should be the same as the branch name in TTree 
  RooRealVar *m = new RooRealVar("Lb_Con_Mass","M(#Lambda_{c}^{+}K^{+}K^{-}#pi^{-})", MassMin,MassMax, "MeV/#it{c}^{2}") ;
  RooRealVar *LcLbKp_m = new RooRealVar("LcLbKp_M","Mass of Lcpi",0,10000,"MeV") ;
  RooRealVar *LcLbKm_m = new RooRealVar("LcLbKm_M","Mass of ppbar",0,10000,"MeV") ;
  RooRealVar *LcLbPi_m = new RooRealVar("LcLbPi_M","Mass of ppi",0,10000,"MeV"); 
  RooRealVar *LcLbKpKm_m = new RooRealVar("LcLbKpKm_M","Mass of Lcpbar",0,10000,"MeV"); 
  RooRealVar *LcLbKpPi_m = new RooRealVar("LcLbKpPi_M","Mass of ppiLc",0,10000,"MeV"); 
  RooRealVar *LcLbKmPi_m = new RooRealVar("LcLbKmPi_M","Mass of ppiLc",0,10000,"MeV"); 
  RooRealVar *BDTG = new RooRealVar("BDTG","Decision tree",-10,10,""); 
  
 // RooDataSet *Data = new RooDataSet("Data", "", selTree, RooArgSet(*m,*LcLbKp_m,*LcLbKm_m,*LcLbPi_m,*LcLbKpKm_m,*LcLbKpPi_m,*LcLbKmPi_m),"BDTG>-0.4" ) ;
  RooDataSet *Data = new RooDataSet("Data", "", selTree, RooArgSet(*m,*LcLbKp_m,*LcLbKm_m,*LcLbPi_m,*LcLbKpKm_m,*LcLbKpPi_m,*LcLbKmPi_m,*BDTG) ) ;
  Data->Print();


  //=================
  // Define regions 
  //=================


  RooRealVar *SigM= new RooRealVar("M(Lb)","",5.61799e+03, 5300, 6000,"MeV/#it{c}^{2}");
  RooRealVar *SigMRes  = new RooRealVar("#sigma_{M}",    "", 1.56143e+01, 5, 20, "MeV/#it{c}^{2}");
  RooRealVar *SigMRes2 = new RooRealVar("#sigma_{M,2}",  "",  1.47119e+01, 6, 50, "MeV/#it{c}^{2}");
  
  RooRealVar n("n","",0.86661e+00);

  RooRealVar a1("a1","",2.0359e+00,"");
  RooRealVar a2("a2","",-2.01187e+00,"");

//  RooFormulaVar a1("a1","2.066+0.0085*@0-0.00011*@0*@0",RooArgSet(*SigMRes));
//  RooFormulaVar a2("a2","2.066+0.0085*@0-0.00011*@0*@0",RooArgSet(*SigMRes2));

  RooCBShape SigMPdf1("CB1","",*m,*SigM,*SigMRes ,a1, n);
  RooCBShape SigMPdf2("CB2","",*m,*SigM,*SigMRes2,a2, n);
  
  RooRealVar CB_fr("CB_fr","",0.5,0.2,0.8);
  RooAddPdf *SigMPdf = new RooAddPdf("SigMPdf","SigMPdf", RooArgList(SigMPdf1,SigMPdf2),RooArgList(CB_fr));
  RooRealVar *p0=new RooRealVar("p0","p0",-0.,-0.5,0.5,"MeV/c^{2}");
//combination bkg
  RooAbsPdf *comb_BkgMPdf=new RooExponential("comb_BkgMPdf","combine BkgMPdf",*m,*p0);
  RooRealVar *nSig = new RooRealVar("N_{Sig}","nsig",Data->numEntries()*0.4,0,Data->numEntries());
  RooRealVar *nBkg_comb = new RooRealVar("N_{Bkg_comb}","nbkg combination",Data->numEntries()*0.6,0,Data->numEntries());
//  RooRealVar *nBkg_LcK = new RooRealVar("N_{Bkg_LcK}","nbkg combination",6.24231e+03,0,Data->numEntries());  
//  RooRealVar *nBkg_Lcrho = new RooRealVar("N_{Bkg_Lcrho}","nbkg combination",5.3640e+04,0,Data->numEntries());  
  
  RooAbsPdf *MassPdf=new RooAddPdf("MassPdf","mass_pdf",RooArgList(*SigMPdf,*comb_BkgMPdf),RooArgList(*nSig,*nBkg_comb));
//No LcK reflection
//  RooAbsPdf *MassPdf=new RooAddPdf("MassPdf","mass_pdf",RooArgList(*SigMPdf,*comb_BkgMPdf,*arg_bg),RooArgList(*nSig,*nBkg_comb,*nBkg_Lcrho));

  RooFitResult* fitres = MassPdf->fitTo(*Data,NumCPU(20));
  RooArgSet* params = MassPdf->getVariables();
  params->writeToFile("result.txt");


//Draw~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  TCanvas *MyCan = new TCanvas("MyCan","",900,600);
  MyCan->SetFillColor(kWhite);
  MyCan->Divide(1,2,0,0,0);
  
  MyCan->cd(2);
  gPad->SetTopMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetPad(0.02,0.02,0.98,0.77);
  RooPlot *mframe = m->frame(Bins(40));
  Data->plotOn(mframe, Name("myData"));
  MassPdf->plotOn(mframe, Name("mySig"),  Components(*SigMPdf), LineColor(kRed) );
//  MassPdf->plotOn(mframe, Name("myBkg_comb"), Components(*comb_BkgMPdf), LineColor(kDark), LineStyle(kDashed) );
  MassPdf->plotOn(mframe, Name("myBkg_comb"), Components(*comb_BkgMPdf), LineStyle(kDashed) );
  MassPdf->plotOn(mframe, Name("myTot") );
  mframe->Draw();
  TString fileName = "Lb_LcPPPi";
  TString epsName = fileName + ".eps";
  TString pngName = fileName + ".png";
  TString pdfName = fileName + ".pdf";
  TString CName = fileName + ".C";
//  MyCan->Print( epsName );
//  MyCan->Print( pngName );
//  MyCan->Print( pdfName );
//  MyCan->Print( CName );


  MyCan->cd(1);
  gPad->SetBottomMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetPad(0.02,0.76,0.98,0.97);
  RooHist *pull_hist = mframe->pullHist();
  pull_hist->Draw();
//  TString fileName_pull = "Lb_LcPPPi_pull";
//  TString epsName_pull = fileName_pull + ".eps";
//  TString pngName_pull = fileName_pull + ".png";
//  TString pdfName_pull = fileName_pull + ".pdf";
//  TString CName_pull = fileName_pull + ".C";
//  MyCan_pull->Print( epsName_pull );
//  MyCan_pull->Print( pngName_pull );
//  MyCan_pull->Print( pdfName_pull );
//  MyCan_pull->Print( CName_pull );
  MyCan->Print( pdfName );



  TCanvas *MyCan_log = new TCanvas("MyCan_log","",600,400);
  gPad->SetLogy(); 
  mframe->Draw();
  TString fileName_log = "Lb_LcPPPi_log";
  TString epsName_log = fileName_log + ".eps";
  TString pngName_log = fileName_log + ".png";
  TString pdfName_log = fileName_log + ".pdf";
  TString CName_log = fileName_log + ".C";
//  MyCan_log->Print( epsName_log );
//  MyCan_log->Print( pngName_log );
  MyCan_log->Print( pdfName_log );
//  MyCan_log->Print( CName_log );




#if 1
//create a new RooDataSet with sweight~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   RooDataSet *t_data = new RooDataSet(Data->GetName(),Data->GetTitle(),Data,*Data->get());
   RooStats::SPlot *sdata = new RooStats::SPlot("sdata","An SPlot",*t_data,MassPdf,RooArgList(*nSig,*nBkg_comb));
   //TFile f("/projects/lhcb/users/xuzh/PentaQuark/Lb2Lc/RootFiles/root_file_2012/Lb2LcKKPi/data/data_LcKKPi_2012_clone_tmva_weight.root","recreate");
  

   
   TFile f("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/root_file_2012/Lb2LcKKPi/data/Lb2LcKKPi_pre_clone_mul_tmva_weight.root","recreate");
   //TTree *SweightData = new TTree("SweightData","Data tree with sweight information");

   TTree *newtree = selTree->CloneTree(0);
   Float_t Sweight_sig;	newtree->Branch("Sweight_sig",&Sweight_sig,"Sweight_sig/F");
   Float_t Sweight_bkg;	newtree->Branch("Sweight_bkg",&Sweight_bkg,"Sweight_bkg/F");

#if 0
   Double_t Lb_Mass,LcPi_Mass, LcKp_Mass,LcKm_Mass,LcKpKm_Mass,LcKpPi_Mass,LcKmPi_Mass;
   Double_t bDTG;
   Float_t Sweight_sig, Sweight_bkg;
   Int_t evtnum, i;
   SweightData->Branch("Lb_Mass",&Lb_Mass,"Lb_Mass/D");
   SweightData->Branch("LcPi_Mass",&LcPi_Mass,"LcPi_Mass/D");
   SweightData->Branch("LcKp_Mass",&LcKp_Mass,"LcKp_Mass/D");
   SweightData->Branch("LcKm_Mass",&LcKm_Mass,"LcKm_Mass/D");
   SweightData->Branch("LcKpKm_Mass",&LcKpKm_Mass,"LcKpKm_Mass/D");
   SweightData->Branch("LcKpPi_Mass",&LcKpPi_Mass,"LcKpPi_Mass/D");
   SweightData->Branch("LcKmPi_Mass",&LcKmPi_Mass,"LcKmPi_Mass/D");
   SweightData->Branch("Sweight_sig",&Sweight_sig,"Sweight_sig/F");
   SweightData->Branch("Sweight_bkg",&Sweight_bkg,"Sweight_bkg/F");
   SweightData->Branch("bDTG",&bDTG,"bDTG/D");
#endif
  
   t_data->Print();
   evtnum = t_data->numEntries();
   cout<< "The total number of events: "<< evtnum << endl;
#if 0   
   RooRealVar* Lb_mass = (RooRealVar*)t_data->get()->find("Lb_Con_Mass");
   RooRealVar* LcPi_mass = (RooRealVar*)t_data->get()->find("LcLbPi_M");
   RooRealVar* LcKp_mass = (RooRealVar*)t_data->get()->find("LcLbKp_M");
   RooRealVar* LcKm_mass = (RooRealVar*)t_data->get()->find("LcLbKm_M");
   RooRealVar* LcKpKm_mass = (RooRealVar*)t_data->get()->find("LcLbKpKm_M");
   RooRealVar* LcKpPi_mass = (RooRealVar*)t_data->get()->find("LcLbKpPi_M");
   RooRealVar* LcKmPi_mass = (RooRealVar*)t_data->get()->find("LcLbKmPi_M");
#endif

   RooRealVar* sweight_sig = (RooRealVar*)t_data->get()->find("N_{Sig}_sw");
   RooRealVar* sweight_bkg = (RooRealVar*)t_data->get()->find("N_{Bkg_comb}_sw");

   for(i=0;i<evtnum;i++)
     {
       t_data->get(i);
	 selTree->GetEntry(i);
#if 0
//	 cout<<"Mass:"<<LcKpPi_mass->getVal()<<endl;
       Lb_Mass = Lb_mass->getVal();
	 LcPi_Mass = LcPi_mass->getVal();
	 LcKp_Mass = LcKp_mass->getVal();
       LcKm_Mass = LcKm_mass->getVal();
       LcKpKm_Mass = LcKpKm_mass->getVal();
       LcKpPi_Mass = LcKpPi_mass->getVal();
	 LcKmPi_Mass = LcKmPi_mass->getVal();
	 bDTG = BDTG->getVal();
#endif

       Sweight_sig = sweight_sig->getVal();
       Sweight_bkg = sweight_bkg->getVal();

	 newtree->Fill();
 //      SweightData->Fill();

     }
     newtree->Write();
     f.Close();    
#endif
 




}
