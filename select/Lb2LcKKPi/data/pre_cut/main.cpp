#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "DecayTree.h"
#include <vector>
#include <map>

using namespace std;

int main(int argc, char **argv) {
//chain the root files 
	TChain *chain = new TChain("DecayTree");
#if 0
	for(int n_root = 2; n_root < argc; n_root++){
		TString rootfile = "";
		rootfile = argv[n_root];
		for(int i =0; i < 200 ; i++){
			if(!access(Form( rootfile+"tuple_%i.root" ,i) ,0)){
				cout << Form( rootfile+"tuple_%i.root" ,i) << endl;
			      TFile *tmp_file = TFile::Open(Form( rootfile+"tuple_%i.root" ,i));
				if(tmp_file->GetNkeys() == 2){	
					chain->Add(Form( rootfile+"tuple_%i.root",i));
				}
			}
		}
	}
#endif
	chain->Add(argv[2]);


//	chain->Add("/home/xuzh/work/data/Lb2Lc/run1/data_2011/47/tuple_0.root");


//define the object
	DecayTree t(chain);

#if 1
//create a new file and clone a tree, but do not copy entries.
	Long64_t nentries = chain->GetEntries();
	cout<< "The Entries are : " << nentries <<endl;		
	TString filename = argv[1];
	TFile *file = new TFile( Form(filename),"recreate");	
	//TFile *file = new TFile( "test2.root","recreate");	
	TTree *newtree = chain->CloneTree(0);
#endif

#if 1
//AddBranch Varies
	
	//The Mass of Lanbda_c and K+
	Double_t LcLbKp_M;		newtree->Branch("LcLbKp_M",&LcLbKp_M,"LcLbKp_M/D");
	Double_t LcLbKp_M_con;		newtree->Branch("LcLbKp_M_con",&LcLbKp_M_con,"LcLbKp_M_con/D");
	//The Mass of Lanbda_c and K-
	Double_t LcLbKm_M;		newtree->Branch("LcLbKm_M",&LcLbKm_M,"LcLbKm_M/D");
	Double_t LcLbKm_M_con;		newtree->Branch("LcLbKm_M_con",&LcLbKm_M_con,"LcLbKm_M_con/D");
	//The Mass of Lanbda_c and pi-
	Double_t LcLbPi_M;		newtree->Branch("LcLbPi_M",&LcLbPi_M,"LcLbPi_M/D");
	Double_t LcLbPi_M_con;		newtree->Branch("LcLbPi_M_con",&LcLbPi_M_con,"LcLbPi_M_con/D");
	//The Mass of Lanbda_c and K+,K-
	Double_t LcLbKpKm_M;		newtree->Branch("LcLbKpKm_M",&LcLbKpKm_M,"LcLbKpKm_M/D");
	Double_t LcLbKpKm_M_con;	newtree->Branch("LcLbKpKm_M_con",&LcLbKpKm_M_con,"LcLbKpKm_M_con/D");
	//The Mass of Lanbda_c and K+,pi-
	Double_t LcLbKpPi_M;		newtree->Branch("LcLbKpPi_M",&LcLbKpPi_M,"LcLbKpPi_M/D");
	Double_t LcLbKpPi_M_con;	newtree->Branch("LcLbKpPi_M_con",&LcLbKpPi_M_con,"LcLbKpPi_M_con/D");
	//The Mass of Lanbda_c and K-,pi-
	Double_t LcLbKmPi_M;		newtree->Branch("LcLbKmPi_M",&LcLbKmPi_M,"LcLbKmPi_M/D");
	Double_t LcLbKmPi_M_con;	newtree->Branch("LcLbKmPi_M_con",&LcLbKmPi_M_con,"LcLbKmPi_M_con/D");
  	//test
	Double_t test_Lb_M;		newtree->Branch("test_Lb_M",&test_Lb_M,"test_Lb_M/D");

	Double_t LbKpKm_M;		newtree->Branch("LbKpKm_M",&LbKpKm_M,"LbKpKm_M/D");
	Double_t LbKpPi_M;		newtree->Branch("LbKpPi_M",&LbKpPi_M,"LbKpPi_M/D");
	Double_t LbKmPi_M;		newtree->Branch("LbKmPi_M",&LbKmPi_M,"LbKmPi_M/D");
	Double_t LbKpKmPi_M;		newtree->Branch("LbKpKmPi_M",&LbKpKmPi_M,"LbKpKmPi_M/D");

	Double_t LbKpKm_M_no;		newtree->Branch("LbKpKm_M_no",&LbKpKm_M_no,"LbKpKm_M_no/D");
	Double_t LbKpPi_M_no;		newtree->Branch("LbKpPi_M_no",&LbKpPi_M_no,"LbKpPi_M_no/D");
	Double_t LbKmPi_M_no;		newtree->Branch("LbKmPi_M_no",&LbKmPi_M_no,"LbKmPi_M_no/D");
	Double_t LbKpKmPi_M_no;		newtree->Branch("LbKpKmPi_M_no",&LbKpKmPi_M_no,"LbKpKmPi_M_no/D");

	//distance of Lb and Lc in z direction
	Double_t Distance_ZZ;		newtree->Branch("Distance_ZZ",&Distance_ZZ,"Distance_ZZ/D");

	//The Wrong Distinguish
	Double_t miss_LcM2K;		newtree->Branch("miss_LcM2K",&miss_LcM2K,"miss_LcM2K/D");
	Double_t miss_LcM2Pi;		newtree->Branch("miss_LcM2Pi",&miss_LcM2Pi,"miss_LcM2Pi/D");
	Double_t miss_LbM2K;		newtree->Branch("miss_LbM2K",&miss_LbM2K,"miss_LbM2K/D");
	Double_t miss_LbM2Pi;		newtree->Branch("miss_LbM2Pi",&miss_LbM2Pi,"miss_LbM2Pi/D");

	Double_t LbM_Kp2Pi;		newtree->Branch("LbM_Kp2Pi",&LbM_Kp2Pi,"LbM_Kp2Pi/D");
	Double_t LbM_Km2Pi;		newtree->Branch("LbM_Km2Pi",&LbM_Km2Pi,"LbM_Km2Pi/D");
	Double_t LbM_Pi2K;		newtree->Branch("LbM_Pi2K",&LbM_Pi2K,"LbM_Pi2K/D");

	Double_t D_star_Kp2Pi;		newtree->Branch("D_star_Kp2Pi",&D_star_Kp2Pi,"D_star_Kp2Pi/D");
	Double_t D_star_Km2Pi;		newtree->Branch("D_star_Km2Pi",&D_star_Km2Pi,"D_star_Km2Pi/D");

	//the daughter information
	Double_t Lc_Dau_Min_PT;		newtree->Branch("Lc_Dau_Min_PT",&Lc_Dau_Min_PT,"Lc_Dau_Min_PT/D");
	Double_t Lc_Dau_Min_IPCHI;	newtree->Branch("Lc_Dau_Min_IPCHI",&Lc_Dau_Min_IPCHI,"Lc_Dau_Min_IPCHI/D");

	//for tmva
	Double_t min_pt_K;		newtree->Branch("min_pt_K",&min_pt_K,"min_pt_K/D");
	Double_t min_IP_K;		newtree->Branch("min_IP_K",&min_IP_K,"min_IP_K/D");

	//angle between final particles
	Double_t angle_LcP_LcPi;	newtree->Branch("angle_LcP_LcPi",&angle_LcP_LcPi,"angle_LcP_LcPi/D");
	Double_t angle_LcP_LbKp;	newtree->Branch("angle_LcP_LbKp",&angle_LcP_LbKp,"angle_LcP_LbKp/D");
	Double_t angle_LcPi_LbKp;	newtree->Branch("angle_LcPi_LbKp",&angle_LcPi_LbKp,"angle_LcPi_LcbKp/D");
	
	Double_t angle_LcK_LbKm;	newtree->Branch("angle_LcK_LbKm",&angle_LcK_LbKm,"angle_LcK_LbKm/D");
	Double_t angle_LcK_LbPi;	newtree->Branch("angle_LcK_LbPi",&angle_LcK_LbPi,"angle_LcK_LbPi/D");
	Double_t angle_LbKm_LbPi;	newtree->Branch("angle_LbKm_LbPi",&angle_LbKm_LbPi,"angle_LbKm_LbPi/D");


	Double_t Lb_y;                newtree->Branch("Lb_y",&Lb_y,"Lb_y/D");
	Double_t Lb_ETA;              newtree->Branch("Lb_ETA",&Lb_ETA,"Lb_ETA/D");
	Double_t Lc_ETA;              newtree->Branch("Lc_ETA",&Lc_ETA,"Lc_ETA/D");
	Double_t LcP_ETA;             newtree->Branch("LcP_ETA",&LcP_ETA,"LcP_ETA/D");
	Double_t LcK_ETA;             newtree->Branch("LcK_ETA",&LcK_ETA,"LcK_ETA/D");
	Double_t LcPi_ETA;            newtree->Branch("LcPi_ETA",&LcPi_ETA,"LcPi_ETA/D");
	Double_t LbPi_ETA;            newtree->Branch("LbPi_ETA",&LbPi_ETA,"LbPi_ETA/D");
	Double_t LbKp_ETA;            newtree->Branch("LbKp_ETA",&LbKp_ETA,"LbKp_ETA/D");
	Double_t LbKm_ETA;            newtree->Branch("LbKm_ETA",&LbKm_ETA,"LbKm_ETA/D");
	
	// for dalize weight
	Double_t mkp;		newtree->Branch("mkp",&mkp,"mkp/D");
	Double_t mkpi;		newtree->Branch("mkpi",&mkpi,"mkpi/D");

	// for poli weight
	Double_t cosTheta;	newtree->Branch("cosTheta",&cosTheta,"cosTheta/D");
	Double_t phi;		newtree->Branch("phi",&phi,"phi/D");
	Double_t cosTheta_pk;	newtree->Branch("cosTheta_pk",&cosTheta_pk,"cosTheta_pk/D");
	Double_t phi_pk;		newtree->Branch("phi_pk",&phi_pk,"phi_pk/D");


#endif


#if 1
	//for (Long64_t jentry=0; jentry<nentries; jentry++){
	for (Long64_t jentry=0; jentry<nentries; jentry++){

		t.LoadTree(jentry);
		chain->GetEntry(jentry);
		if (jentry%1000==0) cout << "jentry = " << jentry << endl;

		bool judge = true;
        	judge = (t.LcP_P > 10000)&&
                	(t.Lc_MM > 2286.46-15 && t.Lc_MM < 2286.46+15)&&
                	(t.Lb_ENDVERTEX_CHI2 < 50)&&
                	(t.Lb_FDCHI2_OWNPV > 64)&&
                	(t.Lb_IPCHI2_OWNPV < 20)&&
                	(t.Lb_DIRA_OWNPV > 0.99994)&&
                	(t.Lc_ENDVERTEX_Z - t.Lb_ENDVERTEX_Z > -1)&&
                	(t.LcP_ProbNNp   > 0.1 )&&
			(t.LcP_PIDp > -10 )&&
                	(t.LcK_ProbNNk   > 0.1 )&&
			(t.LcK_PIDK > -10 ) && 
                	(t.LcPi_PIDK < 20 )&&
                	(t.LbKp_PIDK  > -2 )&&
                	(t.LbKm_PIDK  > -2 )&&
                	(t.LbPi_PIDK  < 10 );

		
		if(judge == 0)	{continue;}

		//Lb_y = 0.5*TMath::Log((t.Lb_PE+t.Lb_P)/(t.Lb_PE-t.Lb_P));
		Lb_y = 0.5*TMath::Log((t.Lb_PE+t.Lb_PZ)/(t.Lb_PE-t.Lb_PZ));

// Fill the Mass
		TLorentzVector v_Lc   (t.Lc_PX,t.Lc_PY,t.Lc_PZ,t.Lc_PE);
		TLorentzVector v_LbKp (t.LbKp_PX,t.LbKp_PY,t.LbKp_PZ,t.LbKp_PE);
		TLorentzVector v_LbKm (t.LbKm_PX,t.LbKm_PY,t.LbKm_PZ,t.LbKm_PE);
		TLorentzVector v_LbPi (t.LbPi_PX,t.LbPi_PY,t.LbPi_PZ,t.LbPi_PE);
		
		TLorentzVector v_LcLbKp = v_Lc+v_LbKp;
		TLorentzVector v_LcLbKm = v_Lc+v_LbKm;
		TLorentzVector v_LcLbPi = v_Lc+v_LbPi;
		TLorentzVector v_LcLbKpKm = v_Lc+v_LbKp+v_LbKm;
		TLorentzVector v_LcLbKpPi = v_Lc+v_LbKp+v_LbPi;
		TLorentzVector v_LcLbKmPi = v_Lc+v_LbKm+v_LbPi;
		//TLorentzVector v_Lb = v_Lc+v_LbKm+v_LbPi+v_LbKp;
		

		LcLbKp_M   =   v_LcLbKp.M();
		LcLbKm_M   =   v_LcLbKm.M();
		LcLbPi_M   =   v_LcLbPi.M();
		LcLbKpKm_M =   v_LcLbKpKm.M();
		LcLbKpPi_M =   v_LcLbKpPi.M();
		LcLbKmPi_M =   v_LcLbKmPi.M();
		//test_Lb_M = v_Lb.M();

		LbKpKm_M_no = (v_LbKp+v_LbKm).M();
		LbKpPi_M_no = (v_LbKp+v_LbPi).M();
		LbKmPi_M_no = (v_LbKm+v_LbPi).M();	
		LbKpKmPi_M_no = (v_LbKp+v_LbKm+v_LbPi).M();

#if 1
		//////////////////////////////////////////////////////////////////////
		TLorentzVector v_Lc_con   (t.Lc_PX,t.Lc_PY,t.Lc_PZ,t.Lc_PE);
		TLorentzVector v_LbKp_con (t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE[0]);
		TLorentzVector v_LbKm_con (t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE[0]);
		TLorentzVector v_LbPi_con (t.Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ[0],t.Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE[0]);
		
		TLorentzVector v_LcLbKp_con = v_Lc_con+v_LbKp_con;
		TLorentzVector v_LcLbKm_con = v_Lc_con+v_LbKm_con;
		TLorentzVector v_LcLbPi_con = v_Lc_con+v_LbPi_con;
		TLorentzVector v_LcLbKpKm_con = v_Lc_con+v_LbKp_con+v_LbKm_con;
		TLorentzVector v_LcLbKpPi_con = v_Lc_con+v_LbKp_con+v_LbPi_con;
		TLorentzVector v_LcLbKmPi_con = v_Lc_con+v_LbKm_con+v_LbPi_con;
		//TLorentzVector v_Lb = v_Lc+v_LbKm+v_LbPi+v_LbKp;
		
		LcLbKp_M_con   =   v_LcLbKp_con.M();
		LcLbKm_M_con   =   v_LcLbKm_con.M();
		LcLbPi_M_con   =   v_LcLbPi_con.M();
		LcLbKpKm_M_con =   v_LcLbKpKm_con.M();
		LcLbKpPi_M_con =   v_LcLbKpPi_con.M();
		LcLbKmPi_M_con =   v_LcLbKmPi_con.M();
		/////////////////////////////////////////////////////////////////////
#endif

		TLorentzVector v_LbKpKm = v_LbKp_con + v_LbKm_con;
		TLorentzVector v_LbKpPi = v_LbKp_con + v_LbPi_con;
		TLorentzVector v_LbKmPi = v_LbKm_con + v_LbPi_con;
		TLorentzVector v_LbKpKmPi = v_LbKp_con + v_LbKm_con + v_LbPi_con;

		LbKpKm_M = v_LbKpKm.M();
		LbKpPi_M = v_LbKpPi.M();
		LbKmPi_M = v_LbKmPi.M();
		LbKpKmPi_M = v_LbKpKmPi.M();

// Fill eta		
		Lb_ETA = 0.5*log( (t.Lb_P+t.Lb_PZ)/(t.Lb_P-t.Lb_PZ) );
		Lc_ETA = 0.5*log( (t.Lc_P+t.Lc_PZ)/(t.Lc_P-t.Lc_PZ) );
		LcP_ETA = 0.5*log( (t.LcP_P+t.LcP_PZ)/(t.LcP_P-t.LcP_PZ) );
		LcK_ETA = 0.5*log( (t.LcK_P+t.LcK_PZ)/(t.LcK_P-t.LcK_PZ) );
		LcPi_ETA = 0.5*log( (t.LcPi_P+t.LcPi_PZ)/(t.LcPi_P-t.LcPi_PZ) );
		LbPi_ETA = 0.5*log( (t.LbPi_P+t.LbPi_PZ)/(t.LbPi_P-t.LbPi_PZ) );
		LbKp_ETA = 0.5*log( (t.LbKp_P+t.LbKp_PZ)/(t.LbKp_P-t.LbKp_PZ) );
		LbKm_ETA = 0.5*log( (t.LbKm_P+t.LbKm_PZ)/(t.LbKm_P-t.LbKm_PZ) );

//Fill the distance of Lb and Lc in z direction, for tmva
		Distance_ZZ = t.Lc_ENDVERTEX_Z - t.Lb_ENDVERTEX_Z;
		min_pt_K = TMath::Min(t.LbKp_PT,t.LbKm_P);
		min_IP_K = TMath::Min(t.LbKp_IPCHI2_OWNPV,t.LbKm_IPCHI2_OWNPV);


//Miss particle
		TLorentzVector v_LcP	  (t.LcP_PX,t.LcP_PY,t.LcP_PZ,t.LcP_PE);
		TLorentzVector v_LcK      (t.LcK_PX,t.LcK_PY,t.LcK_PZ,t.LcK_PE);
		TLorentzVector v_LcPi     (t.LcPi_PX,t.LcPi_PY,t.LcPi_PZ,t.LcPi_PE);
		TLorentzVector v_LcP2K    (t.LcP_PX,t.LcP_PY,t.LcP_PZ,
					sqrt(pow(t.LcP_PX,2)+pow(t.LcP_PY,2)+pow(t.LcP_PZ,2)+pow(493.68,2)) );
		TLorentzVector v_LcP2Pi   (t.LcP_PX,t.LcP_PY,t.LcP_PZ,
					sqrt(pow(t.LcP_PX,2)+pow(t.LcP_PY,2)+pow(t.LcP_PZ,2)+pow(139.57,2)) );
	
		TLorentzVector v_Lc_miss2K  = v_LcK  + v_LcPi + v_LcP2K;
		TLorentzVector v_Lc_miss2Pi = v_LcK  + v_LcPi + v_LcP2Pi;
		TLorentzVector v_Lb_miss2K  = v_LbKp + v_LbKm + v_LbPi + v_Lc_miss2K;
		TLorentzVector v_Lb_miss2Pi = v_LbKp + v_LbKm + v_LbPi + v_Lc_miss2Pi;

		miss_LcM2K  =  v_Lc_miss2K.M();
		miss_LcM2Pi =  v_Lc_miss2Pi.M();
 		miss_LbM2K  =  v_Lb_miss2K.M();
 		miss_LbM2Pi =  v_Lb_miss2Pi.M();


		TLorentzVector v_Kp2Pi;	v_Kp2Pi.SetXYZM(t.LbKp_PX,t.LbKp_PY,t.LbKp_PZ,139.57);
		TLorentzVector v_Km2Pi;	v_Km2Pi.SetXYZM(t.LbKm_PX,t.LbKm_PY,t.LbKm_PZ,139.57);
		TLorentzVector v_Pi2K;	v_Pi2K.SetXYZM(t.LbPi_PX,t.LbPi_PY,t.LbPi_PZ,493.68);

		TLorentzVector v_LbM_Kp2Pi;	v_LbM_Kp2Pi = v_Lc_con + v_Kp2Pi + v_LbKm + v_LbPi;
		TLorentzVector v_LbM_Km2Pi;	v_LbM_Km2Pi = v_Lc_con + v_Km2Pi + v_LbKp + v_LbPi;
		TLorentzVector v_LbM_Pi2K;	v_LbM_Pi2K = v_Lc_con + v_LbKm + v_LbKp + v_Pi2K;

		LbM_Kp2Pi = v_LbM_Kp2Pi.M();
		LbM_Km2Pi = v_LbM_Km2Pi.M();
		LbM_Pi2K = v_LbM_Pi2K.M();

// check D_star

		TLorentzVector v_Dstar_Kp2Pi;	v_Dstar_Kp2Pi = v_Kp2Pi + v_LbKm + v_LbPi;
		TLorentzVector v_Dstar_Km2Pi;	v_Dstar_Km2Pi = v_Km2Pi + v_LbKp + v_LbPi;

		D_star_Kp2Pi = v_Dstar_Kp2Pi.M();
		D_star_Km2Pi = v_Dstar_Km2Pi.M();

// for dalitz weight
		TLorentzVector v_mkp = v_LcK + v_LcP;
		TLorentzVector v_mkpi = v_LcK + v_LcPi;

		mkp = v_mkp.M();
		mkpi = v_mkpi.M();


//calculate the angle between final particles
		angle_LcP_LcPi  = v_LcP.Angle(v_LcPi.Vect());
		angle_LcP_LbKp  = v_LcP.Angle(v_LbKp.Vect());
		angle_LcPi_LbKp = v_LcPi.Angle(v_LbKp.Vect());
		
		angle_LcK_LbKm  = v_LcK.Angle(v_LbKm.Vect());
      	angle_LcK_LbPi  = v_LcK.Angle(v_LbPi.Vect());
      	angle_LbKm_LbPi = v_LbKm.Angle(v_LbPi.Vect());


// for poli weight
		TLorentzVector v_Lb (t.Lb_PX,t.Lb_PY,t.Lb_PZ,t.Lb_PE);
		TVector3 boosttoparent = -v_Lb.BoostVector();
		v_LcP.Boost(boosttoparent);
		v_LcK.Boost(boosttoparent);
		v_LcPi.Boost(boosttoparent);
		v_Lc.Boost(boosttoparent);

		TVector3 Lb_unit = v_Lb.Vect().Unit();
		TVector3 P_beam(0.0,0.0,1.0);
		TVector3 Lc_unit = v_Lc.Vect().Unit();

		////////////////////// Lc frame //////////////////////////////
		boosttoparent = -v_Lc.BoostVector();
		v_LcP.Boost(boosttoparent);
		v_LcK.Boost(boosttoparent);
		v_LcPi.Boost(boosttoparent);
		//v_Lc.Boost(boosttoparent);
		//cout << v_Lc.X() << "---" << v_Lc.Y() << endl;

		TVector3 pi_unit_L = v_LcPi.Vect().Unit();
		cosTheta = Lc_unit.Dot(pi_unit_L);

		TVector3 aboost = -(v_Lb.Vect() - ((v_Lb.Vect()).Dot(Lc_unit))*Lc_unit).Unit();
		double cosphi = aboost.Dot(pi_unit_L);
		double sinphi = (((Lc_unit).Cross(aboost)).Unit()).Dot(pi_unit_L);
		phi = atan2(sinphi,cosphi);

		TLorentzVector v_pk = v_LcP + v_LcK;
		TVector3 pk_unit = v_pk.Vect().Unit();
#if 1
		TVector3 LcPi_unit = v_LcPi.Vect().Unit();
		//////////////////// pk frame /////////////////////////////////
		boosttoparent = -v_pk.BoostVector();
		v_LcK.Boost(boosttoparent);
		v_LcP.Boost(boosttoparent);

		//cout << "LcK: " << v_LcK.X() << " LcP: " << v_LcP.X() << endl;

		TVector3 p_unit_pk = v_LcP.Vect().Unit();
		cosTheta_pk = pk_unit.Dot(p_unit_pk);

		TVector3 aboost_pk = -(v_Lc.Vect() - ((v_Lc.Vect()).Dot(pk_unit))*pk_unit).Unit();
		double cosphi_pk = aboost_pk.Dot(p_unit_pk);
		double sinphi_pk = (((pk_unit).Cross(aboost_pk)).Unit()).Dot(p_unit_pk);
		phi_pk = atan2(sinphi_pk,cosphi_pk);

#endif


//the daughter information
		if(t.LcK_PT > t.LcPi_PT){	Lc_Dau_Min_PT = t.LcPi_PT;	}
                	else{	Lc_Dau_Min_PT = t.LcK_PT;	}
                if(Lc_Dau_Min_PT > t.LcP_PT){	Lc_Dau_Min_PT = t.LcP_PT;	}

                if(t.LcK_IPCHI2_OWNPV > t.LcPi_IPCHI2_OWNPV){	Lc_Dau_Min_IPCHI = t.LcPi_IPCHI2_OWNPV;	}
                	else{	Lc_Dau_Min_IPCHI = t.LcK_IPCHI2_OWNPV;	}
                if(Lc_Dau_Min_IPCHI > t.LcP_IPCHI2_OWNPV){	Lc_Dau_Min_IPCHI = t.LcP_IPCHI2_OWNPV;	}


		bool judge2 = true;
		
		//judge2 = (t.Lc_L0HadronDecision_TOS == true or t.Lb_L0Global_TIS == true)&&
		//judge2 = (t.Lb_L0HadronDecision_TOS == true or t.Lb_L0HadronDecision_TIS == true )&&
		//judge2 = (t.Lb_L0HadronDecision_TOS == true or t.Lb_L0HadronDecision_TIS == true or t.Lb_L0MuonDecision_TIS == true)&&
		judge2 = (t.Lb_L0HadronDecision_TOS == true )&&
		   	   (t.Lb_Hlt1TrackAllL0Decision_TOS == true )&&
		   	   (t.Lb_Hlt2Topo2BodyBBDTDecision_TOS == true or t.Lb_Hlt2Topo3BodyBBDTDecision_TOS == true or t.Lb_Hlt2Topo4BodyBBDTDecision_TOS == true);
		if(judge2 == 0)   {continue;}

		bool judge3 = true;
		judge3 = ( t.LcP_P>5000 && t.LcP_P<201000 && LcP_ETA>1.9 && LcP_ETA<4.9 )&&
		   ( t.LcK_P>5000 && t.LcK_P<201000 && LcK_ETA>1.9 && LcK_ETA<4.9 )&&
		   ( t.LcPi_P>5000 && t.LcPi_P<201000 && LcPi_ETA>1.9 && LcPi_ETA<4.9 )&&
		   ( t.LbKp_P>5000 && t.LbKp_P<201000 && LbKp_ETA>1.9 && LbKp_ETA<4.9 )&&
		   ( t.LbKm_P>5000 && t.LbKm_P<201000 && LbKm_ETA>1.9 && LbKm_ETA<4.9 )&&
		   ( t.LbPi_P>5000 && t.LbPi_P<201000 && LbPi_ETA>1.9 && LbPi_ETA<4.9 );


		if(judge3 == true){
                  newtree->Fill();
		}     

	}
	
	
	newtree->Write();
      file->Close();
#endif
	return 0;
}

	





