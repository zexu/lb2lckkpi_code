//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 23 20:46:53 2018 by ROOT version 5.34/36
// from TTree DecayTree/DecayTree
// found on file: Lb2Lckkpi_reduce.root
//////////////////////////////////////////////////////////

#ifndef DecayTree_h
#define DecayTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxLb_ENDVERTEX_COV = 1;
   const Int_t kMaxLb_OWNPV_COV = 1;
   const Int_t kMaxLc_ENDVERTEX_COV = 1;
   const Int_t kMaxLc_OWNPV_COV = 1;
   const Int_t kMaxLc_ORIVX_COV = 1;
   const Int_t kMaxLcP_OWNPV_COV = 1;
   const Int_t kMaxLcP_ORIVX_COV = 1;
   const Int_t kMaxLcK_OWNPV_COV = 1;
   const Int_t kMaxLcK_ORIVX_COV = 1;
   const Int_t kMaxLcPi_OWNPV_COV = 1;
   const Int_t kMaxLcPi_ORIVX_COV = 1;
   const Int_t kMaxa1_ENDVERTEX_COV = 1;
   const Int_t kMaxa1_OWNPV_COV = 1;
   const Int_t kMaxa1_ORIVX_COV = 1;
   const Int_t kMaxLbKp_OWNPV_COV = 1;
   const Int_t kMaxLbKp_ORIVX_COV = 1;
   const Int_t kMaxLbKm_OWNPV_COV = 1;
   const Int_t kMaxLbKm_ORIVX_COV = 1;
   const Int_t kMaxLbPi_OWNPV_COV = 1;
   const Int_t kMaxLbPi_ORIVX_COV = 1;

class DecayTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        Lb_DTF_CHI2NDOF;
   Double_t        Lb_DTF_CTAU;
   Double_t        Lb_DTF_CTAUS;
   Double_t        Lb_DTF_M;
   Double_t        Lb_LOKI_DIRA;
   Double_t        Lb_LOKI_FDCHI2;
   Double_t        Lb_LOKI_FDS;
   Double_t        Lb_LV01;
   Double_t        Lb_LV02;
   Double_t        Lb_Lc_DTF_CTAU;
   Double_t        Lb_Lc_DTF_CTAUS;
   Double_t        Lb_ENDVERTEX_X;
   Double_t        Lb_ENDVERTEX_Y;
   Double_t        Lb_ENDVERTEX_Z;
   Double_t        Lb_ENDVERTEX_XERR;
   Double_t        Lb_ENDVERTEX_YERR;
   Double_t        Lb_ENDVERTEX_ZERR;
   Double_t        Lb_ENDVERTEX_CHI2;
   Int_t           Lb_ENDVERTEX_NDOF;
   Float_t         Lb_ENDVERTEX_COV_[3][3];
   Double_t        Lb_OWNPV_X;
   Double_t        Lb_OWNPV_Y;
   Double_t        Lb_OWNPV_Z;
   Double_t        Lb_OWNPV_XERR;
   Double_t        Lb_OWNPV_YERR;
   Double_t        Lb_OWNPV_ZERR;
   Double_t        Lb_OWNPV_CHI2;
   Int_t           Lb_OWNPV_NDOF;
   Float_t         Lb_OWNPV_COV_[3][3];
   Double_t        Lb_IP_OWNPV;
   Double_t        Lb_IPCHI2_OWNPV;
   Double_t        Lb_FD_OWNPV;
   Double_t        Lb_FDCHI2_OWNPV;
   Double_t        Lb_DIRA_OWNPV;
   Double_t        Lb_P;
   Double_t        Lb_PT;
   Double_t        Lb_PE;
   Double_t        Lb_PX;
   Double_t        Lb_PY;
   Double_t        Lb_PZ;
   Double_t        Lb_MM;
   Double_t        Lb_MMERR;
   Double_t        Lb_M;
   Int_t           Lb_ID;
   Double_t        Lb_TAU;
   Double_t        Lb_TAUERR;
   Double_t        Lb_TAUCHI2;
   Bool_t          Lb_L0Global_Dec;
   Bool_t          Lb_L0Global_TIS;
   Bool_t          Lb_L0Global_TOS;
   Bool_t          Lb_Hlt1Global_Dec;
   Bool_t          Lb_Hlt1Global_TIS;
   Bool_t          Lb_Hlt1Global_TOS;
   Bool_t          Lb_Hlt1Phys_Dec;
   Bool_t          Lb_Hlt1Phys_TIS;
   Bool_t          Lb_Hlt1Phys_TOS;
   Bool_t          Lb_Hlt2Global_Dec;
   Bool_t          Lb_Hlt2Global_TIS;
   Bool_t          Lb_Hlt2Global_TOS;
   Bool_t          Lb_Hlt2Phys_Dec;
   Bool_t          Lb_Hlt2Phys_TIS;
   Bool_t          Lb_Hlt2Phys_TOS;
   Bool_t          Lb_L0HadronDecision_Dec;
   Bool_t          Lb_L0HadronDecision_TIS;
   Bool_t          Lb_L0HadronDecision_TOS;
   Bool_t          Lb_L0MuonDecision_Dec;
   Bool_t          Lb_L0MuonDecision_TIS;
   Bool_t          Lb_L0MuonDecision_TOS;
   Bool_t          Lb_L0ElectronDecision_Dec;
   Bool_t          Lb_L0ElectronDecision_TIS;
   Bool_t          Lb_L0ElectronDecision_TOS;
   Bool_t          Lb_L0PhotonDecision_Dec;
   Bool_t          Lb_L0PhotonDecision_TIS;
   Bool_t          Lb_L0PhotonDecision_TOS;
   Bool_t          Lb_L0DiMuonDecision_Dec;
   Bool_t          Lb_L0DiMuonDecision_TIS;
   Bool_t          Lb_L0DiMuonDecision_TOS;
   Bool_t          Lb_L0DiHadronDecision_Dec;
   Bool_t          Lb_L0DiHadronDecision_TIS;
   Bool_t          Lb_L0DiHadronDecision_TOS;
   Bool_t          Lb_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Lb_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Lb_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Lb_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Lb_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Lb_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Lb_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Lb_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Lb_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Lb_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Lb_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Lb_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Lb_Hlt2IncPhiDecision_Dec;
   Bool_t          Lb_Hlt2IncPhiDecision_TIS;
   Bool_t          Lb_Hlt2IncPhiDecision_TOS;
   Int_t           Lb_DTF_LbLcMassCon__nPV;
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__M[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__MERR[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__P[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__PERR[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__PV_key[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_ID[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__chi2[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__ctau[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__ctauErr[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__decayLength[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__decayLengthErr[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__nDOF[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__nIter[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Float_t         Lb_DTF_LbLcMassCon__status[100];   //[Lb_DTF_LbLcMassCon__nPV]
   Int_t           Lb_DTF_LcMassCon__nPV;
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_Kplus_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_piplus_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_piplus_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_piplus_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_piplus_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_piplus_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_pplus_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_pplus_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_pplus_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_pplus_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__Lambda_cplus_pplus_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__M[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__MERR[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__P[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__PERR[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__PV_key[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_piplus_ID[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PE[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PX[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PY[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PZ[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__chi2[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__ctau[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__ctauErr[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__decayLength[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__decayLengthErr[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__nDOF[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__nIter[100];   //[Lb_DTF_LcMassCon__nPV]
   Float_t         Lb_DTF_LcMassCon__status[100];   //[Lb_DTF_LcMassCon__nPV]
   Double_t        Lc_DTF_CHI2NDOF;
   Double_t        Lc_DTF_CTAU;
   Double_t        Lc_DTF_CTAUS;
   Double_t        Lc_DTF_M;
   Double_t        Lc_LOKI_DIRA;
   Double_t        Lc_LOKI_FDCHI2;
   Double_t        Lc_LOKI_FDS;
   Double_t        Lc_LV01;
   Double_t        Lc_LV02;
   Double_t        Lc_Lc_DTF_CTAU;
   Double_t        Lc_Lc_DTF_CTAUS;
   Double_t        Lc_ENDVERTEX_X;
   Double_t        Lc_ENDVERTEX_Y;
   Double_t        Lc_ENDVERTEX_Z;
   Double_t        Lc_ENDVERTEX_XERR;
   Double_t        Lc_ENDVERTEX_YERR;
   Double_t        Lc_ENDVERTEX_ZERR;
   Double_t        Lc_ENDVERTEX_CHI2;
   Int_t           Lc_ENDVERTEX_NDOF;
   Float_t         Lc_ENDVERTEX_COV_[3][3];
   Double_t        Lc_OWNPV_X;
   Double_t        Lc_OWNPV_Y;
   Double_t        Lc_OWNPV_Z;
   Double_t        Lc_OWNPV_XERR;
   Double_t        Lc_OWNPV_YERR;
   Double_t        Lc_OWNPV_ZERR;
   Double_t        Lc_OWNPV_CHI2;
   Int_t           Lc_OWNPV_NDOF;
   Float_t         Lc_OWNPV_COV_[3][3];
   Double_t        Lc_IP_OWNPV;
   Double_t        Lc_IPCHI2_OWNPV;
   Double_t        Lc_FD_OWNPV;
   Double_t        Lc_FDCHI2_OWNPV;
   Double_t        Lc_DIRA_OWNPV;
   Double_t        Lc_ORIVX_X;
   Double_t        Lc_ORIVX_Y;
   Double_t        Lc_ORIVX_Z;
   Double_t        Lc_ORIVX_XERR;
   Double_t        Lc_ORIVX_YERR;
   Double_t        Lc_ORIVX_ZERR;
   Double_t        Lc_ORIVX_CHI2;
   Int_t           Lc_ORIVX_NDOF;
   Float_t         Lc_ORIVX_COV_[3][3];
   Double_t        Lc_FD_ORIVX;
   Double_t        Lc_FDCHI2_ORIVX;
   Double_t        Lc_DIRA_ORIVX;
   Double_t        Lc_P;
   Double_t        Lc_PT;
   Double_t        Lc_PE;
   Double_t        Lc_PX;
   Double_t        Lc_PY;
   Double_t        Lc_PZ;
   Double_t        Lc_MM;
   Double_t        Lc_MMERR;
   Double_t        Lc_M;
   Int_t           Lc_ID;
   Double_t        Lc_TAU;
   Double_t        Lc_TAUERR;
   Double_t        Lc_TAUCHI2;
   Bool_t          Lc_L0Global_Dec;
   Bool_t          Lc_L0Global_TIS;
   Bool_t          Lc_L0Global_TOS;
   Bool_t          Lc_Hlt1Global_Dec;
   Bool_t          Lc_Hlt1Global_TIS;
   Bool_t          Lc_Hlt1Global_TOS;
   Bool_t          Lc_Hlt1Phys_Dec;
   Bool_t          Lc_Hlt1Phys_TIS;
   Bool_t          Lc_Hlt1Phys_TOS;
   Bool_t          Lc_Hlt2Global_Dec;
   Bool_t          Lc_Hlt2Global_TIS;
   Bool_t          Lc_Hlt2Global_TOS;
   Bool_t          Lc_Hlt2Phys_Dec;
   Bool_t          Lc_Hlt2Phys_TIS;
   Bool_t          Lc_Hlt2Phys_TOS;
   Bool_t          Lc_L0HadronDecision_Dec;
   Bool_t          Lc_L0HadronDecision_TIS;
   Bool_t          Lc_L0HadronDecision_TOS;
   Bool_t          Lc_L0MuonDecision_Dec;
   Bool_t          Lc_L0MuonDecision_TIS;
   Bool_t          Lc_L0MuonDecision_TOS;
   Bool_t          Lc_L0ElectronDecision_Dec;
   Bool_t          Lc_L0ElectronDecision_TIS;
   Bool_t          Lc_L0ElectronDecision_TOS;
   Bool_t          Lc_L0PhotonDecision_Dec;
   Bool_t          Lc_L0PhotonDecision_TIS;
   Bool_t          Lc_L0PhotonDecision_TOS;
   Bool_t          Lc_L0DiMuonDecision_Dec;
   Bool_t          Lc_L0DiMuonDecision_TIS;
   Bool_t          Lc_L0DiMuonDecision_TOS;
   Bool_t          Lc_L0DiHadronDecision_Dec;
   Bool_t          Lc_L0DiHadronDecision_TIS;
   Bool_t          Lc_L0DiHadronDecision_TOS;
   Bool_t          Lc_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Lc_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Lc_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Lc_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Lc_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Lc_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Lc_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Lc_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Lc_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Lc_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Lc_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Lc_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Lc_Hlt2IncPhiDecision_Dec;
   Bool_t          Lc_Hlt2IncPhiDecision_TIS;
   Bool_t          Lc_Hlt2IncPhiDecision_TOS;
   Double_t        LcP_DTF_CHI2NDOF;
   Double_t        LcP_DTF_CTAU;
   Double_t        LcP_DTF_CTAUS;
   Double_t        LcP_DTF_M;
   Double_t        LcP_LOKI_DIRA;
   Double_t        LcP_LOKI_FDCHI2;
   Double_t        LcP_LOKI_FDS;
   Double_t        LcP_LV01;
   Double_t        LcP_LV02;
   Double_t        LcP_Lc_DTF_CTAU;
   Double_t        LcP_Lc_DTF_CTAUS;
   Double_t        LcP_MC12TuneV2_ProbNNe;
   Double_t        LcP_MC12TuneV2_ProbNNmu;
   Double_t        LcP_MC12TuneV2_ProbNNpi;
   Double_t        LcP_MC12TuneV2_ProbNNk;
   Double_t        LcP_MC12TuneV2_ProbNNp;
   Double_t        LcP_MC12TuneV2_ProbNNghost;
   Double_t        LcP_MC12TuneV3_ProbNNe;
   Double_t        LcP_MC12TuneV3_ProbNNmu;
   Double_t        LcP_MC12TuneV3_ProbNNpi;
   Double_t        LcP_MC12TuneV3_ProbNNk;
   Double_t        LcP_MC12TuneV3_ProbNNp;
   Double_t        LcP_MC12TuneV3_ProbNNghost;
   Double_t        LcP_MC12TuneV4_ProbNNe;
   Double_t        LcP_MC12TuneV4_ProbNNmu;
   Double_t        LcP_MC12TuneV4_ProbNNpi;
   Double_t        LcP_MC12TuneV4_ProbNNk;
   Double_t        LcP_MC12TuneV4_ProbNNp;
   Double_t        LcP_MC12TuneV4_ProbNNghost;
   Double_t        LcP_MC15TuneV1_ProbNNe;
   Double_t        LcP_MC15TuneV1_ProbNNmu;
   Double_t        LcP_MC15TuneV1_ProbNNpi;
   Double_t        LcP_MC15TuneV1_ProbNNk;
   Double_t        LcP_MC15TuneV1_ProbNNp;
   Double_t        LcP_MC15TuneV1_ProbNNghost;
   Double_t        LcP_OWNPV_X;
   Double_t        LcP_OWNPV_Y;
   Double_t        LcP_OWNPV_Z;
   Double_t        LcP_OWNPV_XERR;
   Double_t        LcP_OWNPV_YERR;
   Double_t        LcP_OWNPV_ZERR;
   Double_t        LcP_OWNPV_CHI2;
   Int_t           LcP_OWNPV_NDOF;
   Float_t         LcP_OWNPV_COV_[3][3];
   Double_t        LcP_IP_OWNPV;
   Double_t        LcP_IPCHI2_OWNPV;
   Double_t        LcP_ORIVX_X;
   Double_t        LcP_ORIVX_Y;
   Double_t        LcP_ORIVX_Z;
   Double_t        LcP_ORIVX_XERR;
   Double_t        LcP_ORIVX_YERR;
   Double_t        LcP_ORIVX_ZERR;
   Double_t        LcP_ORIVX_CHI2;
   Int_t           LcP_ORIVX_NDOF;
   Float_t         LcP_ORIVX_COV_[3][3];
   Double_t        LcP_P;
   Double_t        LcP_PT;
   Double_t        LcP_PE;
   Double_t        LcP_PX;
   Double_t        LcP_PY;
   Double_t        LcP_PZ;
   Double_t        LcP_M;
   Int_t           LcP_ID;
   Double_t        LcP_PIDe;
   Double_t        LcP_PIDmu;
   Double_t        LcP_PIDK;
   Double_t        LcP_PIDp;
   Double_t        LcP_ProbNNe;
   Double_t        LcP_ProbNNk;
   Double_t        LcP_ProbNNp;
   Double_t        LcP_ProbNNpi;
   Double_t        LcP_ProbNNmu;
   Double_t        LcP_ProbNNghost;
   Bool_t          LcP_hasMuon;
   Bool_t          LcP_isMuon;
   Bool_t          LcP_hasRich;
   Bool_t          LcP_hasCalo;
   Double_t        LcP_PP_CombDLLe;
   Double_t        LcP_PP_CombDLLmu;
   Double_t        LcP_PP_CombDLLpi;
   Double_t        LcP_PP_CombDLLk;
   Double_t        LcP_PP_CombDLLp;
   Double_t        LcP_PP_CombDLLd;
   Double_t        LcP_PP_ProbNNe;
   Double_t        LcP_PP_ProbNNmu;
   Double_t        LcP_PP_ProbNNpi;
   Double_t        LcP_PP_ProbNNk;
   Double_t        LcP_PP_ProbNNp;
   Double_t        LcP_PP_ProbNNghost;
   Bool_t          LcP_L0Global_Dec;
   Bool_t          LcP_L0Global_TIS;
   Bool_t          LcP_L0Global_TOS;
   Bool_t          LcP_Hlt1Global_Dec;
   Bool_t          LcP_Hlt1Global_TIS;
   Bool_t          LcP_Hlt1Global_TOS;
   Bool_t          LcP_Hlt1Phys_Dec;
   Bool_t          LcP_Hlt1Phys_TIS;
   Bool_t          LcP_Hlt1Phys_TOS;
   Bool_t          LcP_Hlt2Global_Dec;
   Bool_t          LcP_Hlt2Global_TIS;
   Bool_t          LcP_Hlt2Global_TOS;
   Bool_t          LcP_Hlt2Phys_Dec;
   Bool_t          LcP_Hlt2Phys_TIS;
   Bool_t          LcP_Hlt2Phys_TOS;
   Bool_t          LcP_L0HadronDecision_Dec;
   Bool_t          LcP_L0HadronDecision_TIS;
   Bool_t          LcP_L0HadronDecision_TOS;
   Bool_t          LcP_L0MuonDecision_Dec;
   Bool_t          LcP_L0MuonDecision_TIS;
   Bool_t          LcP_L0MuonDecision_TOS;
   Bool_t          LcP_L0ElectronDecision_Dec;
   Bool_t          LcP_L0ElectronDecision_TIS;
   Bool_t          LcP_L0ElectronDecision_TOS;
   Bool_t          LcP_L0PhotonDecision_Dec;
   Bool_t          LcP_L0PhotonDecision_TIS;
   Bool_t          LcP_L0PhotonDecision_TOS;
   Bool_t          LcP_L0DiMuonDecision_Dec;
   Bool_t          LcP_L0DiMuonDecision_TIS;
   Bool_t          LcP_L0DiMuonDecision_TOS;
   Bool_t          LcP_L0DiHadronDecision_Dec;
   Bool_t          LcP_L0DiHadronDecision_TIS;
   Bool_t          LcP_L0DiHadronDecision_TOS;
   Bool_t          LcP_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LcP_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LcP_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LcP_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LcP_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LcP_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LcP_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LcP_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LcP_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LcP_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LcP_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LcP_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LcP_Hlt2IncPhiDecision_Dec;
   Bool_t          LcP_Hlt2IncPhiDecision_TIS;
   Bool_t          LcP_Hlt2IncPhiDecision_TOS;
   Int_t           LcP_TRACK_Type;
   Int_t           LcP_TRACK_Key;
   Double_t        LcP_TRACK_CHI2NDOF;
   Double_t        LcP_TRACK_PCHI2;
   Double_t        LcP_TRACK_MatchCHI2;
   Double_t        LcP_TRACK_GhostProb;
   Double_t        LcP_TRACK_CloneDist;
   Double_t        LcP_TRACK_Likelihood;
   Double_t        LcK_DTF_CHI2NDOF;
   Double_t        LcK_DTF_CTAU;
   Double_t        LcK_DTF_CTAUS;
   Double_t        LcK_DTF_M;
   Double_t        LcK_LOKI_DIRA;
   Double_t        LcK_LOKI_FDCHI2;
   Double_t        LcK_LOKI_FDS;
   Double_t        LcK_LV01;
   Double_t        LcK_LV02;
   Double_t        LcK_Lc_DTF_CTAU;
   Double_t        LcK_Lc_DTF_CTAUS;
   Double_t        LcK_MC12TuneV2_ProbNNe;
   Double_t        LcK_MC12TuneV2_ProbNNmu;
   Double_t        LcK_MC12TuneV2_ProbNNpi;
   Double_t        LcK_MC12TuneV2_ProbNNk;
   Double_t        LcK_MC12TuneV2_ProbNNp;
   Double_t        LcK_MC12TuneV2_ProbNNghost;
   Double_t        LcK_MC12TuneV3_ProbNNe;
   Double_t        LcK_MC12TuneV3_ProbNNmu;
   Double_t        LcK_MC12TuneV3_ProbNNpi;
   Double_t        LcK_MC12TuneV3_ProbNNk;
   Double_t        LcK_MC12TuneV3_ProbNNp;
   Double_t        LcK_MC12TuneV3_ProbNNghost;
   Double_t        LcK_MC12TuneV4_ProbNNe;
   Double_t        LcK_MC12TuneV4_ProbNNmu;
   Double_t        LcK_MC12TuneV4_ProbNNpi;
   Double_t        LcK_MC12TuneV4_ProbNNk;
   Double_t        LcK_MC12TuneV4_ProbNNp;
   Double_t        LcK_MC12TuneV4_ProbNNghost;
   Double_t        LcK_MC15TuneV1_ProbNNe;
   Double_t        LcK_MC15TuneV1_ProbNNmu;
   Double_t        LcK_MC15TuneV1_ProbNNpi;
   Double_t        LcK_MC15TuneV1_ProbNNk;
   Double_t        LcK_MC15TuneV1_ProbNNp;
   Double_t        LcK_MC15TuneV1_ProbNNghost;
   Double_t        LcK_OWNPV_X;
   Double_t        LcK_OWNPV_Y;
   Double_t        LcK_OWNPV_Z;
   Double_t        LcK_OWNPV_XERR;
   Double_t        LcK_OWNPV_YERR;
   Double_t        LcK_OWNPV_ZERR;
   Double_t        LcK_OWNPV_CHI2;
   Int_t           LcK_OWNPV_NDOF;
   Float_t         LcK_OWNPV_COV_[3][3];
   Double_t        LcK_IP_OWNPV;
   Double_t        LcK_IPCHI2_OWNPV;
   Double_t        LcK_ORIVX_X;
   Double_t        LcK_ORIVX_Y;
   Double_t        LcK_ORIVX_Z;
   Double_t        LcK_ORIVX_XERR;
   Double_t        LcK_ORIVX_YERR;
   Double_t        LcK_ORIVX_ZERR;
   Double_t        LcK_ORIVX_CHI2;
   Int_t           LcK_ORIVX_NDOF;
   Float_t         LcK_ORIVX_COV_[3][3];
   Double_t        LcK_P;
   Double_t        LcK_PT;
   Double_t        LcK_PE;
   Double_t        LcK_PX;
   Double_t        LcK_PY;
   Double_t        LcK_PZ;
   Double_t        LcK_M;
   Int_t           LcK_ID;
   Double_t        LcK_PIDe;
   Double_t        LcK_PIDmu;
   Double_t        LcK_PIDK;
   Double_t        LcK_PIDp;
   Double_t        LcK_ProbNNe;
   Double_t        LcK_ProbNNk;
   Double_t        LcK_ProbNNp;
   Double_t        LcK_ProbNNpi;
   Double_t        LcK_ProbNNmu;
   Double_t        LcK_ProbNNghost;
   Bool_t          LcK_hasMuon;
   Bool_t          LcK_isMuon;
   Bool_t          LcK_hasRich;
   Bool_t          LcK_hasCalo;
   Double_t        LcK_PP_CombDLLe;
   Double_t        LcK_PP_CombDLLmu;
   Double_t        LcK_PP_CombDLLpi;
   Double_t        LcK_PP_CombDLLk;
   Double_t        LcK_PP_CombDLLp;
   Double_t        LcK_PP_CombDLLd;
   Double_t        LcK_PP_ProbNNe;
   Double_t        LcK_PP_ProbNNmu;
   Double_t        LcK_PP_ProbNNpi;
   Double_t        LcK_PP_ProbNNk;
   Double_t        LcK_PP_ProbNNp;
   Double_t        LcK_PP_ProbNNghost;
   Bool_t          LcK_L0Global_Dec;
   Bool_t          LcK_L0Global_TIS;
   Bool_t          LcK_L0Global_TOS;
   Bool_t          LcK_Hlt1Global_Dec;
   Bool_t          LcK_Hlt1Global_TIS;
   Bool_t          LcK_Hlt1Global_TOS;
   Bool_t          LcK_Hlt1Phys_Dec;
   Bool_t          LcK_Hlt1Phys_TIS;
   Bool_t          LcK_Hlt1Phys_TOS;
   Bool_t          LcK_Hlt2Global_Dec;
   Bool_t          LcK_Hlt2Global_TIS;
   Bool_t          LcK_Hlt2Global_TOS;
   Bool_t          LcK_Hlt2Phys_Dec;
   Bool_t          LcK_Hlt2Phys_TIS;
   Bool_t          LcK_Hlt2Phys_TOS;
   Bool_t          LcK_L0HadronDecision_Dec;
   Bool_t          LcK_L0HadronDecision_TIS;
   Bool_t          LcK_L0HadronDecision_TOS;
   Bool_t          LcK_L0MuonDecision_Dec;
   Bool_t          LcK_L0MuonDecision_TIS;
   Bool_t          LcK_L0MuonDecision_TOS;
   Bool_t          LcK_L0ElectronDecision_Dec;
   Bool_t          LcK_L0ElectronDecision_TIS;
   Bool_t          LcK_L0ElectronDecision_TOS;
   Bool_t          LcK_L0PhotonDecision_Dec;
   Bool_t          LcK_L0PhotonDecision_TIS;
   Bool_t          LcK_L0PhotonDecision_TOS;
   Bool_t          LcK_L0DiMuonDecision_Dec;
   Bool_t          LcK_L0DiMuonDecision_TIS;
   Bool_t          LcK_L0DiMuonDecision_TOS;
   Bool_t          LcK_L0DiHadronDecision_Dec;
   Bool_t          LcK_L0DiHadronDecision_TIS;
   Bool_t          LcK_L0DiHadronDecision_TOS;
   Bool_t          LcK_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LcK_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LcK_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LcK_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LcK_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LcK_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LcK_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LcK_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LcK_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LcK_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LcK_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LcK_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LcK_Hlt2IncPhiDecision_Dec;
   Bool_t          LcK_Hlt2IncPhiDecision_TIS;
   Bool_t          LcK_Hlt2IncPhiDecision_TOS;
   Int_t           LcK_TRACK_Type;
   Int_t           LcK_TRACK_Key;
   Double_t        LcK_TRACK_CHI2NDOF;
   Double_t        LcK_TRACK_PCHI2;
   Double_t        LcK_TRACK_MatchCHI2;
   Double_t        LcK_TRACK_GhostProb;
   Double_t        LcK_TRACK_CloneDist;
   Double_t        LcK_TRACK_Likelihood;
   Double_t        LcPi_DTF_CHI2NDOF;
   Double_t        LcPi_DTF_CTAU;
   Double_t        LcPi_DTF_CTAUS;
   Double_t        LcPi_DTF_M;
   Double_t        LcPi_LOKI_DIRA;
   Double_t        LcPi_LOKI_FDCHI2;
   Double_t        LcPi_LOKI_FDS;
   Double_t        LcPi_LV01;
   Double_t        LcPi_LV02;
   Double_t        LcPi_Lc_DTF_CTAU;
   Double_t        LcPi_Lc_DTF_CTAUS;
   Double_t        LcPi_MC12TuneV2_ProbNNe;
   Double_t        LcPi_MC12TuneV2_ProbNNmu;
   Double_t        LcPi_MC12TuneV2_ProbNNpi;
   Double_t        LcPi_MC12TuneV2_ProbNNk;
   Double_t        LcPi_MC12TuneV2_ProbNNp;
   Double_t        LcPi_MC12TuneV2_ProbNNghost;
   Double_t        LcPi_MC12TuneV3_ProbNNe;
   Double_t        LcPi_MC12TuneV3_ProbNNmu;
   Double_t        LcPi_MC12TuneV3_ProbNNpi;
   Double_t        LcPi_MC12TuneV3_ProbNNk;
   Double_t        LcPi_MC12TuneV3_ProbNNp;
   Double_t        LcPi_MC12TuneV3_ProbNNghost;
   Double_t        LcPi_MC12TuneV4_ProbNNe;
   Double_t        LcPi_MC12TuneV4_ProbNNmu;
   Double_t        LcPi_MC12TuneV4_ProbNNpi;
   Double_t        LcPi_MC12TuneV4_ProbNNk;
   Double_t        LcPi_MC12TuneV4_ProbNNp;
   Double_t        LcPi_MC12TuneV4_ProbNNghost;
   Double_t        LcPi_MC15TuneV1_ProbNNe;
   Double_t        LcPi_MC15TuneV1_ProbNNmu;
   Double_t        LcPi_MC15TuneV1_ProbNNpi;
   Double_t        LcPi_MC15TuneV1_ProbNNk;
   Double_t        LcPi_MC15TuneV1_ProbNNp;
   Double_t        LcPi_MC15TuneV1_ProbNNghost;
   Double_t        LcPi_OWNPV_X;
   Double_t        LcPi_OWNPV_Y;
   Double_t        LcPi_OWNPV_Z;
   Double_t        LcPi_OWNPV_XERR;
   Double_t        LcPi_OWNPV_YERR;
   Double_t        LcPi_OWNPV_ZERR;
   Double_t        LcPi_OWNPV_CHI2;
   Int_t           LcPi_OWNPV_NDOF;
   Float_t         LcPi_OWNPV_COV_[3][3];
   Double_t        LcPi_IP_OWNPV;
   Double_t        LcPi_IPCHI2_OWNPV;
   Double_t        LcPi_ORIVX_X;
   Double_t        LcPi_ORIVX_Y;
   Double_t        LcPi_ORIVX_Z;
   Double_t        LcPi_ORIVX_XERR;
   Double_t        LcPi_ORIVX_YERR;
   Double_t        LcPi_ORIVX_ZERR;
   Double_t        LcPi_ORIVX_CHI2;
   Int_t           LcPi_ORIVX_NDOF;
   Float_t         LcPi_ORIVX_COV_[3][3];
   Double_t        LcPi_P;
   Double_t        LcPi_PT;
   Double_t        LcPi_PE;
   Double_t        LcPi_PX;
   Double_t        LcPi_PY;
   Double_t        LcPi_PZ;
   Double_t        LcPi_M;
   Int_t           LcPi_ID;
   Double_t        LcPi_PIDe;
   Double_t        LcPi_PIDmu;
   Double_t        LcPi_PIDK;
   Double_t        LcPi_PIDp;
   Double_t        LcPi_ProbNNe;
   Double_t        LcPi_ProbNNk;
   Double_t        LcPi_ProbNNp;
   Double_t        LcPi_ProbNNpi;
   Double_t        LcPi_ProbNNmu;
   Double_t        LcPi_ProbNNghost;
   Bool_t          LcPi_hasMuon;
   Bool_t          LcPi_isMuon;
   Bool_t          LcPi_hasRich;
   Bool_t          LcPi_hasCalo;
   Double_t        LcPi_PP_CombDLLe;
   Double_t        LcPi_PP_CombDLLmu;
   Double_t        LcPi_PP_CombDLLpi;
   Double_t        LcPi_PP_CombDLLk;
   Double_t        LcPi_PP_CombDLLp;
   Double_t        LcPi_PP_CombDLLd;
   Double_t        LcPi_PP_ProbNNe;
   Double_t        LcPi_PP_ProbNNmu;
   Double_t        LcPi_PP_ProbNNpi;
   Double_t        LcPi_PP_ProbNNk;
   Double_t        LcPi_PP_ProbNNp;
   Double_t        LcPi_PP_ProbNNghost;
   Bool_t          LcPi_L0Global_Dec;
   Bool_t          LcPi_L0Global_TIS;
   Bool_t          LcPi_L0Global_TOS;
   Bool_t          LcPi_Hlt1Global_Dec;
   Bool_t          LcPi_Hlt1Global_TIS;
   Bool_t          LcPi_Hlt1Global_TOS;
   Bool_t          LcPi_Hlt1Phys_Dec;
   Bool_t          LcPi_Hlt1Phys_TIS;
   Bool_t          LcPi_Hlt1Phys_TOS;
   Bool_t          LcPi_Hlt2Global_Dec;
   Bool_t          LcPi_Hlt2Global_TIS;
   Bool_t          LcPi_Hlt2Global_TOS;
   Bool_t          LcPi_Hlt2Phys_Dec;
   Bool_t          LcPi_Hlt2Phys_TIS;
   Bool_t          LcPi_Hlt2Phys_TOS;
   Bool_t          LcPi_L0HadronDecision_Dec;
   Bool_t          LcPi_L0HadronDecision_TIS;
   Bool_t          LcPi_L0HadronDecision_TOS;
   Bool_t          LcPi_L0MuonDecision_Dec;
   Bool_t          LcPi_L0MuonDecision_TIS;
   Bool_t          LcPi_L0MuonDecision_TOS;
   Bool_t          LcPi_L0ElectronDecision_Dec;
   Bool_t          LcPi_L0ElectronDecision_TIS;
   Bool_t          LcPi_L0ElectronDecision_TOS;
   Bool_t          LcPi_L0PhotonDecision_Dec;
   Bool_t          LcPi_L0PhotonDecision_TIS;
   Bool_t          LcPi_L0PhotonDecision_TOS;
   Bool_t          LcPi_L0DiMuonDecision_Dec;
   Bool_t          LcPi_L0DiMuonDecision_TIS;
   Bool_t          LcPi_L0DiMuonDecision_TOS;
   Bool_t          LcPi_L0DiHadronDecision_Dec;
   Bool_t          LcPi_L0DiHadronDecision_TIS;
   Bool_t          LcPi_L0DiHadronDecision_TOS;
   Bool_t          LcPi_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LcPi_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LcPi_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LcPi_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LcPi_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LcPi_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LcPi_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LcPi_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LcPi_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LcPi_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LcPi_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LcPi_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LcPi_Hlt2IncPhiDecision_Dec;
   Bool_t          LcPi_Hlt2IncPhiDecision_TIS;
   Bool_t          LcPi_Hlt2IncPhiDecision_TOS;
   Int_t           LcPi_TRACK_Type;
   Int_t           LcPi_TRACK_Key;
   Double_t        LcPi_TRACK_CHI2NDOF;
   Double_t        LcPi_TRACK_PCHI2;
   Double_t        LcPi_TRACK_MatchCHI2;
   Double_t        LcPi_TRACK_GhostProb;
   Double_t        LcPi_TRACK_CloneDist;
   Double_t        LcPi_TRACK_Likelihood;
   Double_t        a1_DTF_CHI2NDOF;
   Double_t        a1_DTF_CTAU;
   Double_t        a1_DTF_CTAUS;
   Double_t        a1_DTF_M;
   Double_t        a1_LOKI_DIRA;
   Double_t        a1_LOKI_FDCHI2;
   Double_t        a1_LOKI_FDS;
   Double_t        a1_LV01;
   Double_t        a1_LV02;
   Double_t        a1_Lc_DTF_CTAU;
   Double_t        a1_Lc_DTF_CTAUS;
   Double_t        a1_ENDVERTEX_X;
   Double_t        a1_ENDVERTEX_Y;
   Double_t        a1_ENDVERTEX_Z;
   Double_t        a1_ENDVERTEX_XERR;
   Double_t        a1_ENDVERTEX_YERR;
   Double_t        a1_ENDVERTEX_ZERR;
   Double_t        a1_ENDVERTEX_CHI2;
   Int_t           a1_ENDVERTEX_NDOF;
   Float_t         a1_ENDVERTEX_COV_[3][3];
   Double_t        a1_OWNPV_X;
   Double_t        a1_OWNPV_Y;
   Double_t        a1_OWNPV_Z;
   Double_t        a1_OWNPV_XERR;
   Double_t        a1_OWNPV_YERR;
   Double_t        a1_OWNPV_ZERR;
   Double_t        a1_OWNPV_CHI2;
   Int_t           a1_OWNPV_NDOF;
   Float_t         a1_OWNPV_COV_[3][3];
   Double_t        a1_IP_OWNPV;
   Double_t        a1_IPCHI2_OWNPV;
   Double_t        a1_FD_OWNPV;
   Double_t        a1_FDCHI2_OWNPV;
   Double_t        a1_DIRA_OWNPV;
   Double_t        a1_ORIVX_X;
   Double_t        a1_ORIVX_Y;
   Double_t        a1_ORIVX_Z;
   Double_t        a1_ORIVX_XERR;
   Double_t        a1_ORIVX_YERR;
   Double_t        a1_ORIVX_ZERR;
   Double_t        a1_ORIVX_CHI2;
   Int_t           a1_ORIVX_NDOF;
   Float_t         a1_ORIVX_COV_[3][3];
   Double_t        a1_FD_ORIVX;
   Double_t        a1_FDCHI2_ORIVX;
   Double_t        a1_DIRA_ORIVX;
   Double_t        a1_P;
   Double_t        a1_PT;
   Double_t        a1_PE;
   Double_t        a1_PX;
   Double_t        a1_PY;
   Double_t        a1_PZ;
   Double_t        a1_MM;
   Double_t        a1_MMERR;
   Double_t        a1_M;
   Int_t           a1_ID;
   Double_t        a1_TAU;
   Double_t        a1_TAUERR;
   Double_t        a1_TAUCHI2;
   Bool_t          a1_L0Global_Dec;
   Bool_t          a1_L0Global_TIS;
   Bool_t          a1_L0Global_TOS;
   Bool_t          a1_Hlt1Global_Dec;
   Bool_t          a1_Hlt1Global_TIS;
   Bool_t          a1_Hlt1Global_TOS;
   Bool_t          a1_Hlt1Phys_Dec;
   Bool_t          a1_Hlt1Phys_TIS;
   Bool_t          a1_Hlt1Phys_TOS;
   Bool_t          a1_Hlt2Global_Dec;
   Bool_t          a1_Hlt2Global_TIS;
   Bool_t          a1_Hlt2Global_TOS;
   Bool_t          a1_Hlt2Phys_Dec;
   Bool_t          a1_Hlt2Phys_TIS;
   Bool_t          a1_Hlt2Phys_TOS;
   Bool_t          a1_L0HadronDecision_Dec;
   Bool_t          a1_L0HadronDecision_TIS;
   Bool_t          a1_L0HadronDecision_TOS;
   Bool_t          a1_L0MuonDecision_Dec;
   Bool_t          a1_L0MuonDecision_TIS;
   Bool_t          a1_L0MuonDecision_TOS;
   Bool_t          a1_L0ElectronDecision_Dec;
   Bool_t          a1_L0ElectronDecision_TIS;
   Bool_t          a1_L0ElectronDecision_TOS;
   Bool_t          a1_L0PhotonDecision_Dec;
   Bool_t          a1_L0PhotonDecision_TIS;
   Bool_t          a1_L0PhotonDecision_TOS;
   Bool_t          a1_L0DiMuonDecision_Dec;
   Bool_t          a1_L0DiMuonDecision_TIS;
   Bool_t          a1_L0DiMuonDecision_TOS;
   Bool_t          a1_L0DiHadronDecision_Dec;
   Bool_t          a1_L0DiHadronDecision_TIS;
   Bool_t          a1_L0DiHadronDecision_TOS;
   Bool_t          a1_Hlt1TrackAllL0Decision_Dec;
   Bool_t          a1_Hlt1TrackAllL0Decision_TIS;
   Bool_t          a1_Hlt1TrackAllL0Decision_TOS;
   Bool_t          a1_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          a1_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          a1_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          a1_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          a1_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          a1_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          a1_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          a1_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          a1_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          a1_Hlt2IncPhiDecision_Dec;
   Bool_t          a1_Hlt2IncPhiDecision_TIS;
   Bool_t          a1_Hlt2IncPhiDecision_TOS;
   Double_t        LbKp_DTF_CHI2NDOF;
   Double_t        LbKp_DTF_CTAU;
   Double_t        LbKp_DTF_CTAUS;
   Double_t        LbKp_DTF_M;
   Double_t        LbKp_LOKI_DIRA;
   Double_t        LbKp_LOKI_FDCHI2;
   Double_t        LbKp_LOKI_FDS;
   Double_t        LbKp_LV01;
   Double_t        LbKp_LV02;
   Double_t        LbKp_Lc_DTF_CTAU;
   Double_t        LbKp_Lc_DTF_CTAUS;
   Double_t        LbKp_MC12TuneV2_ProbNNe;
   Double_t        LbKp_MC12TuneV2_ProbNNmu;
   Double_t        LbKp_MC12TuneV2_ProbNNpi;
   Double_t        LbKp_MC12TuneV2_ProbNNk;
   Double_t        LbKp_MC12TuneV2_ProbNNp;
   Double_t        LbKp_MC12TuneV2_ProbNNghost;
   Double_t        LbKp_MC12TuneV3_ProbNNe;
   Double_t        LbKp_MC12TuneV3_ProbNNmu;
   Double_t        LbKp_MC12TuneV3_ProbNNpi;
   Double_t        LbKp_MC12TuneV3_ProbNNk;
   Double_t        LbKp_MC12TuneV3_ProbNNp;
   Double_t        LbKp_MC12TuneV3_ProbNNghost;
   Double_t        LbKp_MC12TuneV4_ProbNNe;
   Double_t        LbKp_MC12TuneV4_ProbNNmu;
   Double_t        LbKp_MC12TuneV4_ProbNNpi;
   Double_t        LbKp_MC12TuneV4_ProbNNk;
   Double_t        LbKp_MC12TuneV4_ProbNNp;
   Double_t        LbKp_MC12TuneV4_ProbNNghost;
   Double_t        LbKp_MC15TuneV1_ProbNNe;
   Double_t        LbKp_MC15TuneV1_ProbNNmu;
   Double_t        LbKp_MC15TuneV1_ProbNNpi;
   Double_t        LbKp_MC15TuneV1_ProbNNk;
   Double_t        LbKp_MC15TuneV1_ProbNNp;
   Double_t        LbKp_MC15TuneV1_ProbNNghost;
   Double_t        LbKp_OWNPV_X;
   Double_t        LbKp_OWNPV_Y;
   Double_t        LbKp_OWNPV_Z;
   Double_t        LbKp_OWNPV_XERR;
   Double_t        LbKp_OWNPV_YERR;
   Double_t        LbKp_OWNPV_ZERR;
   Double_t        LbKp_OWNPV_CHI2;
   Int_t           LbKp_OWNPV_NDOF;
   Float_t         LbKp_OWNPV_COV_[3][3];
   Double_t        LbKp_IP_OWNPV;
   Double_t        LbKp_IPCHI2_OWNPV;
   Double_t        LbKp_ORIVX_X;
   Double_t        LbKp_ORIVX_Y;
   Double_t        LbKp_ORIVX_Z;
   Double_t        LbKp_ORIVX_XERR;
   Double_t        LbKp_ORIVX_YERR;
   Double_t        LbKp_ORIVX_ZERR;
   Double_t        LbKp_ORIVX_CHI2;
   Int_t           LbKp_ORIVX_NDOF;
   Float_t         LbKp_ORIVX_COV_[3][3];
   Double_t        LbKp_P;
   Double_t        LbKp_PT;
   Double_t        LbKp_PE;
   Double_t        LbKp_PX;
   Double_t        LbKp_PY;
   Double_t        LbKp_PZ;
   Double_t        LbKp_M;
   Int_t           LbKp_ID;
   Double_t        LbKp_PIDe;
   Double_t        LbKp_PIDmu;
   Double_t        LbKp_PIDK;
   Double_t        LbKp_PIDp;
   Double_t        LbKp_ProbNNe;
   Double_t        LbKp_ProbNNk;
   Double_t        LbKp_ProbNNp;
   Double_t        LbKp_ProbNNpi;
   Double_t        LbKp_ProbNNmu;
   Double_t        LbKp_ProbNNghost;
   Bool_t          LbKp_hasMuon;
   Bool_t          LbKp_isMuon;
   Bool_t          LbKp_hasRich;
   Bool_t          LbKp_hasCalo;
   Double_t        LbKp_PP_CombDLLe;
   Double_t        LbKp_PP_CombDLLmu;
   Double_t        LbKp_PP_CombDLLpi;
   Double_t        LbKp_PP_CombDLLk;
   Double_t        LbKp_PP_CombDLLp;
   Double_t        LbKp_PP_CombDLLd;
   Double_t        LbKp_PP_ProbNNe;
   Double_t        LbKp_PP_ProbNNmu;
   Double_t        LbKp_PP_ProbNNpi;
   Double_t        LbKp_PP_ProbNNk;
   Double_t        LbKp_PP_ProbNNp;
   Double_t        LbKp_PP_ProbNNghost;
   Bool_t          LbKp_L0Global_Dec;
   Bool_t          LbKp_L0Global_TIS;
   Bool_t          LbKp_L0Global_TOS;
   Bool_t          LbKp_Hlt1Global_Dec;
   Bool_t          LbKp_Hlt1Global_TIS;
   Bool_t          LbKp_Hlt1Global_TOS;
   Bool_t          LbKp_Hlt1Phys_Dec;
   Bool_t          LbKp_Hlt1Phys_TIS;
   Bool_t          LbKp_Hlt1Phys_TOS;
   Bool_t          LbKp_Hlt2Global_Dec;
   Bool_t          LbKp_Hlt2Global_TIS;
   Bool_t          LbKp_Hlt2Global_TOS;
   Bool_t          LbKp_Hlt2Phys_Dec;
   Bool_t          LbKp_Hlt2Phys_TIS;
   Bool_t          LbKp_Hlt2Phys_TOS;
   Bool_t          LbKp_L0HadronDecision_Dec;
   Bool_t          LbKp_L0HadronDecision_TIS;
   Bool_t          LbKp_L0HadronDecision_TOS;
   Bool_t          LbKp_L0MuonDecision_Dec;
   Bool_t          LbKp_L0MuonDecision_TIS;
   Bool_t          LbKp_L0MuonDecision_TOS;
   Bool_t          LbKp_L0ElectronDecision_Dec;
   Bool_t          LbKp_L0ElectronDecision_TIS;
   Bool_t          LbKp_L0ElectronDecision_TOS;
   Bool_t          LbKp_L0PhotonDecision_Dec;
   Bool_t          LbKp_L0PhotonDecision_TIS;
   Bool_t          LbKp_L0PhotonDecision_TOS;
   Bool_t          LbKp_L0DiMuonDecision_Dec;
   Bool_t          LbKp_L0DiMuonDecision_TIS;
   Bool_t          LbKp_L0DiMuonDecision_TOS;
   Bool_t          LbKp_L0DiHadronDecision_Dec;
   Bool_t          LbKp_L0DiHadronDecision_TIS;
   Bool_t          LbKp_L0DiHadronDecision_TOS;
   Bool_t          LbKp_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LbKp_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LbKp_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LbKp_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LbKp_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LbKp_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LbKp_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LbKp_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LbKp_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LbKp_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LbKp_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LbKp_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LbKp_Hlt2IncPhiDecision_Dec;
   Bool_t          LbKp_Hlt2IncPhiDecision_TIS;
   Bool_t          LbKp_Hlt2IncPhiDecision_TOS;
   Int_t           LbKp_TRACK_Type;
   Int_t           LbKp_TRACK_Key;
   Double_t        LbKp_TRACK_CHI2NDOF;
   Double_t        LbKp_TRACK_PCHI2;
   Double_t        LbKp_TRACK_MatchCHI2;
   Double_t        LbKp_TRACK_GhostProb;
   Double_t        LbKp_TRACK_CloneDist;
   Double_t        LbKp_TRACK_Likelihood;
   Double_t        LbKm_DTF_CHI2NDOF;
   Double_t        LbKm_DTF_CTAU;
   Double_t        LbKm_DTF_CTAUS;
   Double_t        LbKm_DTF_M;
   Double_t        LbKm_LOKI_DIRA;
   Double_t        LbKm_LOKI_FDCHI2;
   Double_t        LbKm_LOKI_FDS;
   Double_t        LbKm_LV01;
   Double_t        LbKm_LV02;
   Double_t        LbKm_Lc_DTF_CTAU;
   Double_t        LbKm_Lc_DTF_CTAUS;
   Double_t        LbKm_MC12TuneV2_ProbNNe;
   Double_t        LbKm_MC12TuneV2_ProbNNmu;
   Double_t        LbKm_MC12TuneV2_ProbNNpi;
   Double_t        LbKm_MC12TuneV2_ProbNNk;
   Double_t        LbKm_MC12TuneV2_ProbNNp;
   Double_t        LbKm_MC12TuneV2_ProbNNghost;
   Double_t        LbKm_MC12TuneV3_ProbNNe;
   Double_t        LbKm_MC12TuneV3_ProbNNmu;
   Double_t        LbKm_MC12TuneV3_ProbNNpi;
   Double_t        LbKm_MC12TuneV3_ProbNNk;
   Double_t        LbKm_MC12TuneV3_ProbNNp;
   Double_t        LbKm_MC12TuneV3_ProbNNghost;
   Double_t        LbKm_MC12TuneV4_ProbNNe;
   Double_t        LbKm_MC12TuneV4_ProbNNmu;
   Double_t        LbKm_MC12TuneV4_ProbNNpi;
   Double_t        LbKm_MC12TuneV4_ProbNNk;
   Double_t        LbKm_MC12TuneV4_ProbNNp;
   Double_t        LbKm_MC12TuneV4_ProbNNghost;
   Double_t        LbKm_MC15TuneV1_ProbNNe;
   Double_t        LbKm_MC15TuneV1_ProbNNmu;
   Double_t        LbKm_MC15TuneV1_ProbNNpi;
   Double_t        LbKm_MC15TuneV1_ProbNNk;
   Double_t        LbKm_MC15TuneV1_ProbNNp;
   Double_t        LbKm_MC15TuneV1_ProbNNghost;
   Double_t        LbKm_OWNPV_X;
   Double_t        LbKm_OWNPV_Y;
   Double_t        LbKm_OWNPV_Z;
   Double_t        LbKm_OWNPV_XERR;
   Double_t        LbKm_OWNPV_YERR;
   Double_t        LbKm_OWNPV_ZERR;
   Double_t        LbKm_OWNPV_CHI2;
   Int_t           LbKm_OWNPV_NDOF;
   Float_t         LbKm_OWNPV_COV_[3][3];
   Double_t        LbKm_IP_OWNPV;
   Double_t        LbKm_IPCHI2_OWNPV;
   Double_t        LbKm_ORIVX_X;
   Double_t        LbKm_ORIVX_Y;
   Double_t        LbKm_ORIVX_Z;
   Double_t        LbKm_ORIVX_XERR;
   Double_t        LbKm_ORIVX_YERR;
   Double_t        LbKm_ORIVX_ZERR;
   Double_t        LbKm_ORIVX_CHI2;
   Int_t           LbKm_ORIVX_NDOF;
   Float_t         LbKm_ORIVX_COV_[3][3];
   Double_t        LbKm_P;
   Double_t        LbKm_PT;
   Double_t        LbKm_PE;
   Double_t        LbKm_PX;
   Double_t        LbKm_PY;
   Double_t        LbKm_PZ;
   Double_t        LbKm_M;
   Int_t           LbKm_ID;
   Double_t        LbKm_PIDe;
   Double_t        LbKm_PIDmu;
   Double_t        LbKm_PIDK;
   Double_t        LbKm_PIDp;
   Double_t        LbKm_ProbNNe;
   Double_t        LbKm_ProbNNk;
   Double_t        LbKm_ProbNNp;
   Double_t        LbKm_ProbNNpi;
   Double_t        LbKm_ProbNNmu;
   Double_t        LbKm_ProbNNghost;
   Bool_t          LbKm_hasMuon;
   Bool_t          LbKm_isMuon;
   Bool_t          LbKm_hasRich;
   Bool_t          LbKm_hasCalo;
   Double_t        LbKm_PP_CombDLLe;
   Double_t        LbKm_PP_CombDLLmu;
   Double_t        LbKm_PP_CombDLLpi;
   Double_t        LbKm_PP_CombDLLk;
   Double_t        LbKm_PP_CombDLLp;
   Double_t        LbKm_PP_CombDLLd;
   Double_t        LbKm_PP_ProbNNe;
   Double_t        LbKm_PP_ProbNNmu;
   Double_t        LbKm_PP_ProbNNpi;
   Double_t        LbKm_PP_ProbNNk;
   Double_t        LbKm_PP_ProbNNp;
   Double_t        LbKm_PP_ProbNNghost;
   Bool_t          LbKm_L0Global_Dec;
   Bool_t          LbKm_L0Global_TIS;
   Bool_t          LbKm_L0Global_TOS;
   Bool_t          LbKm_Hlt1Global_Dec;
   Bool_t          LbKm_Hlt1Global_TIS;
   Bool_t          LbKm_Hlt1Global_TOS;
   Bool_t          LbKm_Hlt1Phys_Dec;
   Bool_t          LbKm_Hlt1Phys_TIS;
   Bool_t          LbKm_Hlt1Phys_TOS;
   Bool_t          LbKm_Hlt2Global_Dec;
   Bool_t          LbKm_Hlt2Global_TIS;
   Bool_t          LbKm_Hlt2Global_TOS;
   Bool_t          LbKm_Hlt2Phys_Dec;
   Bool_t          LbKm_Hlt2Phys_TIS;
   Bool_t          LbKm_Hlt2Phys_TOS;
   Bool_t          LbKm_L0HadronDecision_Dec;
   Bool_t          LbKm_L0HadronDecision_TIS;
   Bool_t          LbKm_L0HadronDecision_TOS;
   Bool_t          LbKm_L0MuonDecision_Dec;
   Bool_t          LbKm_L0MuonDecision_TIS;
   Bool_t          LbKm_L0MuonDecision_TOS;
   Bool_t          LbKm_L0ElectronDecision_Dec;
   Bool_t          LbKm_L0ElectronDecision_TIS;
   Bool_t          LbKm_L0ElectronDecision_TOS;
   Bool_t          LbKm_L0PhotonDecision_Dec;
   Bool_t          LbKm_L0PhotonDecision_TIS;
   Bool_t          LbKm_L0PhotonDecision_TOS;
   Bool_t          LbKm_L0DiMuonDecision_Dec;
   Bool_t          LbKm_L0DiMuonDecision_TIS;
   Bool_t          LbKm_L0DiMuonDecision_TOS;
   Bool_t          LbKm_L0DiHadronDecision_Dec;
   Bool_t          LbKm_L0DiHadronDecision_TIS;
   Bool_t          LbKm_L0DiHadronDecision_TOS;
   Bool_t          LbKm_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LbKm_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LbKm_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LbKm_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LbKm_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LbKm_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LbKm_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LbKm_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LbKm_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LbKm_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LbKm_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LbKm_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LbKm_Hlt2IncPhiDecision_Dec;
   Bool_t          LbKm_Hlt2IncPhiDecision_TIS;
   Bool_t          LbKm_Hlt2IncPhiDecision_TOS;
   Int_t           LbKm_TRACK_Type;
   Int_t           LbKm_TRACK_Key;
   Double_t        LbKm_TRACK_CHI2NDOF;
   Double_t        LbKm_TRACK_PCHI2;
   Double_t        LbKm_TRACK_MatchCHI2;
   Double_t        LbKm_TRACK_GhostProb;
   Double_t        LbKm_TRACK_CloneDist;
   Double_t        LbKm_TRACK_Likelihood;
   Double_t        LbPi_DTF_CHI2NDOF;
   Double_t        LbPi_DTF_CTAU;
   Double_t        LbPi_DTF_CTAUS;
   Double_t        LbPi_DTF_M;
   Double_t        LbPi_LOKI_DIRA;
   Double_t        LbPi_LOKI_FDCHI2;
   Double_t        LbPi_LOKI_FDS;
   Double_t        LbPi_LV01;
   Double_t        LbPi_LV02;
   Double_t        LbPi_Lc_DTF_CTAU;
   Double_t        LbPi_Lc_DTF_CTAUS;
   Double_t        LbPi_MC12TuneV2_ProbNNe;
   Double_t        LbPi_MC12TuneV2_ProbNNmu;
   Double_t        LbPi_MC12TuneV2_ProbNNpi;
   Double_t        LbPi_MC12TuneV2_ProbNNk;
   Double_t        LbPi_MC12TuneV2_ProbNNp;
   Double_t        LbPi_MC12TuneV2_ProbNNghost;
   Double_t        LbPi_MC12TuneV3_ProbNNe;
   Double_t        LbPi_MC12TuneV3_ProbNNmu;
   Double_t        LbPi_MC12TuneV3_ProbNNpi;
   Double_t        LbPi_MC12TuneV3_ProbNNk;
   Double_t        LbPi_MC12TuneV3_ProbNNp;
   Double_t        LbPi_MC12TuneV3_ProbNNghost;
   Double_t        LbPi_MC12TuneV4_ProbNNe;
   Double_t        LbPi_MC12TuneV4_ProbNNmu;
   Double_t        LbPi_MC12TuneV4_ProbNNpi;
   Double_t        LbPi_MC12TuneV4_ProbNNk;
   Double_t        LbPi_MC12TuneV4_ProbNNp;
   Double_t        LbPi_MC12TuneV4_ProbNNghost;
   Double_t        LbPi_MC15TuneV1_ProbNNe;
   Double_t        LbPi_MC15TuneV1_ProbNNmu;
   Double_t        LbPi_MC15TuneV1_ProbNNpi;
   Double_t        LbPi_MC15TuneV1_ProbNNk;
   Double_t        LbPi_MC15TuneV1_ProbNNp;
   Double_t        LbPi_MC15TuneV1_ProbNNghost;
   Double_t        LbPi_OWNPV_X;
   Double_t        LbPi_OWNPV_Y;
   Double_t        LbPi_OWNPV_Z;
   Double_t        LbPi_OWNPV_XERR;
   Double_t        LbPi_OWNPV_YERR;
   Double_t        LbPi_OWNPV_ZERR;
   Double_t        LbPi_OWNPV_CHI2;
   Int_t           LbPi_OWNPV_NDOF;
   Float_t         LbPi_OWNPV_COV_[3][3];
   Double_t        LbPi_IP_OWNPV;
   Double_t        LbPi_IPCHI2_OWNPV;
   Double_t        LbPi_ORIVX_X;
   Double_t        LbPi_ORIVX_Y;
   Double_t        LbPi_ORIVX_Z;
   Double_t        LbPi_ORIVX_XERR;
   Double_t        LbPi_ORIVX_YERR;
   Double_t        LbPi_ORIVX_ZERR;
   Double_t        LbPi_ORIVX_CHI2;
   Int_t           LbPi_ORIVX_NDOF;
   Float_t         LbPi_ORIVX_COV_[3][3];
   Double_t        LbPi_P;
   Double_t        LbPi_PT;
   Double_t        LbPi_PE;
   Double_t        LbPi_PX;
   Double_t        LbPi_PY;
   Double_t        LbPi_PZ;
   Double_t        LbPi_M;
   Int_t           LbPi_ID;
   Double_t        LbPi_PIDe;
   Double_t        LbPi_PIDmu;
   Double_t        LbPi_PIDK;
   Double_t        LbPi_PIDp;
   Double_t        LbPi_ProbNNe;
   Double_t        LbPi_ProbNNk;
   Double_t        LbPi_ProbNNp;
   Double_t        LbPi_ProbNNpi;
   Double_t        LbPi_ProbNNmu;
   Double_t        LbPi_ProbNNghost;
   Bool_t          LbPi_hasMuon;
   Bool_t          LbPi_isMuon;
   Bool_t          LbPi_hasRich;
   Bool_t          LbPi_hasCalo;
   Double_t        LbPi_PP_CombDLLe;
   Double_t        LbPi_PP_CombDLLmu;
   Double_t        LbPi_PP_CombDLLpi;
   Double_t        LbPi_PP_CombDLLk;
   Double_t        LbPi_PP_CombDLLp;
   Double_t        LbPi_PP_CombDLLd;
   Double_t        LbPi_PP_ProbNNe;
   Double_t        LbPi_PP_ProbNNmu;
   Double_t        LbPi_PP_ProbNNpi;
   Double_t        LbPi_PP_ProbNNk;
   Double_t        LbPi_PP_ProbNNp;
   Double_t        LbPi_PP_ProbNNghost;
   Bool_t          LbPi_L0Global_Dec;
   Bool_t          LbPi_L0Global_TIS;
   Bool_t          LbPi_L0Global_TOS;
   Bool_t          LbPi_Hlt1Global_Dec;
   Bool_t          LbPi_Hlt1Global_TIS;
   Bool_t          LbPi_Hlt1Global_TOS;
   Bool_t          LbPi_Hlt1Phys_Dec;
   Bool_t          LbPi_Hlt1Phys_TIS;
   Bool_t          LbPi_Hlt1Phys_TOS;
   Bool_t          LbPi_Hlt2Global_Dec;
   Bool_t          LbPi_Hlt2Global_TIS;
   Bool_t          LbPi_Hlt2Global_TOS;
   Bool_t          LbPi_Hlt2Phys_Dec;
   Bool_t          LbPi_Hlt2Phys_TIS;
   Bool_t          LbPi_Hlt2Phys_TOS;
   Bool_t          LbPi_L0HadronDecision_Dec;
   Bool_t          LbPi_L0HadronDecision_TIS;
   Bool_t          LbPi_L0HadronDecision_TOS;
   Bool_t          LbPi_L0MuonDecision_Dec;
   Bool_t          LbPi_L0MuonDecision_TIS;
   Bool_t          LbPi_L0MuonDecision_TOS;
   Bool_t          LbPi_L0ElectronDecision_Dec;
   Bool_t          LbPi_L0ElectronDecision_TIS;
   Bool_t          LbPi_L0ElectronDecision_TOS;
   Bool_t          LbPi_L0PhotonDecision_Dec;
   Bool_t          LbPi_L0PhotonDecision_TIS;
   Bool_t          LbPi_L0PhotonDecision_TOS;
   Bool_t          LbPi_L0DiMuonDecision_Dec;
   Bool_t          LbPi_L0DiMuonDecision_TIS;
   Bool_t          LbPi_L0DiMuonDecision_TOS;
   Bool_t          LbPi_L0DiHadronDecision_Dec;
   Bool_t          LbPi_L0DiHadronDecision_TIS;
   Bool_t          LbPi_L0DiHadronDecision_TOS;
   Bool_t          LbPi_Hlt1TrackAllL0Decision_Dec;
   Bool_t          LbPi_Hlt1TrackAllL0Decision_TIS;
   Bool_t          LbPi_Hlt1TrackAllL0Decision_TOS;
   Bool_t          LbPi_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          LbPi_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          LbPi_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          LbPi_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          LbPi_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          LbPi_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          LbPi_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          LbPi_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          LbPi_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          LbPi_Hlt2IncPhiDecision_Dec;
   Bool_t          LbPi_Hlt2IncPhiDecision_TIS;
   Bool_t          LbPi_Hlt2IncPhiDecision_TOS;
   Int_t           LbPi_TRACK_Type;
   Int_t           LbPi_TRACK_Key;
   Double_t        LbPi_TRACK_CHI2NDOF;
   Double_t        LbPi_TRACK_PCHI2;
   Double_t        LbPi_TRACK_MatchCHI2;
   Double_t        LbPi_TRACK_GhostProb;
   Double_t        LbPi_TRACK_CloneDist;
   Double_t        LbPi_TRACK_Likelihood;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0HadronDecision;
   Int_t           L0MuonDecision;
   Int_t           L0ElectronDecision;
   Int_t           L0PhotonDecision;
   Int_t           L0DiMuonDecision;
   Int_t           L0DiHadronDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1TrackAllL0Decision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2Topo2BodyBBDTDecision;
   Int_t           Hlt2Topo3BodyBBDTDecision;
   Int_t           Hlt2Topo4BodyBBDTDecision;
   Int_t           Hlt2IncPhiDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]

   // List of branches
   TBranch        *b_Lb_DTF_CHI2NDOF;   //!
   TBranch        *b_Lb_DTF_CTAU;   //!
   TBranch        *b_Lb_DTF_CTAUS;   //!
   TBranch        *b_Lb_DTF_M;   //!
   TBranch        *b_Lb_LOKI_DIRA;   //!
   TBranch        *b_Lb_LOKI_FDCHI2;   //!
   TBranch        *b_Lb_LOKI_FDS;   //!
   TBranch        *b_Lb_LV01;   //!
   TBranch        *b_Lb_LV02;   //!
   TBranch        *b_Lb_Lc_DTF_CTAU;   //!
   TBranch        *b_Lb_Lc_DTF_CTAUS;   //!
   TBranch        *b_Lb_ENDVERTEX_X;   //!
   TBranch        *b_Lb_ENDVERTEX_Y;   //!
   TBranch        *b_Lb_ENDVERTEX_Z;   //!
   TBranch        *b_Lb_ENDVERTEX_XERR;   //!
   TBranch        *b_Lb_ENDVERTEX_YERR;   //!
   TBranch        *b_Lb_ENDVERTEX_ZERR;   //!
   TBranch        *b_Lb_ENDVERTEX_CHI2;   //!
   TBranch        *b_Lb_ENDVERTEX_NDOF;   //!
   TBranch        *b_Lb_ENDVERTEX_COV_;   //!
   TBranch        *b_Lb_OWNPV_X;   //!
   TBranch        *b_Lb_OWNPV_Y;   //!
   TBranch        *b_Lb_OWNPV_Z;   //!
   TBranch        *b_Lb_OWNPV_XERR;   //!
   TBranch        *b_Lb_OWNPV_YERR;   //!
   TBranch        *b_Lb_OWNPV_ZERR;   //!
   TBranch        *b_Lb_OWNPV_CHI2;   //!
   TBranch        *b_Lb_OWNPV_NDOF;   //!
   TBranch        *b_Lb_OWNPV_COV_;   //!
   TBranch        *b_Lb_IP_OWNPV;   //!
   TBranch        *b_Lb_IPCHI2_OWNPV;   //!
   TBranch        *b_Lb_FD_OWNPV;   //!
   TBranch        *b_Lb_FDCHI2_OWNPV;   //!
   TBranch        *b_Lb_DIRA_OWNPV;   //!
   TBranch        *b_Lb_P;   //!
   TBranch        *b_Lb_PT;   //!
   TBranch        *b_Lb_PE;   //!
   TBranch        *b_Lb_PX;   //!
   TBranch        *b_Lb_PY;   //!
   TBranch        *b_Lb_PZ;   //!
   TBranch        *b_Lb_MM;   //!
   TBranch        *b_Lb_MMERR;   //!
   TBranch        *b_Lb_M;   //!
   TBranch        *b_Lb_ID;   //!
   TBranch        *b_Lb_TAU;   //!
   TBranch        *b_Lb_TAUERR;   //!
   TBranch        *b_Lb_TAUCHI2;   //!
   TBranch        *b_Lb_L0Global_Dec;   //!
   TBranch        *b_Lb_L0Global_TIS;   //!
   TBranch        *b_Lb_L0Global_TOS;   //!
   TBranch        *b_Lb_Hlt1Global_Dec;   //!
   TBranch        *b_Lb_Hlt1Global_TIS;   //!
   TBranch        *b_Lb_Hlt1Global_TOS;   //!
   TBranch        *b_Lb_Hlt1Phys_Dec;   //!
   TBranch        *b_Lb_Hlt1Phys_TIS;   //!
   TBranch        *b_Lb_Hlt1Phys_TOS;   //!
   TBranch        *b_Lb_Hlt2Global_Dec;   //!
   TBranch        *b_Lb_Hlt2Global_TIS;   //!
   TBranch        *b_Lb_Hlt2Global_TOS;   //!
   TBranch        *b_Lb_Hlt2Phys_Dec;   //!
   TBranch        *b_Lb_Hlt2Phys_TIS;   //!
   TBranch        *b_Lb_Hlt2Phys_TOS;   //!
   TBranch        *b_Lb_L0HadronDecision_Dec;   //!
   TBranch        *b_Lb_L0HadronDecision_TIS;   //!
   TBranch        *b_Lb_L0HadronDecision_TOS;   //!
   TBranch        *b_Lb_L0MuonDecision_Dec;   //!
   TBranch        *b_Lb_L0MuonDecision_TIS;   //!
   TBranch        *b_Lb_L0MuonDecision_TOS;   //!
   TBranch        *b_Lb_L0ElectronDecision_Dec;   //!
   TBranch        *b_Lb_L0ElectronDecision_TIS;   //!
   TBranch        *b_Lb_L0ElectronDecision_TOS;   //!
   TBranch        *b_Lb_L0PhotonDecision_Dec;   //!
   TBranch        *b_Lb_L0PhotonDecision_TIS;   //!
   TBranch        *b_Lb_L0PhotonDecision_TOS;   //!
   TBranch        *b_Lb_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Lb_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Lb_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Lb_L0DiHadronDecision_Dec;   //!
   TBranch        *b_Lb_L0DiHadronDecision_TIS;   //!
   TBranch        *b_Lb_L0DiHadronDecision_TOS;   //!
   TBranch        *b_Lb_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Lb_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Lb_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Lb_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lb_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lb_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lb_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lb_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lb_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lb_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lb_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lb_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lb_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_Lb_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_Lb_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__nPV;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__M;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__MERR;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__P;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__PERR;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__PV_key;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_ID;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__chi2;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__ctau;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__ctauErr;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__decayLength;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__decayLengthErr;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__nDOF;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__nIter;   //!
   TBranch        *b_Lb_DTF_LbLcMassCon__status;   //!
   TBranch        *b_Lb_DTF_LcMassCon__nPV;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__M;   //!
   TBranch        *b_Lb_DTF_LcMassCon__MERR;   //!
   TBranch        *b_Lb_DTF_LcMassCon__P;   //!
   TBranch        *b_Lb_DTF_LcMassCon__PERR;   //!
   TBranch        *b_Lb_DTF_LcMassCon__PV_key;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_ID;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PE;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PX;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PY;   //!
   TBranch        *b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PZ;   //!
   TBranch        *b_Lb_DTF_LcMassCon__chi2;   //!
   TBranch        *b_Lb_DTF_LcMassCon__ctau;   //!
   TBranch        *b_Lb_DTF_LcMassCon__ctauErr;   //!
   TBranch        *b_Lb_DTF_LcMassCon__decayLength;   //!
   TBranch        *b_Lb_DTF_LcMassCon__decayLengthErr;   //!
   TBranch        *b_Lb_DTF_LcMassCon__nDOF;   //!
   TBranch        *b_Lb_DTF_LcMassCon__nIter;   //!
   TBranch        *b_Lb_DTF_LcMassCon__status;   //!
   TBranch        *b_Lc_DTF_CHI2NDOF;   //!
   TBranch        *b_Lc_DTF_CTAU;   //!
   TBranch        *b_Lc_DTF_CTAUS;   //!
   TBranch        *b_Lc_DTF_M;   //!
   TBranch        *b_Lc_LOKI_DIRA;   //!
   TBranch        *b_Lc_LOKI_FDCHI2;   //!
   TBranch        *b_Lc_LOKI_FDS;   //!
   TBranch        *b_Lc_LV01;   //!
   TBranch        *b_Lc_LV02;   //!
   TBranch        *b_Lc_Lc_DTF_CTAU;   //!
   TBranch        *b_Lc_Lc_DTF_CTAUS;   //!
   TBranch        *b_Lc_ENDVERTEX_X;   //!
   TBranch        *b_Lc_ENDVERTEX_Y;   //!
   TBranch        *b_Lc_ENDVERTEX_Z;   //!
   TBranch        *b_Lc_ENDVERTEX_XERR;   //!
   TBranch        *b_Lc_ENDVERTEX_YERR;   //!
   TBranch        *b_Lc_ENDVERTEX_ZERR;   //!
   TBranch        *b_Lc_ENDVERTEX_CHI2;   //!
   TBranch        *b_Lc_ENDVERTEX_NDOF;   //!
   TBranch        *b_Lc_ENDVERTEX_COV_;   //!
   TBranch        *b_Lc_OWNPV_X;   //!
   TBranch        *b_Lc_OWNPV_Y;   //!
   TBranch        *b_Lc_OWNPV_Z;   //!
   TBranch        *b_Lc_OWNPV_XERR;   //!
   TBranch        *b_Lc_OWNPV_YERR;   //!
   TBranch        *b_Lc_OWNPV_ZERR;   //!
   TBranch        *b_Lc_OWNPV_CHI2;   //!
   TBranch        *b_Lc_OWNPV_NDOF;   //!
   TBranch        *b_Lc_OWNPV_COV_;   //!
   TBranch        *b_Lc_IP_OWNPV;   //!
   TBranch        *b_Lc_IPCHI2_OWNPV;   //!
   TBranch        *b_Lc_FD_OWNPV;   //!
   TBranch        *b_Lc_FDCHI2_OWNPV;   //!
   TBranch        *b_Lc_DIRA_OWNPV;   //!
   TBranch        *b_Lc_ORIVX_X;   //!
   TBranch        *b_Lc_ORIVX_Y;   //!
   TBranch        *b_Lc_ORIVX_Z;   //!
   TBranch        *b_Lc_ORIVX_XERR;   //!
   TBranch        *b_Lc_ORIVX_YERR;   //!
   TBranch        *b_Lc_ORIVX_ZERR;   //!
   TBranch        *b_Lc_ORIVX_CHI2;   //!
   TBranch        *b_Lc_ORIVX_NDOF;   //!
   TBranch        *b_Lc_ORIVX_COV_;   //!
   TBranch        *b_Lc_FD_ORIVX;   //!
   TBranch        *b_Lc_FDCHI2_ORIVX;   //!
   TBranch        *b_Lc_DIRA_ORIVX;   //!
   TBranch        *b_Lc_P;   //!
   TBranch        *b_Lc_PT;   //!
   TBranch        *b_Lc_PE;   //!
   TBranch        *b_Lc_PX;   //!
   TBranch        *b_Lc_PY;   //!
   TBranch        *b_Lc_PZ;   //!
   TBranch        *b_Lc_MM;   //!
   TBranch        *b_Lc_MMERR;   //!
   TBranch        *b_Lc_M;   //!
   TBranch        *b_Lc_ID;   //!
   TBranch        *b_Lc_TAU;   //!
   TBranch        *b_Lc_TAUERR;   //!
   TBranch        *b_Lc_TAUCHI2;   //!
   TBranch        *b_Lc_L0Global_Dec;   //!
   TBranch        *b_Lc_L0Global_TIS;   //!
   TBranch        *b_Lc_L0Global_TOS;   //!
   TBranch        *b_Lc_Hlt1Global_Dec;   //!
   TBranch        *b_Lc_Hlt1Global_TIS;   //!
   TBranch        *b_Lc_Hlt1Global_TOS;   //!
   TBranch        *b_Lc_Hlt1Phys_Dec;   //!
   TBranch        *b_Lc_Hlt1Phys_TIS;   //!
   TBranch        *b_Lc_Hlt1Phys_TOS;   //!
   TBranch        *b_Lc_Hlt2Global_Dec;   //!
   TBranch        *b_Lc_Hlt2Global_TIS;   //!
   TBranch        *b_Lc_Hlt2Global_TOS;   //!
   TBranch        *b_Lc_Hlt2Phys_Dec;   //!
   TBranch        *b_Lc_Hlt2Phys_TIS;   //!
   TBranch        *b_Lc_Hlt2Phys_TOS;   //!
   TBranch        *b_Lc_L0HadronDecision_Dec;   //!
   TBranch        *b_Lc_L0HadronDecision_TIS;   //!
   TBranch        *b_Lc_L0HadronDecision_TOS;   //!
   TBranch        *b_Lc_L0MuonDecision_Dec;   //!
   TBranch        *b_Lc_L0MuonDecision_TIS;   //!
   TBranch        *b_Lc_L0MuonDecision_TOS;   //!
   TBranch        *b_Lc_L0ElectronDecision_Dec;   //!
   TBranch        *b_Lc_L0ElectronDecision_TIS;   //!
   TBranch        *b_Lc_L0ElectronDecision_TOS;   //!
   TBranch        *b_Lc_L0PhotonDecision_Dec;   //!
   TBranch        *b_Lc_L0PhotonDecision_TIS;   //!
   TBranch        *b_Lc_L0PhotonDecision_TOS;   //!
   TBranch        *b_Lc_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Lc_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Lc_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Lc_L0DiHadronDecision_Dec;   //!
   TBranch        *b_Lc_L0DiHadronDecision_TIS;   //!
   TBranch        *b_Lc_L0DiHadronDecision_TOS;   //!
   TBranch        *b_Lc_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Lc_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Lc_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Lc_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lc_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lc_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lc_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lc_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lc_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lc_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Lc_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Lc_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Lc_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_Lc_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_Lc_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LcP_DTF_CHI2NDOF;   //!
   TBranch        *b_LcP_DTF_CTAU;   //!
   TBranch        *b_LcP_DTF_CTAUS;   //!
   TBranch        *b_LcP_DTF_M;   //!
   TBranch        *b_LcP_LOKI_DIRA;   //!
   TBranch        *b_LcP_LOKI_FDCHI2;   //!
   TBranch        *b_LcP_LOKI_FDS;   //!
   TBranch        *b_LcP_LV01;   //!
   TBranch        *b_LcP_LV02;   //!
   TBranch        *b_LcP_Lc_DTF_CTAU;   //!
   TBranch        *b_LcP_Lc_DTF_CTAUS;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LcP_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LcP_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LcP_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LcP_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LcP_OWNPV_X;   //!
   TBranch        *b_LcP_OWNPV_Y;   //!
   TBranch        *b_LcP_OWNPV_Z;   //!
   TBranch        *b_LcP_OWNPV_XERR;   //!
   TBranch        *b_LcP_OWNPV_YERR;   //!
   TBranch        *b_LcP_OWNPV_ZERR;   //!
   TBranch        *b_LcP_OWNPV_CHI2;   //!
   TBranch        *b_LcP_OWNPV_NDOF;   //!
   TBranch        *b_LcP_OWNPV_COV_;   //!
   TBranch        *b_LcP_IP_OWNPV;   //!
   TBranch        *b_LcP_IPCHI2_OWNPV;   //!
   TBranch        *b_LcP_ORIVX_X;   //!
   TBranch        *b_LcP_ORIVX_Y;   //!
   TBranch        *b_LcP_ORIVX_Z;   //!
   TBranch        *b_LcP_ORIVX_XERR;   //!
   TBranch        *b_LcP_ORIVX_YERR;   //!
   TBranch        *b_LcP_ORIVX_ZERR;   //!
   TBranch        *b_LcP_ORIVX_CHI2;   //!
   TBranch        *b_LcP_ORIVX_NDOF;   //!
   TBranch        *b_LcP_ORIVX_COV_;   //!
   TBranch        *b_LcP_P;   //!
   TBranch        *b_LcP_PT;   //!
   TBranch        *b_LcP_PE;   //!
   TBranch        *b_LcP_PX;   //!
   TBranch        *b_LcP_PY;   //!
   TBranch        *b_LcP_PZ;   //!
   TBranch        *b_LcP_M;   //!
   TBranch        *b_LcP_ID;   //!
   TBranch        *b_LcP_PIDe;   //!
   TBranch        *b_LcP_PIDmu;   //!
   TBranch        *b_LcP_PIDK;   //!
   TBranch        *b_LcP_PIDp;   //!
   TBranch        *b_LcP_ProbNNe;   //!
   TBranch        *b_LcP_ProbNNk;   //!
   TBranch        *b_LcP_ProbNNp;   //!
   TBranch        *b_LcP_ProbNNpi;   //!
   TBranch        *b_LcP_ProbNNmu;   //!
   TBranch        *b_LcP_ProbNNghost;   //!
   TBranch        *b_LcP_hasMuon;   //!
   TBranch        *b_LcP_isMuon;   //!
   TBranch        *b_LcP_hasRich;   //!
   TBranch        *b_LcP_hasCalo;   //!
   TBranch        *b_LcP_PP_CombDLLe;   //!
   TBranch        *b_LcP_PP_CombDLLmu;   //!
   TBranch        *b_LcP_PP_CombDLLpi;   //!
   TBranch        *b_LcP_PP_CombDLLk;   //!
   TBranch        *b_LcP_PP_CombDLLp;   //!
   TBranch        *b_LcP_PP_CombDLLd;   //!
   TBranch        *b_LcP_PP_ProbNNe;   //!
   TBranch        *b_LcP_PP_ProbNNmu;   //!
   TBranch        *b_LcP_PP_ProbNNpi;   //!
   TBranch        *b_LcP_PP_ProbNNk;   //!
   TBranch        *b_LcP_PP_ProbNNp;   //!
   TBranch        *b_LcP_PP_ProbNNghost;   //!
   TBranch        *b_LcP_L0Global_Dec;   //!
   TBranch        *b_LcP_L0Global_TIS;   //!
   TBranch        *b_LcP_L0Global_TOS;   //!
   TBranch        *b_LcP_Hlt1Global_Dec;   //!
   TBranch        *b_LcP_Hlt1Global_TIS;   //!
   TBranch        *b_LcP_Hlt1Global_TOS;   //!
   TBranch        *b_LcP_Hlt1Phys_Dec;   //!
   TBranch        *b_LcP_Hlt1Phys_TIS;   //!
   TBranch        *b_LcP_Hlt1Phys_TOS;   //!
   TBranch        *b_LcP_Hlt2Global_Dec;   //!
   TBranch        *b_LcP_Hlt2Global_TIS;   //!
   TBranch        *b_LcP_Hlt2Global_TOS;   //!
   TBranch        *b_LcP_Hlt2Phys_Dec;   //!
   TBranch        *b_LcP_Hlt2Phys_TIS;   //!
   TBranch        *b_LcP_Hlt2Phys_TOS;   //!
   TBranch        *b_LcP_L0HadronDecision_Dec;   //!
   TBranch        *b_LcP_L0HadronDecision_TIS;   //!
   TBranch        *b_LcP_L0HadronDecision_TOS;   //!
   TBranch        *b_LcP_L0MuonDecision_Dec;   //!
   TBranch        *b_LcP_L0MuonDecision_TIS;   //!
   TBranch        *b_LcP_L0MuonDecision_TOS;   //!
   TBranch        *b_LcP_L0ElectronDecision_Dec;   //!
   TBranch        *b_LcP_L0ElectronDecision_TIS;   //!
   TBranch        *b_LcP_L0ElectronDecision_TOS;   //!
   TBranch        *b_LcP_L0PhotonDecision_Dec;   //!
   TBranch        *b_LcP_L0PhotonDecision_TIS;   //!
   TBranch        *b_LcP_L0PhotonDecision_TOS;   //!
   TBranch        *b_LcP_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LcP_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LcP_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LcP_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LcP_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LcP_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LcP_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LcP_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LcP_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LcP_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcP_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcP_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcP_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcP_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcP_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcP_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcP_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcP_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcP_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LcP_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LcP_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LcP_TRACK_Type;   //!
   TBranch        *b_LcP_TRACK_Key;   //!
   TBranch        *b_LcP_TRACK_CHI2NDOF;   //!
   TBranch        *b_LcP_TRACK_PCHI2;   //!
   TBranch        *b_LcP_TRACK_MatchCHI2;   //!
   TBranch        *b_LcP_TRACK_GhostProb;   //!
   TBranch        *b_LcP_TRACK_CloneDist;   //!
   TBranch        *b_LcP_TRACK_Likelihood;   //!
   TBranch        *b_LcK_DTF_CHI2NDOF;   //!
   TBranch        *b_LcK_DTF_CTAU;   //!
   TBranch        *b_LcK_DTF_CTAUS;   //!
   TBranch        *b_LcK_DTF_M;   //!
   TBranch        *b_LcK_LOKI_DIRA;   //!
   TBranch        *b_LcK_LOKI_FDCHI2;   //!
   TBranch        *b_LcK_LOKI_FDS;   //!
   TBranch        *b_LcK_LV01;   //!
   TBranch        *b_LcK_LV02;   //!
   TBranch        *b_LcK_Lc_DTF_CTAU;   //!
   TBranch        *b_LcK_Lc_DTF_CTAUS;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LcK_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LcK_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LcK_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LcK_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LcK_OWNPV_X;   //!
   TBranch        *b_LcK_OWNPV_Y;   //!
   TBranch        *b_LcK_OWNPV_Z;   //!
   TBranch        *b_LcK_OWNPV_XERR;   //!
   TBranch        *b_LcK_OWNPV_YERR;   //!
   TBranch        *b_LcK_OWNPV_ZERR;   //!
   TBranch        *b_LcK_OWNPV_CHI2;   //!
   TBranch        *b_LcK_OWNPV_NDOF;   //!
   TBranch        *b_LcK_OWNPV_COV_;   //!
   TBranch        *b_LcK_IP_OWNPV;   //!
   TBranch        *b_LcK_IPCHI2_OWNPV;   //!
   TBranch        *b_LcK_ORIVX_X;   //!
   TBranch        *b_LcK_ORIVX_Y;   //!
   TBranch        *b_LcK_ORIVX_Z;   //!
   TBranch        *b_LcK_ORIVX_XERR;   //!
   TBranch        *b_LcK_ORIVX_YERR;   //!
   TBranch        *b_LcK_ORIVX_ZERR;   //!
   TBranch        *b_LcK_ORIVX_CHI2;   //!
   TBranch        *b_LcK_ORIVX_NDOF;   //!
   TBranch        *b_LcK_ORIVX_COV_;   //!
   TBranch        *b_LcK_P;   //!
   TBranch        *b_LcK_PT;   //!
   TBranch        *b_LcK_PE;   //!
   TBranch        *b_LcK_PX;   //!
   TBranch        *b_LcK_PY;   //!
   TBranch        *b_LcK_PZ;   //!
   TBranch        *b_LcK_M;   //!
   TBranch        *b_LcK_ID;   //!
   TBranch        *b_LcK_PIDe;   //!
   TBranch        *b_LcK_PIDmu;   //!
   TBranch        *b_LcK_PIDK;   //!
   TBranch        *b_LcK_PIDp;   //!
   TBranch        *b_LcK_ProbNNe;   //!
   TBranch        *b_LcK_ProbNNk;   //!
   TBranch        *b_LcK_ProbNNp;   //!
   TBranch        *b_LcK_ProbNNpi;   //!
   TBranch        *b_LcK_ProbNNmu;   //!
   TBranch        *b_LcK_ProbNNghost;   //!
   TBranch        *b_LcK_hasMuon;   //!
   TBranch        *b_LcK_isMuon;   //!
   TBranch        *b_LcK_hasRich;   //!
   TBranch        *b_LcK_hasCalo;   //!
   TBranch        *b_LcK_PP_CombDLLe;   //!
   TBranch        *b_LcK_PP_CombDLLmu;   //!
   TBranch        *b_LcK_PP_CombDLLpi;   //!
   TBranch        *b_LcK_PP_CombDLLk;   //!
   TBranch        *b_LcK_PP_CombDLLp;   //!
   TBranch        *b_LcK_PP_CombDLLd;   //!
   TBranch        *b_LcK_PP_ProbNNe;   //!
   TBranch        *b_LcK_PP_ProbNNmu;   //!
   TBranch        *b_LcK_PP_ProbNNpi;   //!
   TBranch        *b_LcK_PP_ProbNNk;   //!
   TBranch        *b_LcK_PP_ProbNNp;   //!
   TBranch        *b_LcK_PP_ProbNNghost;   //!
   TBranch        *b_LcK_L0Global_Dec;   //!
   TBranch        *b_LcK_L0Global_TIS;   //!
   TBranch        *b_LcK_L0Global_TOS;   //!
   TBranch        *b_LcK_Hlt1Global_Dec;   //!
   TBranch        *b_LcK_Hlt1Global_TIS;   //!
   TBranch        *b_LcK_Hlt1Global_TOS;   //!
   TBranch        *b_LcK_Hlt1Phys_Dec;   //!
   TBranch        *b_LcK_Hlt1Phys_TIS;   //!
   TBranch        *b_LcK_Hlt1Phys_TOS;   //!
   TBranch        *b_LcK_Hlt2Global_Dec;   //!
   TBranch        *b_LcK_Hlt2Global_TIS;   //!
   TBranch        *b_LcK_Hlt2Global_TOS;   //!
   TBranch        *b_LcK_Hlt2Phys_Dec;   //!
   TBranch        *b_LcK_Hlt2Phys_TIS;   //!
   TBranch        *b_LcK_Hlt2Phys_TOS;   //!
   TBranch        *b_LcK_L0HadronDecision_Dec;   //!
   TBranch        *b_LcK_L0HadronDecision_TIS;   //!
   TBranch        *b_LcK_L0HadronDecision_TOS;   //!
   TBranch        *b_LcK_L0MuonDecision_Dec;   //!
   TBranch        *b_LcK_L0MuonDecision_TIS;   //!
   TBranch        *b_LcK_L0MuonDecision_TOS;   //!
   TBranch        *b_LcK_L0ElectronDecision_Dec;   //!
   TBranch        *b_LcK_L0ElectronDecision_TIS;   //!
   TBranch        *b_LcK_L0ElectronDecision_TOS;   //!
   TBranch        *b_LcK_L0PhotonDecision_Dec;   //!
   TBranch        *b_LcK_L0PhotonDecision_TIS;   //!
   TBranch        *b_LcK_L0PhotonDecision_TOS;   //!
   TBranch        *b_LcK_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LcK_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LcK_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LcK_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LcK_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LcK_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LcK_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LcK_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LcK_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LcK_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcK_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcK_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcK_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcK_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcK_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcK_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcK_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcK_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcK_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LcK_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LcK_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LcK_TRACK_Type;   //!
   TBranch        *b_LcK_TRACK_Key;   //!
   TBranch        *b_LcK_TRACK_CHI2NDOF;   //!
   TBranch        *b_LcK_TRACK_PCHI2;   //!
   TBranch        *b_LcK_TRACK_MatchCHI2;   //!
   TBranch        *b_LcK_TRACK_GhostProb;   //!
   TBranch        *b_LcK_TRACK_CloneDist;   //!
   TBranch        *b_LcK_TRACK_Likelihood;   //!
   TBranch        *b_LcPi_DTF_CHI2NDOF;   //!
   TBranch        *b_LcPi_DTF_CTAU;   //!
   TBranch        *b_LcPi_DTF_CTAUS;   //!
   TBranch        *b_LcPi_DTF_M;   //!
   TBranch        *b_LcPi_LOKI_DIRA;   //!
   TBranch        *b_LcPi_LOKI_FDCHI2;   //!
   TBranch        *b_LcPi_LOKI_FDS;   //!
   TBranch        *b_LcPi_LV01;   //!
   TBranch        *b_LcPi_LV02;   //!
   TBranch        *b_LcPi_Lc_DTF_CTAU;   //!
   TBranch        *b_LcPi_Lc_DTF_CTAUS;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LcPi_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LcPi_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LcPi_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LcPi_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LcPi_OWNPV_X;   //!
   TBranch        *b_LcPi_OWNPV_Y;   //!
   TBranch        *b_LcPi_OWNPV_Z;   //!
   TBranch        *b_LcPi_OWNPV_XERR;   //!
   TBranch        *b_LcPi_OWNPV_YERR;   //!
   TBranch        *b_LcPi_OWNPV_ZERR;   //!
   TBranch        *b_LcPi_OWNPV_CHI2;   //!
   TBranch        *b_LcPi_OWNPV_NDOF;   //!
   TBranch        *b_LcPi_OWNPV_COV_;   //!
   TBranch        *b_LcPi_IP_OWNPV;   //!
   TBranch        *b_LcPi_IPCHI2_OWNPV;   //!
   TBranch        *b_LcPi_ORIVX_X;   //!
   TBranch        *b_LcPi_ORIVX_Y;   //!
   TBranch        *b_LcPi_ORIVX_Z;   //!
   TBranch        *b_LcPi_ORIVX_XERR;   //!
   TBranch        *b_LcPi_ORIVX_YERR;   //!
   TBranch        *b_LcPi_ORIVX_ZERR;   //!
   TBranch        *b_LcPi_ORIVX_CHI2;   //!
   TBranch        *b_LcPi_ORIVX_NDOF;   //!
   TBranch        *b_LcPi_ORIVX_COV_;   //!
   TBranch        *b_LcPi_P;   //!
   TBranch        *b_LcPi_PT;   //!
   TBranch        *b_LcPi_PE;   //!
   TBranch        *b_LcPi_PX;   //!
   TBranch        *b_LcPi_PY;   //!
   TBranch        *b_LcPi_PZ;   //!
   TBranch        *b_LcPi_M;   //!
   TBranch        *b_LcPi_ID;   //!
   TBranch        *b_LcPi_PIDe;   //!
   TBranch        *b_LcPi_PIDmu;   //!
   TBranch        *b_LcPi_PIDK;   //!
   TBranch        *b_LcPi_PIDp;   //!
   TBranch        *b_LcPi_ProbNNe;   //!
   TBranch        *b_LcPi_ProbNNk;   //!
   TBranch        *b_LcPi_ProbNNp;   //!
   TBranch        *b_LcPi_ProbNNpi;   //!
   TBranch        *b_LcPi_ProbNNmu;   //!
   TBranch        *b_LcPi_ProbNNghost;   //!
   TBranch        *b_LcPi_hasMuon;   //!
   TBranch        *b_LcPi_isMuon;   //!
   TBranch        *b_LcPi_hasRich;   //!
   TBranch        *b_LcPi_hasCalo;   //!
   TBranch        *b_LcPi_PP_CombDLLe;   //!
   TBranch        *b_LcPi_PP_CombDLLmu;   //!
   TBranch        *b_LcPi_PP_CombDLLpi;   //!
   TBranch        *b_LcPi_PP_CombDLLk;   //!
   TBranch        *b_LcPi_PP_CombDLLp;   //!
   TBranch        *b_LcPi_PP_CombDLLd;   //!
   TBranch        *b_LcPi_PP_ProbNNe;   //!
   TBranch        *b_LcPi_PP_ProbNNmu;   //!
   TBranch        *b_LcPi_PP_ProbNNpi;   //!
   TBranch        *b_LcPi_PP_ProbNNk;   //!
   TBranch        *b_LcPi_PP_ProbNNp;   //!
   TBranch        *b_LcPi_PP_ProbNNghost;   //!
   TBranch        *b_LcPi_L0Global_Dec;   //!
   TBranch        *b_LcPi_L0Global_TIS;   //!
   TBranch        *b_LcPi_L0Global_TOS;   //!
   TBranch        *b_LcPi_Hlt1Global_Dec;   //!
   TBranch        *b_LcPi_Hlt1Global_TIS;   //!
   TBranch        *b_LcPi_Hlt1Global_TOS;   //!
   TBranch        *b_LcPi_Hlt1Phys_Dec;   //!
   TBranch        *b_LcPi_Hlt1Phys_TIS;   //!
   TBranch        *b_LcPi_Hlt1Phys_TOS;   //!
   TBranch        *b_LcPi_Hlt2Global_Dec;   //!
   TBranch        *b_LcPi_Hlt2Global_TIS;   //!
   TBranch        *b_LcPi_Hlt2Global_TOS;   //!
   TBranch        *b_LcPi_Hlt2Phys_Dec;   //!
   TBranch        *b_LcPi_Hlt2Phys_TIS;   //!
   TBranch        *b_LcPi_Hlt2Phys_TOS;   //!
   TBranch        *b_LcPi_L0HadronDecision_Dec;   //!
   TBranch        *b_LcPi_L0HadronDecision_TIS;   //!
   TBranch        *b_LcPi_L0HadronDecision_TOS;   //!
   TBranch        *b_LcPi_L0MuonDecision_Dec;   //!
   TBranch        *b_LcPi_L0MuonDecision_TIS;   //!
   TBranch        *b_LcPi_L0MuonDecision_TOS;   //!
   TBranch        *b_LcPi_L0ElectronDecision_Dec;   //!
   TBranch        *b_LcPi_L0ElectronDecision_TIS;   //!
   TBranch        *b_LcPi_L0ElectronDecision_TOS;   //!
   TBranch        *b_LcPi_L0PhotonDecision_Dec;   //!
   TBranch        *b_LcPi_L0PhotonDecision_TIS;   //!
   TBranch        *b_LcPi_L0PhotonDecision_TOS;   //!
   TBranch        *b_LcPi_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LcPi_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LcPi_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LcPi_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LcPi_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LcPi_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LcPi_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LcPi_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LcPi_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LcPi_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcPi_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcPi_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcPi_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcPi_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcPi_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcPi_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LcPi_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LcPi_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LcPi_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LcPi_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LcPi_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LcPi_TRACK_Type;   //!
   TBranch        *b_LcPi_TRACK_Key;   //!
   TBranch        *b_LcPi_TRACK_CHI2NDOF;   //!
   TBranch        *b_LcPi_TRACK_PCHI2;   //!
   TBranch        *b_LcPi_TRACK_MatchCHI2;   //!
   TBranch        *b_LcPi_TRACK_GhostProb;   //!
   TBranch        *b_LcPi_TRACK_CloneDist;   //!
   TBranch        *b_LcPi_TRACK_Likelihood;   //!
   TBranch        *b_a1_DTF_CHI2NDOF;   //!
   TBranch        *b_a1_DTF_CTAU;   //!
   TBranch        *b_a1_DTF_CTAUS;   //!
   TBranch        *b_a1_DTF_M;   //!
   TBranch        *b_a1_LOKI_DIRA;   //!
   TBranch        *b_a1_LOKI_FDCHI2;   //!
   TBranch        *b_a1_LOKI_FDS;   //!
   TBranch        *b_a1_LV01;   //!
   TBranch        *b_a1_LV02;   //!
   TBranch        *b_a1_Lc_DTF_CTAU;   //!
   TBranch        *b_a1_Lc_DTF_CTAUS;   //!
   TBranch        *b_a1_ENDVERTEX_X;   //!
   TBranch        *b_a1_ENDVERTEX_Y;   //!
   TBranch        *b_a1_ENDVERTEX_Z;   //!
   TBranch        *b_a1_ENDVERTEX_XERR;   //!
   TBranch        *b_a1_ENDVERTEX_YERR;   //!
   TBranch        *b_a1_ENDVERTEX_ZERR;   //!
   TBranch        *b_a1_ENDVERTEX_CHI2;   //!
   TBranch        *b_a1_ENDVERTEX_NDOF;   //!
   TBranch        *b_a1_ENDVERTEX_COV_;   //!
   TBranch        *b_a1_OWNPV_X;   //!
   TBranch        *b_a1_OWNPV_Y;   //!
   TBranch        *b_a1_OWNPV_Z;   //!
   TBranch        *b_a1_OWNPV_XERR;   //!
   TBranch        *b_a1_OWNPV_YERR;   //!
   TBranch        *b_a1_OWNPV_ZERR;   //!
   TBranch        *b_a1_OWNPV_CHI2;   //!
   TBranch        *b_a1_OWNPV_NDOF;   //!
   TBranch        *b_a1_OWNPV_COV_;   //!
   TBranch        *b_a1_IP_OWNPV;   //!
   TBranch        *b_a1_IPCHI2_OWNPV;   //!
   TBranch        *b_a1_FD_OWNPV;   //!
   TBranch        *b_a1_FDCHI2_OWNPV;   //!
   TBranch        *b_a1_DIRA_OWNPV;   //!
   TBranch        *b_a1_ORIVX_X;   //!
   TBranch        *b_a1_ORIVX_Y;   //!
   TBranch        *b_a1_ORIVX_Z;   //!
   TBranch        *b_a1_ORIVX_XERR;   //!
   TBranch        *b_a1_ORIVX_YERR;   //!
   TBranch        *b_a1_ORIVX_ZERR;   //!
   TBranch        *b_a1_ORIVX_CHI2;   //!
   TBranch        *b_a1_ORIVX_NDOF;   //!
   TBranch        *b_a1_ORIVX_COV_;   //!
   TBranch        *b_a1_FD_ORIVX;   //!
   TBranch        *b_a1_FDCHI2_ORIVX;   //!
   TBranch        *b_a1_DIRA_ORIVX;   //!
   TBranch        *b_a1_P;   //!
   TBranch        *b_a1_PT;   //!
   TBranch        *b_a1_PE;   //!
   TBranch        *b_a1_PX;   //!
   TBranch        *b_a1_PY;   //!
   TBranch        *b_a1_PZ;   //!
   TBranch        *b_a1_MM;   //!
   TBranch        *b_a1_MMERR;   //!
   TBranch        *b_a1_M;   //!
   TBranch        *b_a1_ID;   //!
   TBranch        *b_a1_TAU;   //!
   TBranch        *b_a1_TAUERR;   //!
   TBranch        *b_a1_TAUCHI2;   //!
   TBranch        *b_a1_L0Global_Dec;   //!
   TBranch        *b_a1_L0Global_TIS;   //!
   TBranch        *b_a1_L0Global_TOS;   //!
   TBranch        *b_a1_Hlt1Global_Dec;   //!
   TBranch        *b_a1_Hlt1Global_TIS;   //!
   TBranch        *b_a1_Hlt1Global_TOS;   //!
   TBranch        *b_a1_Hlt1Phys_Dec;   //!
   TBranch        *b_a1_Hlt1Phys_TIS;   //!
   TBranch        *b_a1_Hlt1Phys_TOS;   //!
   TBranch        *b_a1_Hlt2Global_Dec;   //!
   TBranch        *b_a1_Hlt2Global_TIS;   //!
   TBranch        *b_a1_Hlt2Global_TOS;   //!
   TBranch        *b_a1_Hlt2Phys_Dec;   //!
   TBranch        *b_a1_Hlt2Phys_TIS;   //!
   TBranch        *b_a1_Hlt2Phys_TOS;   //!
   TBranch        *b_a1_L0HadronDecision_Dec;   //!
   TBranch        *b_a1_L0HadronDecision_TIS;   //!
   TBranch        *b_a1_L0HadronDecision_TOS;   //!
   TBranch        *b_a1_L0MuonDecision_Dec;   //!
   TBranch        *b_a1_L0MuonDecision_TIS;   //!
   TBranch        *b_a1_L0MuonDecision_TOS;   //!
   TBranch        *b_a1_L0ElectronDecision_Dec;   //!
   TBranch        *b_a1_L0ElectronDecision_TIS;   //!
   TBranch        *b_a1_L0ElectronDecision_TOS;   //!
   TBranch        *b_a1_L0PhotonDecision_Dec;   //!
   TBranch        *b_a1_L0PhotonDecision_TIS;   //!
   TBranch        *b_a1_L0PhotonDecision_TOS;   //!
   TBranch        *b_a1_L0DiMuonDecision_Dec;   //!
   TBranch        *b_a1_L0DiMuonDecision_TIS;   //!
   TBranch        *b_a1_L0DiMuonDecision_TOS;   //!
   TBranch        *b_a1_L0DiHadronDecision_Dec;   //!
   TBranch        *b_a1_L0DiHadronDecision_TIS;   //!
   TBranch        *b_a1_L0DiHadronDecision_TOS;   //!
   TBranch        *b_a1_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_a1_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_a1_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_a1_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_a1_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_a1_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_a1_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_a1_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_a1_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_a1_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_a1_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_a1_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_a1_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_a1_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_a1_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LbKp_DTF_CHI2NDOF;   //!
   TBranch        *b_LbKp_DTF_CTAU;   //!
   TBranch        *b_LbKp_DTF_CTAUS;   //!
   TBranch        *b_LbKp_DTF_M;   //!
   TBranch        *b_LbKp_LOKI_DIRA;   //!
   TBranch        *b_LbKp_LOKI_FDCHI2;   //!
   TBranch        *b_LbKp_LOKI_FDS;   //!
   TBranch        *b_LbKp_LV01;   //!
   TBranch        *b_LbKp_LV02;   //!
   TBranch        *b_LbKp_Lc_DTF_CTAU;   //!
   TBranch        *b_LbKp_Lc_DTF_CTAUS;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LbKp_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LbKp_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LbKp_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LbKp_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LbKp_OWNPV_X;   //!
   TBranch        *b_LbKp_OWNPV_Y;   //!
   TBranch        *b_LbKp_OWNPV_Z;   //!
   TBranch        *b_LbKp_OWNPV_XERR;   //!
   TBranch        *b_LbKp_OWNPV_YERR;   //!
   TBranch        *b_LbKp_OWNPV_ZERR;   //!
   TBranch        *b_LbKp_OWNPV_CHI2;   //!
   TBranch        *b_LbKp_OWNPV_NDOF;   //!
   TBranch        *b_LbKp_OWNPV_COV_;   //!
   TBranch        *b_LbKp_IP_OWNPV;   //!
   TBranch        *b_LbKp_IPCHI2_OWNPV;   //!
   TBranch        *b_LbKp_ORIVX_X;   //!
   TBranch        *b_LbKp_ORIVX_Y;   //!
   TBranch        *b_LbKp_ORIVX_Z;   //!
   TBranch        *b_LbKp_ORIVX_XERR;   //!
   TBranch        *b_LbKp_ORIVX_YERR;   //!
   TBranch        *b_LbKp_ORIVX_ZERR;   //!
   TBranch        *b_LbKp_ORIVX_CHI2;   //!
   TBranch        *b_LbKp_ORIVX_NDOF;   //!
   TBranch        *b_LbKp_ORIVX_COV_;   //!
   TBranch        *b_LbKp_P;   //!
   TBranch        *b_LbKp_PT;   //!
   TBranch        *b_LbKp_PE;   //!
   TBranch        *b_LbKp_PX;   //!
   TBranch        *b_LbKp_PY;   //!
   TBranch        *b_LbKp_PZ;   //!
   TBranch        *b_LbKp_M;   //!
   TBranch        *b_LbKp_ID;   //!
   TBranch        *b_LbKp_PIDe;   //!
   TBranch        *b_LbKp_PIDmu;   //!
   TBranch        *b_LbKp_PIDK;   //!
   TBranch        *b_LbKp_PIDp;   //!
   TBranch        *b_LbKp_ProbNNe;   //!
   TBranch        *b_LbKp_ProbNNk;   //!
   TBranch        *b_LbKp_ProbNNp;   //!
   TBranch        *b_LbKp_ProbNNpi;   //!
   TBranch        *b_LbKp_ProbNNmu;   //!
   TBranch        *b_LbKp_ProbNNghost;   //!
   TBranch        *b_LbKp_hasMuon;   //!
   TBranch        *b_LbKp_isMuon;   //!
   TBranch        *b_LbKp_hasRich;   //!
   TBranch        *b_LbKp_hasCalo;   //!
   TBranch        *b_LbKp_PP_CombDLLe;   //!
   TBranch        *b_LbKp_PP_CombDLLmu;   //!
   TBranch        *b_LbKp_PP_CombDLLpi;   //!
   TBranch        *b_LbKp_PP_CombDLLk;   //!
   TBranch        *b_LbKp_PP_CombDLLp;   //!
   TBranch        *b_LbKp_PP_CombDLLd;   //!
   TBranch        *b_LbKp_PP_ProbNNe;   //!
   TBranch        *b_LbKp_PP_ProbNNmu;   //!
   TBranch        *b_LbKp_PP_ProbNNpi;   //!
   TBranch        *b_LbKp_PP_ProbNNk;   //!
   TBranch        *b_LbKp_PP_ProbNNp;   //!
   TBranch        *b_LbKp_PP_ProbNNghost;   //!
   TBranch        *b_LbKp_L0Global_Dec;   //!
   TBranch        *b_LbKp_L0Global_TIS;   //!
   TBranch        *b_LbKp_L0Global_TOS;   //!
   TBranch        *b_LbKp_Hlt1Global_Dec;   //!
   TBranch        *b_LbKp_Hlt1Global_TIS;   //!
   TBranch        *b_LbKp_Hlt1Global_TOS;   //!
   TBranch        *b_LbKp_Hlt1Phys_Dec;   //!
   TBranch        *b_LbKp_Hlt1Phys_TIS;   //!
   TBranch        *b_LbKp_Hlt1Phys_TOS;   //!
   TBranch        *b_LbKp_Hlt2Global_Dec;   //!
   TBranch        *b_LbKp_Hlt2Global_TIS;   //!
   TBranch        *b_LbKp_Hlt2Global_TOS;   //!
   TBranch        *b_LbKp_Hlt2Phys_Dec;   //!
   TBranch        *b_LbKp_Hlt2Phys_TIS;   //!
   TBranch        *b_LbKp_Hlt2Phys_TOS;   //!
   TBranch        *b_LbKp_L0HadronDecision_Dec;   //!
   TBranch        *b_LbKp_L0HadronDecision_TIS;   //!
   TBranch        *b_LbKp_L0HadronDecision_TOS;   //!
   TBranch        *b_LbKp_L0MuonDecision_Dec;   //!
   TBranch        *b_LbKp_L0MuonDecision_TIS;   //!
   TBranch        *b_LbKp_L0MuonDecision_TOS;   //!
   TBranch        *b_LbKp_L0ElectronDecision_Dec;   //!
   TBranch        *b_LbKp_L0ElectronDecision_TIS;   //!
   TBranch        *b_LbKp_L0ElectronDecision_TOS;   //!
   TBranch        *b_LbKp_L0PhotonDecision_Dec;   //!
   TBranch        *b_LbKp_L0PhotonDecision_TIS;   //!
   TBranch        *b_LbKp_L0PhotonDecision_TOS;   //!
   TBranch        *b_LbKp_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LbKp_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LbKp_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LbKp_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LbKp_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LbKp_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LbKp_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LbKp_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LbKp_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LbKp_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKp_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKp_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKp_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKp_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKp_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKp_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKp_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKp_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKp_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LbKp_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LbKp_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LbKp_TRACK_Type;   //!
   TBranch        *b_LbKp_TRACK_Key;   //!
   TBranch        *b_LbKp_TRACK_CHI2NDOF;   //!
   TBranch        *b_LbKp_TRACK_PCHI2;   //!
   TBranch        *b_LbKp_TRACK_MatchCHI2;   //!
   TBranch        *b_LbKp_TRACK_GhostProb;   //!
   TBranch        *b_LbKp_TRACK_CloneDist;   //!
   TBranch        *b_LbKp_TRACK_Likelihood;   //!
   TBranch        *b_LbKm_DTF_CHI2NDOF;   //!
   TBranch        *b_LbKm_DTF_CTAU;   //!
   TBranch        *b_LbKm_DTF_CTAUS;   //!
   TBranch        *b_LbKm_DTF_M;   //!
   TBranch        *b_LbKm_LOKI_DIRA;   //!
   TBranch        *b_LbKm_LOKI_FDCHI2;   //!
   TBranch        *b_LbKm_LOKI_FDS;   //!
   TBranch        *b_LbKm_LV01;   //!
   TBranch        *b_LbKm_LV02;   //!
   TBranch        *b_LbKm_Lc_DTF_CTAU;   //!
   TBranch        *b_LbKm_Lc_DTF_CTAUS;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LbKm_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LbKm_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LbKm_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LbKm_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LbKm_OWNPV_X;   //!
   TBranch        *b_LbKm_OWNPV_Y;   //!
   TBranch        *b_LbKm_OWNPV_Z;   //!
   TBranch        *b_LbKm_OWNPV_XERR;   //!
   TBranch        *b_LbKm_OWNPV_YERR;   //!
   TBranch        *b_LbKm_OWNPV_ZERR;   //!
   TBranch        *b_LbKm_OWNPV_CHI2;   //!
   TBranch        *b_LbKm_OWNPV_NDOF;   //!
   TBranch        *b_LbKm_OWNPV_COV_;   //!
   TBranch        *b_LbKm_IP_OWNPV;   //!
   TBranch        *b_LbKm_IPCHI2_OWNPV;   //!
   TBranch        *b_LbKm_ORIVX_X;   //!
   TBranch        *b_LbKm_ORIVX_Y;   //!
   TBranch        *b_LbKm_ORIVX_Z;   //!
   TBranch        *b_LbKm_ORIVX_XERR;   //!
   TBranch        *b_LbKm_ORIVX_YERR;   //!
   TBranch        *b_LbKm_ORIVX_ZERR;   //!
   TBranch        *b_LbKm_ORIVX_CHI2;   //!
   TBranch        *b_LbKm_ORIVX_NDOF;   //!
   TBranch        *b_LbKm_ORIVX_COV_;   //!
   TBranch        *b_LbKm_P;   //!
   TBranch        *b_LbKm_PT;   //!
   TBranch        *b_LbKm_PE;   //!
   TBranch        *b_LbKm_PX;   //!
   TBranch        *b_LbKm_PY;   //!
   TBranch        *b_LbKm_PZ;   //!
   TBranch        *b_LbKm_M;   //!
   TBranch        *b_LbKm_ID;   //!
   TBranch        *b_LbKm_PIDe;   //!
   TBranch        *b_LbKm_PIDmu;   //!
   TBranch        *b_LbKm_PIDK;   //!
   TBranch        *b_LbKm_PIDp;   //!
   TBranch        *b_LbKm_ProbNNe;   //!
   TBranch        *b_LbKm_ProbNNk;   //!
   TBranch        *b_LbKm_ProbNNp;   //!
   TBranch        *b_LbKm_ProbNNpi;   //!
   TBranch        *b_LbKm_ProbNNmu;   //!
   TBranch        *b_LbKm_ProbNNghost;   //!
   TBranch        *b_LbKm_hasMuon;   //!
   TBranch        *b_LbKm_isMuon;   //!
   TBranch        *b_LbKm_hasRich;   //!
   TBranch        *b_LbKm_hasCalo;   //!
   TBranch        *b_LbKm_PP_CombDLLe;   //!
   TBranch        *b_LbKm_PP_CombDLLmu;   //!
   TBranch        *b_LbKm_PP_CombDLLpi;   //!
   TBranch        *b_LbKm_PP_CombDLLk;   //!
   TBranch        *b_LbKm_PP_CombDLLp;   //!
   TBranch        *b_LbKm_PP_CombDLLd;   //!
   TBranch        *b_LbKm_PP_ProbNNe;   //!
   TBranch        *b_LbKm_PP_ProbNNmu;   //!
   TBranch        *b_LbKm_PP_ProbNNpi;   //!
   TBranch        *b_LbKm_PP_ProbNNk;   //!
   TBranch        *b_LbKm_PP_ProbNNp;   //!
   TBranch        *b_LbKm_PP_ProbNNghost;   //!
   TBranch        *b_LbKm_L0Global_Dec;   //!
   TBranch        *b_LbKm_L0Global_TIS;   //!
   TBranch        *b_LbKm_L0Global_TOS;   //!
   TBranch        *b_LbKm_Hlt1Global_Dec;   //!
   TBranch        *b_LbKm_Hlt1Global_TIS;   //!
   TBranch        *b_LbKm_Hlt1Global_TOS;   //!
   TBranch        *b_LbKm_Hlt1Phys_Dec;   //!
   TBranch        *b_LbKm_Hlt1Phys_TIS;   //!
   TBranch        *b_LbKm_Hlt1Phys_TOS;   //!
   TBranch        *b_LbKm_Hlt2Global_Dec;   //!
   TBranch        *b_LbKm_Hlt2Global_TIS;   //!
   TBranch        *b_LbKm_Hlt2Global_TOS;   //!
   TBranch        *b_LbKm_Hlt2Phys_Dec;   //!
   TBranch        *b_LbKm_Hlt2Phys_TIS;   //!
   TBranch        *b_LbKm_Hlt2Phys_TOS;   //!
   TBranch        *b_LbKm_L0HadronDecision_Dec;   //!
   TBranch        *b_LbKm_L0HadronDecision_TIS;   //!
   TBranch        *b_LbKm_L0HadronDecision_TOS;   //!
   TBranch        *b_LbKm_L0MuonDecision_Dec;   //!
   TBranch        *b_LbKm_L0MuonDecision_TIS;   //!
   TBranch        *b_LbKm_L0MuonDecision_TOS;   //!
   TBranch        *b_LbKm_L0ElectronDecision_Dec;   //!
   TBranch        *b_LbKm_L0ElectronDecision_TIS;   //!
   TBranch        *b_LbKm_L0ElectronDecision_TOS;   //!
   TBranch        *b_LbKm_L0PhotonDecision_Dec;   //!
   TBranch        *b_LbKm_L0PhotonDecision_TIS;   //!
   TBranch        *b_LbKm_L0PhotonDecision_TOS;   //!
   TBranch        *b_LbKm_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LbKm_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LbKm_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LbKm_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LbKm_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LbKm_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LbKm_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LbKm_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LbKm_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LbKm_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKm_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKm_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKm_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKm_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKm_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKm_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbKm_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbKm_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbKm_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LbKm_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LbKm_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LbKm_TRACK_Type;   //!
   TBranch        *b_LbKm_TRACK_Key;   //!
   TBranch        *b_LbKm_TRACK_CHI2NDOF;   //!
   TBranch        *b_LbKm_TRACK_PCHI2;   //!
   TBranch        *b_LbKm_TRACK_MatchCHI2;   //!
   TBranch        *b_LbKm_TRACK_GhostProb;   //!
   TBranch        *b_LbKm_TRACK_CloneDist;   //!
   TBranch        *b_LbKm_TRACK_Likelihood;   //!
   TBranch        *b_LbPi_DTF_CHI2NDOF;   //!
   TBranch        *b_LbPi_DTF_CTAU;   //!
   TBranch        *b_LbPi_DTF_CTAUS;   //!
   TBranch        *b_LbPi_DTF_M;   //!
   TBranch        *b_LbPi_LOKI_DIRA;   //!
   TBranch        *b_LbPi_LOKI_FDCHI2;   //!
   TBranch        *b_LbPi_LOKI_FDS;   //!
   TBranch        *b_LbPi_LV01;   //!
   TBranch        *b_LbPi_LV02;   //!
   TBranch        *b_LbPi_Lc_DTF_CTAU;   //!
   TBranch        *b_LbPi_Lc_DTF_CTAUS;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_LbPi_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_LbPi_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_LbPi_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_LbPi_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_LbPi_OWNPV_X;   //!
   TBranch        *b_LbPi_OWNPV_Y;   //!
   TBranch        *b_LbPi_OWNPV_Z;   //!
   TBranch        *b_LbPi_OWNPV_XERR;   //!
   TBranch        *b_LbPi_OWNPV_YERR;   //!
   TBranch        *b_LbPi_OWNPV_ZERR;   //!
   TBranch        *b_LbPi_OWNPV_CHI2;   //!
   TBranch        *b_LbPi_OWNPV_NDOF;   //!
   TBranch        *b_LbPi_OWNPV_COV_;   //!
   TBranch        *b_LbPi_IP_OWNPV;   //!
   TBranch        *b_LbPi_IPCHI2_OWNPV;   //!
   TBranch        *b_LbPi_ORIVX_X;   //!
   TBranch        *b_LbPi_ORIVX_Y;   //!
   TBranch        *b_LbPi_ORIVX_Z;   //!
   TBranch        *b_LbPi_ORIVX_XERR;   //!
   TBranch        *b_LbPi_ORIVX_YERR;   //!
   TBranch        *b_LbPi_ORIVX_ZERR;   //!
   TBranch        *b_LbPi_ORIVX_CHI2;   //!
   TBranch        *b_LbPi_ORIVX_NDOF;   //!
   TBranch        *b_LbPi_ORIVX_COV_;   //!
   TBranch        *b_LbPi_P;   //!
   TBranch        *b_LbPi_PT;   //!
   TBranch        *b_LbPi_PE;   //!
   TBranch        *b_LbPi_PX;   //!
   TBranch        *b_LbPi_PY;   //!
   TBranch        *b_LbPi_PZ;   //!
   TBranch        *b_LbPi_M;   //!
   TBranch        *b_LbPi_ID;   //!
   TBranch        *b_LbPi_PIDe;   //!
   TBranch        *b_LbPi_PIDmu;   //!
   TBranch        *b_LbPi_PIDK;   //!
   TBranch        *b_LbPi_PIDp;   //!
   TBranch        *b_LbPi_ProbNNe;   //!
   TBranch        *b_LbPi_ProbNNk;   //!
   TBranch        *b_LbPi_ProbNNp;   //!
   TBranch        *b_LbPi_ProbNNpi;   //!
   TBranch        *b_LbPi_ProbNNmu;   //!
   TBranch        *b_LbPi_ProbNNghost;   //!
   TBranch        *b_LbPi_hasMuon;   //!
   TBranch        *b_LbPi_isMuon;   //!
   TBranch        *b_LbPi_hasRich;   //!
   TBranch        *b_LbPi_hasCalo;   //!
   TBranch        *b_LbPi_PP_CombDLLe;   //!
   TBranch        *b_LbPi_PP_CombDLLmu;   //!
   TBranch        *b_LbPi_PP_CombDLLpi;   //!
   TBranch        *b_LbPi_PP_CombDLLk;   //!
   TBranch        *b_LbPi_PP_CombDLLp;   //!
   TBranch        *b_LbPi_PP_CombDLLd;   //!
   TBranch        *b_LbPi_PP_ProbNNe;   //!
   TBranch        *b_LbPi_PP_ProbNNmu;   //!
   TBranch        *b_LbPi_PP_ProbNNpi;   //!
   TBranch        *b_LbPi_PP_ProbNNk;   //!
   TBranch        *b_LbPi_PP_ProbNNp;   //!
   TBranch        *b_LbPi_PP_ProbNNghost;   //!
   TBranch        *b_LbPi_L0Global_Dec;   //!
   TBranch        *b_LbPi_L0Global_TIS;   //!
   TBranch        *b_LbPi_L0Global_TOS;   //!
   TBranch        *b_LbPi_Hlt1Global_Dec;   //!
   TBranch        *b_LbPi_Hlt1Global_TIS;   //!
   TBranch        *b_LbPi_Hlt1Global_TOS;   //!
   TBranch        *b_LbPi_Hlt1Phys_Dec;   //!
   TBranch        *b_LbPi_Hlt1Phys_TIS;   //!
   TBranch        *b_LbPi_Hlt1Phys_TOS;   //!
   TBranch        *b_LbPi_Hlt2Global_Dec;   //!
   TBranch        *b_LbPi_Hlt2Global_TIS;   //!
   TBranch        *b_LbPi_Hlt2Global_TOS;   //!
   TBranch        *b_LbPi_Hlt2Phys_Dec;   //!
   TBranch        *b_LbPi_Hlt2Phys_TIS;   //!
   TBranch        *b_LbPi_Hlt2Phys_TOS;   //!
   TBranch        *b_LbPi_L0HadronDecision_Dec;   //!
   TBranch        *b_LbPi_L0HadronDecision_TIS;   //!
   TBranch        *b_LbPi_L0HadronDecision_TOS;   //!
   TBranch        *b_LbPi_L0MuonDecision_Dec;   //!
   TBranch        *b_LbPi_L0MuonDecision_TIS;   //!
   TBranch        *b_LbPi_L0MuonDecision_TOS;   //!
   TBranch        *b_LbPi_L0ElectronDecision_Dec;   //!
   TBranch        *b_LbPi_L0ElectronDecision_TIS;   //!
   TBranch        *b_LbPi_L0ElectronDecision_TOS;   //!
   TBranch        *b_LbPi_L0PhotonDecision_Dec;   //!
   TBranch        *b_LbPi_L0PhotonDecision_TIS;   //!
   TBranch        *b_LbPi_L0PhotonDecision_TOS;   //!
   TBranch        *b_LbPi_L0DiMuonDecision_Dec;   //!
   TBranch        *b_LbPi_L0DiMuonDecision_TIS;   //!
   TBranch        *b_LbPi_L0DiMuonDecision_TOS;   //!
   TBranch        *b_LbPi_L0DiHadronDecision_Dec;   //!
   TBranch        *b_LbPi_L0DiHadronDecision_TIS;   //!
   TBranch        *b_LbPi_L0DiHadronDecision_TOS;   //!
   TBranch        *b_LbPi_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_LbPi_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_LbPi_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_LbPi_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbPi_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbPi_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbPi_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbPi_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbPi_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbPi_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_LbPi_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_LbPi_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_LbPi_Hlt2IncPhiDecision_Dec;   //!
   TBranch        *b_LbPi_Hlt2IncPhiDecision_TIS;   //!
   TBranch        *b_LbPi_Hlt2IncPhiDecision_TOS;   //!
   TBranch        *b_LbPi_TRACK_Type;   //!
   TBranch        *b_LbPi_TRACK_Key;   //!
   TBranch        *b_LbPi_TRACK_CHI2NDOF;   //!
   TBranch        *b_LbPi_TRACK_PCHI2;   //!
   TBranch        *b_LbPi_TRACK_MatchCHI2;   //!
   TBranch        *b_LbPi_TRACK_GhostProb;   //!
   TBranch        *b_LbPi_TRACK_CloneDist;   //!
   TBranch        *b_LbPi_TRACK_Likelihood;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0HadronDecision;   //!
   TBranch        *b_L0MuonDecision;   //!
   TBranch        *b_L0ElectronDecision;   //!
   TBranch        *b_L0PhotonDecision;   //!
   TBranch        *b_L0DiMuonDecision;   //!
   TBranch        *b_L0DiHadronDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1TrackAllL0Decision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2Topo2BodyBBDTDecision;   //!
   TBranch        *b_Hlt2Topo3BodyBBDTDecision;   //!
   TBranch        *b_Hlt2Topo4BodyBBDTDecision;   //!
   TBranch        *b_Hlt2IncPhiDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!

   DecayTree(TTree *tree=0);
   virtual ~DecayTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
//   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

//#ifdef DecayTree_cxx
DecayTree::DecayTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Lb2Lckkpi_reduce.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Lb2Lckkpi_reduce.root");
      }
      f->GetObject("DecayTree",tree);

   }
   Init(tree);
}

DecayTree::~DecayTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DecayTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DecayTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DecayTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Lb_DTF_CHI2NDOF", &Lb_DTF_CHI2NDOF, &b_Lb_DTF_CHI2NDOF);
   fChain->SetBranchAddress("Lb_DTF_CTAU", &Lb_DTF_CTAU, &b_Lb_DTF_CTAU);
   fChain->SetBranchAddress("Lb_DTF_CTAUS", &Lb_DTF_CTAUS, &b_Lb_DTF_CTAUS);
   fChain->SetBranchAddress("Lb_DTF_M", &Lb_DTF_M, &b_Lb_DTF_M);
   fChain->SetBranchAddress("Lb_LOKI_DIRA", &Lb_LOKI_DIRA, &b_Lb_LOKI_DIRA);
   fChain->SetBranchAddress("Lb_LOKI_FDCHI2", &Lb_LOKI_FDCHI2, &b_Lb_LOKI_FDCHI2);
   fChain->SetBranchAddress("Lb_LOKI_FDS", &Lb_LOKI_FDS, &b_Lb_LOKI_FDS);
   fChain->SetBranchAddress("Lb_LV01", &Lb_LV01, &b_Lb_LV01);
   fChain->SetBranchAddress("Lb_LV02", &Lb_LV02, &b_Lb_LV02);
   fChain->SetBranchAddress("Lb_Lc_DTF_CTAU", &Lb_Lc_DTF_CTAU, &b_Lb_Lc_DTF_CTAU);
   fChain->SetBranchAddress("Lb_Lc_DTF_CTAUS", &Lb_Lc_DTF_CTAUS, &b_Lb_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("Lb_ENDVERTEX_X", &Lb_ENDVERTEX_X, &b_Lb_ENDVERTEX_X);
   fChain->SetBranchAddress("Lb_ENDVERTEX_Y", &Lb_ENDVERTEX_Y, &b_Lb_ENDVERTEX_Y);
   fChain->SetBranchAddress("Lb_ENDVERTEX_Z", &Lb_ENDVERTEX_Z, &b_Lb_ENDVERTEX_Z);
   fChain->SetBranchAddress("Lb_ENDVERTEX_XERR", &Lb_ENDVERTEX_XERR, &b_Lb_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Lb_ENDVERTEX_YERR", &Lb_ENDVERTEX_YERR, &b_Lb_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Lb_ENDVERTEX_ZERR", &Lb_ENDVERTEX_ZERR, &b_Lb_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Lb_ENDVERTEX_CHI2", &Lb_ENDVERTEX_CHI2, &b_Lb_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Lb_ENDVERTEX_NDOF", &Lb_ENDVERTEX_NDOF, &b_Lb_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Lb_ENDVERTEX_COV_", Lb_ENDVERTEX_COV_, &b_Lb_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Lb_OWNPV_X", &Lb_OWNPV_X, &b_Lb_OWNPV_X);
   fChain->SetBranchAddress("Lb_OWNPV_Y", &Lb_OWNPV_Y, &b_Lb_OWNPV_Y);
   fChain->SetBranchAddress("Lb_OWNPV_Z", &Lb_OWNPV_Z, &b_Lb_OWNPV_Z);
   fChain->SetBranchAddress("Lb_OWNPV_XERR", &Lb_OWNPV_XERR, &b_Lb_OWNPV_XERR);
   fChain->SetBranchAddress("Lb_OWNPV_YERR", &Lb_OWNPV_YERR, &b_Lb_OWNPV_YERR);
   fChain->SetBranchAddress("Lb_OWNPV_ZERR", &Lb_OWNPV_ZERR, &b_Lb_OWNPV_ZERR);
   fChain->SetBranchAddress("Lb_OWNPV_CHI2", &Lb_OWNPV_CHI2, &b_Lb_OWNPV_CHI2);
   fChain->SetBranchAddress("Lb_OWNPV_NDOF", &Lb_OWNPV_NDOF, &b_Lb_OWNPV_NDOF);
   fChain->SetBranchAddress("Lb_OWNPV_COV_", Lb_OWNPV_COV_, &b_Lb_OWNPV_COV_);
   fChain->SetBranchAddress("Lb_IP_OWNPV", &Lb_IP_OWNPV, &b_Lb_IP_OWNPV);
   fChain->SetBranchAddress("Lb_IPCHI2_OWNPV", &Lb_IPCHI2_OWNPV, &b_Lb_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Lb_FD_OWNPV", &Lb_FD_OWNPV, &b_Lb_FD_OWNPV);
   fChain->SetBranchAddress("Lb_FDCHI2_OWNPV", &Lb_FDCHI2_OWNPV, &b_Lb_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Lb_DIRA_OWNPV", &Lb_DIRA_OWNPV, &b_Lb_DIRA_OWNPV);
   fChain->SetBranchAddress("Lb_P", &Lb_P, &b_Lb_P);
   fChain->SetBranchAddress("Lb_PT", &Lb_PT, &b_Lb_PT);
   fChain->SetBranchAddress("Lb_PE", &Lb_PE, &b_Lb_PE);
   fChain->SetBranchAddress("Lb_PX", &Lb_PX, &b_Lb_PX);
   fChain->SetBranchAddress("Lb_PY", &Lb_PY, &b_Lb_PY);
   fChain->SetBranchAddress("Lb_PZ", &Lb_PZ, &b_Lb_PZ);
   fChain->SetBranchAddress("Lb_MM", &Lb_MM, &b_Lb_MM);
   fChain->SetBranchAddress("Lb_MMERR", &Lb_MMERR, &b_Lb_MMERR);
   fChain->SetBranchAddress("Lb_M", &Lb_M, &b_Lb_M);
   fChain->SetBranchAddress("Lb_ID", &Lb_ID, &b_Lb_ID);
   fChain->SetBranchAddress("Lb_TAU", &Lb_TAU, &b_Lb_TAU);
   fChain->SetBranchAddress("Lb_TAUERR", &Lb_TAUERR, &b_Lb_TAUERR);
   fChain->SetBranchAddress("Lb_TAUCHI2", &Lb_TAUCHI2, &b_Lb_TAUCHI2);
   fChain->SetBranchAddress("Lb_L0Global_Dec", &Lb_L0Global_Dec, &b_Lb_L0Global_Dec);
   fChain->SetBranchAddress("Lb_L0Global_TIS", &Lb_L0Global_TIS, &b_Lb_L0Global_TIS);
   fChain->SetBranchAddress("Lb_L0Global_TOS", &Lb_L0Global_TOS, &b_Lb_L0Global_TOS);
   fChain->SetBranchAddress("Lb_Hlt1Global_Dec", &Lb_Hlt1Global_Dec, &b_Lb_Hlt1Global_Dec);
   fChain->SetBranchAddress("Lb_Hlt1Global_TIS", &Lb_Hlt1Global_TIS, &b_Lb_Hlt1Global_TIS);
   fChain->SetBranchAddress("Lb_Hlt1Global_TOS", &Lb_Hlt1Global_TOS, &b_Lb_Hlt1Global_TOS);
   fChain->SetBranchAddress("Lb_Hlt1Phys_Dec", &Lb_Hlt1Phys_Dec, &b_Lb_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Lb_Hlt1Phys_TIS", &Lb_Hlt1Phys_TIS, &b_Lb_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Lb_Hlt1Phys_TOS", &Lb_Hlt1Phys_TOS, &b_Lb_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Lb_Hlt2Global_Dec", &Lb_Hlt2Global_Dec, &b_Lb_Hlt2Global_Dec);
   fChain->SetBranchAddress("Lb_Hlt2Global_TIS", &Lb_Hlt2Global_TIS, &b_Lb_Hlt2Global_TIS);
   fChain->SetBranchAddress("Lb_Hlt2Global_TOS", &Lb_Hlt2Global_TOS, &b_Lb_Hlt2Global_TOS);
   fChain->SetBranchAddress("Lb_Hlt2Phys_Dec", &Lb_Hlt2Phys_Dec, &b_Lb_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Lb_Hlt2Phys_TIS", &Lb_Hlt2Phys_TIS, &b_Lb_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Lb_Hlt2Phys_TOS", &Lb_Hlt2Phys_TOS, &b_Lb_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Lb_L0HadronDecision_Dec", &Lb_L0HadronDecision_Dec, &b_Lb_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Lb_L0HadronDecision_TIS", &Lb_L0HadronDecision_TIS, &b_Lb_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Lb_L0HadronDecision_TOS", &Lb_L0HadronDecision_TOS, &b_Lb_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Lb_L0MuonDecision_Dec", &Lb_L0MuonDecision_Dec, &b_Lb_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Lb_L0MuonDecision_TIS", &Lb_L0MuonDecision_TIS, &b_Lb_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Lb_L0MuonDecision_TOS", &Lb_L0MuonDecision_TOS, &b_Lb_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Lb_L0ElectronDecision_Dec", &Lb_L0ElectronDecision_Dec, &b_Lb_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Lb_L0ElectronDecision_TIS", &Lb_L0ElectronDecision_TIS, &b_Lb_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Lb_L0ElectronDecision_TOS", &Lb_L0ElectronDecision_TOS, &b_Lb_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Lb_L0PhotonDecision_Dec", &Lb_L0PhotonDecision_Dec, &b_Lb_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("Lb_L0PhotonDecision_TIS", &Lb_L0PhotonDecision_TIS, &b_Lb_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("Lb_L0PhotonDecision_TOS", &Lb_L0PhotonDecision_TOS, &b_Lb_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("Lb_L0DiMuonDecision_Dec", &Lb_L0DiMuonDecision_Dec, &b_Lb_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Lb_L0DiMuonDecision_TIS", &Lb_L0DiMuonDecision_TIS, &b_Lb_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Lb_L0DiMuonDecision_TOS", &Lb_L0DiMuonDecision_TOS, &b_Lb_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Lb_L0DiHadronDecision_Dec", &Lb_L0DiHadronDecision_Dec, &b_Lb_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("Lb_L0DiHadronDecision_TIS", &Lb_L0DiHadronDecision_TIS, &b_Lb_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("Lb_L0DiHadronDecision_TOS", &Lb_L0DiHadronDecision_TOS, &b_Lb_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("Lb_Hlt1TrackAllL0Decision_Dec", &Lb_Hlt1TrackAllL0Decision_Dec, &b_Lb_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Lb_Hlt1TrackAllL0Decision_TIS", &Lb_Hlt1TrackAllL0Decision_TIS, &b_Lb_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Lb_Hlt1TrackAllL0Decision_TOS", &Lb_Hlt1TrackAllL0Decision_TOS, &b_Lb_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Lb_Hlt2Topo2BodyBBDTDecision_Dec", &Lb_Hlt2Topo2BodyBBDTDecision_Dec, &b_Lb_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lb_Hlt2Topo2BodyBBDTDecision_TIS", &Lb_Hlt2Topo2BodyBBDTDecision_TIS, &b_Lb_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lb_Hlt2Topo2BodyBBDTDecision_TOS", &Lb_Hlt2Topo2BodyBBDTDecision_TOS, &b_Lb_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lb_Hlt2Topo3BodyBBDTDecision_Dec", &Lb_Hlt2Topo3BodyBBDTDecision_Dec, &b_Lb_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lb_Hlt2Topo3BodyBBDTDecision_TIS", &Lb_Hlt2Topo3BodyBBDTDecision_TIS, &b_Lb_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lb_Hlt2Topo3BodyBBDTDecision_TOS", &Lb_Hlt2Topo3BodyBBDTDecision_TOS, &b_Lb_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lb_Hlt2Topo4BodyBBDTDecision_Dec", &Lb_Hlt2Topo4BodyBBDTDecision_Dec, &b_Lb_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lb_Hlt2Topo4BodyBBDTDecision_TIS", &Lb_Hlt2Topo4BodyBBDTDecision_TIS, &b_Lb_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lb_Hlt2Topo4BodyBBDTDecision_TOS", &Lb_Hlt2Topo4BodyBBDTDecision_TOS, &b_Lb_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lb_Hlt2IncPhiDecision_Dec", &Lb_Hlt2IncPhiDecision_Dec, &b_Lb_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("Lb_Hlt2IncPhiDecision_TIS", &Lb_Hlt2IncPhiDecision_TIS, &b_Lb_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("Lb_Hlt2IncPhiDecision_TOS", &Lb_Hlt2IncPhiDecision_TOS, &b_Lb_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__nPV", &Lb_DTF_LbLcMassCon__nPV, &b_Lb_DTF_LbLcMassCon__nPV);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_ID", Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_ID, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PE", Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PE, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PX", Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PX, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PY", Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PY, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PZ", Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PZ, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_Kplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_ID", Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_ID, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PE", Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PE, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PX", Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PX, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PY", Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PY, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PZ", Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PZ, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_piplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_ID", Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_ID, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PE", Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PE, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PX", Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PX, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PY", Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PY, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PZ", Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PZ, &b_Lb_DTF_LbLcMassCon__Lambda_cplus_pplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__M", Lb_DTF_LbLcMassCon__M, &b_Lb_DTF_LbLcMassCon__M);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__MERR", Lb_DTF_LbLcMassCon__MERR, &b_Lb_DTF_LbLcMassCon__MERR);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__P", Lb_DTF_LbLcMassCon__P, &b_Lb_DTF_LbLcMassCon__P);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__PERR", Lb_DTF_LbLcMassCon__PERR, &b_Lb_DTF_LbLcMassCon__PERR);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__PV_key", Lb_DTF_LbLcMassCon__PV_key, &b_Lb_DTF_LbLcMassCon__PV_key);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_ID", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_ID, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_0_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_ID", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_ID, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ", Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_Kplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_ID", Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_ID, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE", Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX", Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY", Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ", Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ, &b_Lb_DTF_LbLcMassCon__a_1_1260_plus_piplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__chi2", Lb_DTF_LbLcMassCon__chi2, &b_Lb_DTF_LbLcMassCon__chi2);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__ctau", Lb_DTF_LbLcMassCon__ctau, &b_Lb_DTF_LbLcMassCon__ctau);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__ctauErr", Lb_DTF_LbLcMassCon__ctauErr, &b_Lb_DTF_LbLcMassCon__ctauErr);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__decayLength", Lb_DTF_LbLcMassCon__decayLength, &b_Lb_DTF_LbLcMassCon__decayLength);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__decayLengthErr", Lb_DTF_LbLcMassCon__decayLengthErr, &b_Lb_DTF_LbLcMassCon__decayLengthErr);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__nDOF", Lb_DTF_LbLcMassCon__nDOF, &b_Lb_DTF_LbLcMassCon__nDOF);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__nIter", Lb_DTF_LbLcMassCon__nIter, &b_Lb_DTF_LbLcMassCon__nIter);
   fChain->SetBranchAddress("Lb_DTF_LbLcMassCon__status", Lb_DTF_LbLcMassCon__status, &b_Lb_DTF_LbLcMassCon__status);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__nPV", &Lb_DTF_LcMassCon__nPV, &b_Lb_DTF_LcMassCon__nPV);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_Kplus_ID", Lb_DTF_LcMassCon__Lambda_cplus_Kplus_ID, &b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PE", Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PE, &b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PX", Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PX, &b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PY", Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PY, &b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PZ", Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PZ, &b_Lb_DTF_LcMassCon__Lambda_cplus_Kplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_piplus_ID", Lb_DTF_LcMassCon__Lambda_cplus_piplus_ID, &b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_piplus_PE", Lb_DTF_LcMassCon__Lambda_cplus_piplus_PE, &b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_piplus_PX", Lb_DTF_LcMassCon__Lambda_cplus_piplus_PX, &b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_piplus_PY", Lb_DTF_LcMassCon__Lambda_cplus_piplus_PY, &b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_piplus_PZ", Lb_DTF_LcMassCon__Lambda_cplus_piplus_PZ, &b_Lb_DTF_LcMassCon__Lambda_cplus_piplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_pplus_ID", Lb_DTF_LcMassCon__Lambda_cplus_pplus_ID, &b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_pplus_PE", Lb_DTF_LcMassCon__Lambda_cplus_pplus_PE, &b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_pplus_PX", Lb_DTF_LcMassCon__Lambda_cplus_pplus_PX, &b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_pplus_PY", Lb_DTF_LcMassCon__Lambda_cplus_pplus_PY, &b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__Lambda_cplus_pplus_PZ", Lb_DTF_LcMassCon__Lambda_cplus_pplus_PZ, &b_Lb_DTF_LcMassCon__Lambda_cplus_pplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__M", Lb_DTF_LcMassCon__M, &b_Lb_DTF_LcMassCon__M);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__MERR", Lb_DTF_LcMassCon__MERR, &b_Lb_DTF_LcMassCon__MERR);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__P", Lb_DTF_LcMassCon__P, &b_Lb_DTF_LcMassCon__P);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__PERR", Lb_DTF_LcMassCon__PERR, &b_Lb_DTF_LcMassCon__PERR);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__PV_key", Lb_DTF_LcMassCon__PV_key, &b_Lb_DTF_LcMassCon__PV_key);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_ID", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_ID, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PE", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PE, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PX", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PX, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PY", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PY, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PZ", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PZ, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_0_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_ID", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_ID, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PE", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PE, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PX", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PX, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PY", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PY, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PZ", Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PZ, &b_Lb_DTF_LcMassCon__a_1_1260_plus_Kplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_piplus_ID", Lb_DTF_LcMassCon__a_1_1260_plus_piplus_ID, &b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_ID);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PE", Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PE, &b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PE);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PX", Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PX, &b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PX);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PY", Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PY, &b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PY);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PZ", Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PZ, &b_Lb_DTF_LcMassCon__a_1_1260_plus_piplus_PZ);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__chi2", Lb_DTF_LcMassCon__chi2, &b_Lb_DTF_LcMassCon__chi2);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__ctau", Lb_DTF_LcMassCon__ctau, &b_Lb_DTF_LcMassCon__ctau);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__ctauErr", Lb_DTF_LcMassCon__ctauErr, &b_Lb_DTF_LcMassCon__ctauErr);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__decayLength", Lb_DTF_LcMassCon__decayLength, &b_Lb_DTF_LcMassCon__decayLength);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__decayLengthErr", Lb_DTF_LcMassCon__decayLengthErr, &b_Lb_DTF_LcMassCon__decayLengthErr);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__nDOF", Lb_DTF_LcMassCon__nDOF, &b_Lb_DTF_LcMassCon__nDOF);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__nIter", Lb_DTF_LcMassCon__nIter, &b_Lb_DTF_LcMassCon__nIter);
   fChain->SetBranchAddress("Lb_DTF_LcMassCon__status", Lb_DTF_LcMassCon__status, &b_Lb_DTF_LcMassCon__status);
   fChain->SetBranchAddress("Lc_DTF_CHI2NDOF", &Lc_DTF_CHI2NDOF, &b_Lc_DTF_CHI2NDOF);
   fChain->SetBranchAddress("Lc_DTF_CTAU", &Lc_DTF_CTAU, &b_Lc_DTF_CTAU);
   fChain->SetBranchAddress("Lc_DTF_CTAUS", &Lc_DTF_CTAUS, &b_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("Lc_DTF_M", &Lc_DTF_M, &b_Lc_DTF_M);
   fChain->SetBranchAddress("Lc_LOKI_DIRA", &Lc_LOKI_DIRA, &b_Lc_LOKI_DIRA);
   fChain->SetBranchAddress("Lc_LOKI_FDCHI2", &Lc_LOKI_FDCHI2, &b_Lc_LOKI_FDCHI2);
   fChain->SetBranchAddress("Lc_LOKI_FDS", &Lc_LOKI_FDS, &b_Lc_LOKI_FDS);
   fChain->SetBranchAddress("Lc_LV01", &Lc_LV01, &b_Lc_LV01);
   fChain->SetBranchAddress("Lc_LV02", &Lc_LV02, &b_Lc_LV02);
   fChain->SetBranchAddress("Lc_Lc_DTF_CTAU", &Lc_Lc_DTF_CTAU, &b_Lc_Lc_DTF_CTAU);
   fChain->SetBranchAddress("Lc_Lc_DTF_CTAUS", &Lc_Lc_DTF_CTAUS, &b_Lc_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("Lc_ENDVERTEX_X", &Lc_ENDVERTEX_X, &b_Lc_ENDVERTEX_X);
   fChain->SetBranchAddress("Lc_ENDVERTEX_Y", &Lc_ENDVERTEX_Y, &b_Lc_ENDVERTEX_Y);
   fChain->SetBranchAddress("Lc_ENDVERTEX_Z", &Lc_ENDVERTEX_Z, &b_Lc_ENDVERTEX_Z);
   fChain->SetBranchAddress("Lc_ENDVERTEX_XERR", &Lc_ENDVERTEX_XERR, &b_Lc_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Lc_ENDVERTEX_YERR", &Lc_ENDVERTEX_YERR, &b_Lc_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Lc_ENDVERTEX_ZERR", &Lc_ENDVERTEX_ZERR, &b_Lc_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Lc_ENDVERTEX_CHI2", &Lc_ENDVERTEX_CHI2, &b_Lc_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Lc_ENDVERTEX_NDOF", &Lc_ENDVERTEX_NDOF, &b_Lc_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Lc_ENDVERTEX_COV_", Lc_ENDVERTEX_COV_, &b_Lc_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Lc_OWNPV_X", &Lc_OWNPV_X, &b_Lc_OWNPV_X);
   fChain->SetBranchAddress("Lc_OWNPV_Y", &Lc_OWNPV_Y, &b_Lc_OWNPV_Y);
   fChain->SetBranchAddress("Lc_OWNPV_Z", &Lc_OWNPV_Z, &b_Lc_OWNPV_Z);
   fChain->SetBranchAddress("Lc_OWNPV_XERR", &Lc_OWNPV_XERR, &b_Lc_OWNPV_XERR);
   fChain->SetBranchAddress("Lc_OWNPV_YERR", &Lc_OWNPV_YERR, &b_Lc_OWNPV_YERR);
   fChain->SetBranchAddress("Lc_OWNPV_ZERR", &Lc_OWNPV_ZERR, &b_Lc_OWNPV_ZERR);
   fChain->SetBranchAddress("Lc_OWNPV_CHI2", &Lc_OWNPV_CHI2, &b_Lc_OWNPV_CHI2);
   fChain->SetBranchAddress("Lc_OWNPV_NDOF", &Lc_OWNPV_NDOF, &b_Lc_OWNPV_NDOF);
   fChain->SetBranchAddress("Lc_OWNPV_COV_", Lc_OWNPV_COV_, &b_Lc_OWNPV_COV_);
   fChain->SetBranchAddress("Lc_IP_OWNPV", &Lc_IP_OWNPV, &b_Lc_IP_OWNPV);
   fChain->SetBranchAddress("Lc_IPCHI2_OWNPV", &Lc_IPCHI2_OWNPV, &b_Lc_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Lc_FD_OWNPV", &Lc_FD_OWNPV, &b_Lc_FD_OWNPV);
   fChain->SetBranchAddress("Lc_FDCHI2_OWNPV", &Lc_FDCHI2_OWNPV, &b_Lc_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Lc_DIRA_OWNPV", &Lc_DIRA_OWNPV, &b_Lc_DIRA_OWNPV);
   fChain->SetBranchAddress("Lc_ORIVX_X", &Lc_ORIVX_X, &b_Lc_ORIVX_X);
   fChain->SetBranchAddress("Lc_ORIVX_Y", &Lc_ORIVX_Y, &b_Lc_ORIVX_Y);
   fChain->SetBranchAddress("Lc_ORIVX_Z", &Lc_ORIVX_Z, &b_Lc_ORIVX_Z);
   fChain->SetBranchAddress("Lc_ORIVX_XERR", &Lc_ORIVX_XERR, &b_Lc_ORIVX_XERR);
   fChain->SetBranchAddress("Lc_ORIVX_YERR", &Lc_ORIVX_YERR, &b_Lc_ORIVX_YERR);
   fChain->SetBranchAddress("Lc_ORIVX_ZERR", &Lc_ORIVX_ZERR, &b_Lc_ORIVX_ZERR);
   fChain->SetBranchAddress("Lc_ORIVX_CHI2", &Lc_ORIVX_CHI2, &b_Lc_ORIVX_CHI2);
   fChain->SetBranchAddress("Lc_ORIVX_NDOF", &Lc_ORIVX_NDOF, &b_Lc_ORIVX_NDOF);
   fChain->SetBranchAddress("Lc_ORIVX_COV_", Lc_ORIVX_COV_, &b_Lc_ORIVX_COV_);
   fChain->SetBranchAddress("Lc_FD_ORIVX", &Lc_FD_ORIVX, &b_Lc_FD_ORIVX);
   fChain->SetBranchAddress("Lc_FDCHI2_ORIVX", &Lc_FDCHI2_ORIVX, &b_Lc_FDCHI2_ORIVX);
   fChain->SetBranchAddress("Lc_DIRA_ORIVX", &Lc_DIRA_ORIVX, &b_Lc_DIRA_ORIVX);
   fChain->SetBranchAddress("Lc_P", &Lc_P, &b_Lc_P);
   fChain->SetBranchAddress("Lc_PT", &Lc_PT, &b_Lc_PT);
   fChain->SetBranchAddress("Lc_PE", &Lc_PE, &b_Lc_PE);
   fChain->SetBranchAddress("Lc_PX", &Lc_PX, &b_Lc_PX);
   fChain->SetBranchAddress("Lc_PY", &Lc_PY, &b_Lc_PY);
   fChain->SetBranchAddress("Lc_PZ", &Lc_PZ, &b_Lc_PZ);
   fChain->SetBranchAddress("Lc_MM", &Lc_MM, &b_Lc_MM);
   fChain->SetBranchAddress("Lc_MMERR", &Lc_MMERR, &b_Lc_MMERR);
   fChain->SetBranchAddress("Lc_M", &Lc_M, &b_Lc_M);
   fChain->SetBranchAddress("Lc_ID", &Lc_ID, &b_Lc_ID);
   fChain->SetBranchAddress("Lc_TAU", &Lc_TAU, &b_Lc_TAU);
   fChain->SetBranchAddress("Lc_TAUERR", &Lc_TAUERR, &b_Lc_TAUERR);
   fChain->SetBranchAddress("Lc_TAUCHI2", &Lc_TAUCHI2, &b_Lc_TAUCHI2);
   fChain->SetBranchAddress("Lc_L0Global_Dec", &Lc_L0Global_Dec, &b_Lc_L0Global_Dec);
   fChain->SetBranchAddress("Lc_L0Global_TIS", &Lc_L0Global_TIS, &b_Lc_L0Global_TIS);
   fChain->SetBranchAddress("Lc_L0Global_TOS", &Lc_L0Global_TOS, &b_Lc_L0Global_TOS);
   fChain->SetBranchAddress("Lc_Hlt1Global_Dec", &Lc_Hlt1Global_Dec, &b_Lc_Hlt1Global_Dec);
   fChain->SetBranchAddress("Lc_Hlt1Global_TIS", &Lc_Hlt1Global_TIS, &b_Lc_Hlt1Global_TIS);
   fChain->SetBranchAddress("Lc_Hlt1Global_TOS", &Lc_Hlt1Global_TOS, &b_Lc_Hlt1Global_TOS);
   fChain->SetBranchAddress("Lc_Hlt1Phys_Dec", &Lc_Hlt1Phys_Dec, &b_Lc_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Lc_Hlt1Phys_TIS", &Lc_Hlt1Phys_TIS, &b_Lc_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Lc_Hlt1Phys_TOS", &Lc_Hlt1Phys_TOS, &b_Lc_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Lc_Hlt2Global_Dec", &Lc_Hlt2Global_Dec, &b_Lc_Hlt2Global_Dec);
   fChain->SetBranchAddress("Lc_Hlt2Global_TIS", &Lc_Hlt2Global_TIS, &b_Lc_Hlt2Global_TIS);
   fChain->SetBranchAddress("Lc_Hlt2Global_TOS", &Lc_Hlt2Global_TOS, &b_Lc_Hlt2Global_TOS);
   fChain->SetBranchAddress("Lc_Hlt2Phys_Dec", &Lc_Hlt2Phys_Dec, &b_Lc_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Lc_Hlt2Phys_TIS", &Lc_Hlt2Phys_TIS, &b_Lc_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Lc_Hlt2Phys_TOS", &Lc_Hlt2Phys_TOS, &b_Lc_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Lc_L0HadronDecision_Dec", &Lc_L0HadronDecision_Dec, &b_Lc_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Lc_L0HadronDecision_TIS", &Lc_L0HadronDecision_TIS, &b_Lc_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Lc_L0HadronDecision_TOS", &Lc_L0HadronDecision_TOS, &b_Lc_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Lc_L0MuonDecision_Dec", &Lc_L0MuonDecision_Dec, &b_Lc_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Lc_L0MuonDecision_TIS", &Lc_L0MuonDecision_TIS, &b_Lc_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Lc_L0MuonDecision_TOS", &Lc_L0MuonDecision_TOS, &b_Lc_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Lc_L0ElectronDecision_Dec", &Lc_L0ElectronDecision_Dec, &b_Lc_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Lc_L0ElectronDecision_TIS", &Lc_L0ElectronDecision_TIS, &b_Lc_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Lc_L0ElectronDecision_TOS", &Lc_L0ElectronDecision_TOS, &b_Lc_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Lc_L0PhotonDecision_Dec", &Lc_L0PhotonDecision_Dec, &b_Lc_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("Lc_L0PhotonDecision_TIS", &Lc_L0PhotonDecision_TIS, &b_Lc_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("Lc_L0PhotonDecision_TOS", &Lc_L0PhotonDecision_TOS, &b_Lc_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("Lc_L0DiMuonDecision_Dec", &Lc_L0DiMuonDecision_Dec, &b_Lc_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Lc_L0DiMuonDecision_TIS", &Lc_L0DiMuonDecision_TIS, &b_Lc_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Lc_L0DiMuonDecision_TOS", &Lc_L0DiMuonDecision_TOS, &b_Lc_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Lc_L0DiHadronDecision_Dec", &Lc_L0DiHadronDecision_Dec, &b_Lc_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("Lc_L0DiHadronDecision_TIS", &Lc_L0DiHadronDecision_TIS, &b_Lc_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("Lc_L0DiHadronDecision_TOS", &Lc_L0DiHadronDecision_TOS, &b_Lc_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("Lc_Hlt1TrackAllL0Decision_Dec", &Lc_Hlt1TrackAllL0Decision_Dec, &b_Lc_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Lc_Hlt1TrackAllL0Decision_TIS", &Lc_Hlt1TrackAllL0Decision_TIS, &b_Lc_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Lc_Hlt1TrackAllL0Decision_TOS", &Lc_Hlt1TrackAllL0Decision_TOS, &b_Lc_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Lc_Hlt2Topo2BodyBBDTDecision_Dec", &Lc_Hlt2Topo2BodyBBDTDecision_Dec, &b_Lc_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lc_Hlt2Topo2BodyBBDTDecision_TIS", &Lc_Hlt2Topo2BodyBBDTDecision_TIS, &b_Lc_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lc_Hlt2Topo2BodyBBDTDecision_TOS", &Lc_Hlt2Topo2BodyBBDTDecision_TOS, &b_Lc_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lc_Hlt2Topo3BodyBBDTDecision_Dec", &Lc_Hlt2Topo3BodyBBDTDecision_Dec, &b_Lc_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lc_Hlt2Topo3BodyBBDTDecision_TIS", &Lc_Hlt2Topo3BodyBBDTDecision_TIS, &b_Lc_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lc_Hlt2Topo3BodyBBDTDecision_TOS", &Lc_Hlt2Topo3BodyBBDTDecision_TOS, &b_Lc_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lc_Hlt2Topo4BodyBBDTDecision_Dec", &Lc_Hlt2Topo4BodyBBDTDecision_Dec, &b_Lc_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Lc_Hlt2Topo4BodyBBDTDecision_TIS", &Lc_Hlt2Topo4BodyBBDTDecision_TIS, &b_Lc_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Lc_Hlt2Topo4BodyBBDTDecision_TOS", &Lc_Hlt2Topo4BodyBBDTDecision_TOS, &b_Lc_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Lc_Hlt2IncPhiDecision_Dec", &Lc_Hlt2IncPhiDecision_Dec, &b_Lc_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("Lc_Hlt2IncPhiDecision_TIS", &Lc_Hlt2IncPhiDecision_TIS, &b_Lc_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("Lc_Hlt2IncPhiDecision_TOS", &Lc_Hlt2IncPhiDecision_TOS, &b_Lc_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LcP_DTF_CHI2NDOF", &LcP_DTF_CHI2NDOF, &b_LcP_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LcP_DTF_CTAU", &LcP_DTF_CTAU, &b_LcP_DTF_CTAU);
   fChain->SetBranchAddress("LcP_DTF_CTAUS", &LcP_DTF_CTAUS, &b_LcP_DTF_CTAUS);
   fChain->SetBranchAddress("LcP_DTF_M", &LcP_DTF_M, &b_LcP_DTF_M);
   fChain->SetBranchAddress("LcP_LOKI_DIRA", &LcP_LOKI_DIRA, &b_LcP_LOKI_DIRA);
   fChain->SetBranchAddress("LcP_LOKI_FDCHI2", &LcP_LOKI_FDCHI2, &b_LcP_LOKI_FDCHI2);
   fChain->SetBranchAddress("LcP_LOKI_FDS", &LcP_LOKI_FDS, &b_LcP_LOKI_FDS);
   fChain->SetBranchAddress("LcP_LV01", &LcP_LV01, &b_LcP_LV01);
   fChain->SetBranchAddress("LcP_LV02", &LcP_LV02, &b_LcP_LV02);
   fChain->SetBranchAddress("LcP_Lc_DTF_CTAU", &LcP_Lc_DTF_CTAU, &b_LcP_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LcP_Lc_DTF_CTAUS", &LcP_Lc_DTF_CTAUS, &b_LcP_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNe", &LcP_MC12TuneV2_ProbNNe, &b_LcP_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNmu", &LcP_MC12TuneV2_ProbNNmu, &b_LcP_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNpi", &LcP_MC12TuneV2_ProbNNpi, &b_LcP_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNk", &LcP_MC12TuneV2_ProbNNk, &b_LcP_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNp", &LcP_MC12TuneV2_ProbNNp, &b_LcP_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LcP_MC12TuneV2_ProbNNghost", &LcP_MC12TuneV2_ProbNNghost, &b_LcP_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNe", &LcP_MC12TuneV3_ProbNNe, &b_LcP_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNmu", &LcP_MC12TuneV3_ProbNNmu, &b_LcP_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNpi", &LcP_MC12TuneV3_ProbNNpi, &b_LcP_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNk", &LcP_MC12TuneV3_ProbNNk, &b_LcP_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNp", &LcP_MC12TuneV3_ProbNNp, &b_LcP_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LcP_MC12TuneV3_ProbNNghost", &LcP_MC12TuneV3_ProbNNghost, &b_LcP_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNe", &LcP_MC12TuneV4_ProbNNe, &b_LcP_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNmu", &LcP_MC12TuneV4_ProbNNmu, &b_LcP_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNpi", &LcP_MC12TuneV4_ProbNNpi, &b_LcP_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNk", &LcP_MC12TuneV4_ProbNNk, &b_LcP_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNp", &LcP_MC12TuneV4_ProbNNp, &b_LcP_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LcP_MC12TuneV4_ProbNNghost", &LcP_MC12TuneV4_ProbNNghost, &b_LcP_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNe", &LcP_MC15TuneV1_ProbNNe, &b_LcP_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNmu", &LcP_MC15TuneV1_ProbNNmu, &b_LcP_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNpi", &LcP_MC15TuneV1_ProbNNpi, &b_LcP_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNk", &LcP_MC15TuneV1_ProbNNk, &b_LcP_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNp", &LcP_MC15TuneV1_ProbNNp, &b_LcP_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LcP_MC15TuneV1_ProbNNghost", &LcP_MC15TuneV1_ProbNNghost, &b_LcP_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LcP_OWNPV_X", &LcP_OWNPV_X, &b_LcP_OWNPV_X);
   fChain->SetBranchAddress("LcP_OWNPV_Y", &LcP_OWNPV_Y, &b_LcP_OWNPV_Y);
   fChain->SetBranchAddress("LcP_OWNPV_Z", &LcP_OWNPV_Z, &b_LcP_OWNPV_Z);
   fChain->SetBranchAddress("LcP_OWNPV_XERR", &LcP_OWNPV_XERR, &b_LcP_OWNPV_XERR);
   fChain->SetBranchAddress("LcP_OWNPV_YERR", &LcP_OWNPV_YERR, &b_LcP_OWNPV_YERR);
   fChain->SetBranchAddress("LcP_OWNPV_ZERR", &LcP_OWNPV_ZERR, &b_LcP_OWNPV_ZERR);
   fChain->SetBranchAddress("LcP_OWNPV_CHI2", &LcP_OWNPV_CHI2, &b_LcP_OWNPV_CHI2);
   fChain->SetBranchAddress("LcP_OWNPV_NDOF", &LcP_OWNPV_NDOF, &b_LcP_OWNPV_NDOF);
   fChain->SetBranchAddress("LcP_OWNPV_COV_", LcP_OWNPV_COV_, &b_LcP_OWNPV_COV_);
   fChain->SetBranchAddress("LcP_IP_OWNPV", &LcP_IP_OWNPV, &b_LcP_IP_OWNPV);
   fChain->SetBranchAddress("LcP_IPCHI2_OWNPV", &LcP_IPCHI2_OWNPV, &b_LcP_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LcP_ORIVX_X", &LcP_ORIVX_X, &b_LcP_ORIVX_X);
   fChain->SetBranchAddress("LcP_ORIVX_Y", &LcP_ORIVX_Y, &b_LcP_ORIVX_Y);
   fChain->SetBranchAddress("LcP_ORIVX_Z", &LcP_ORIVX_Z, &b_LcP_ORIVX_Z);
   fChain->SetBranchAddress("LcP_ORIVX_XERR", &LcP_ORIVX_XERR, &b_LcP_ORIVX_XERR);
   fChain->SetBranchAddress("LcP_ORIVX_YERR", &LcP_ORIVX_YERR, &b_LcP_ORIVX_YERR);
   fChain->SetBranchAddress("LcP_ORIVX_ZERR", &LcP_ORIVX_ZERR, &b_LcP_ORIVX_ZERR);
   fChain->SetBranchAddress("LcP_ORIVX_CHI2", &LcP_ORIVX_CHI2, &b_LcP_ORIVX_CHI2);
   fChain->SetBranchAddress("LcP_ORIVX_NDOF", &LcP_ORIVX_NDOF, &b_LcP_ORIVX_NDOF);
   fChain->SetBranchAddress("LcP_ORIVX_COV_", LcP_ORIVX_COV_, &b_LcP_ORIVX_COV_);
   fChain->SetBranchAddress("LcP_P", &LcP_P, &b_LcP_P);
   fChain->SetBranchAddress("LcP_PT", &LcP_PT, &b_LcP_PT);
   fChain->SetBranchAddress("LcP_PE", &LcP_PE, &b_LcP_PE);
   fChain->SetBranchAddress("LcP_PX", &LcP_PX, &b_LcP_PX);
   fChain->SetBranchAddress("LcP_PY", &LcP_PY, &b_LcP_PY);
   fChain->SetBranchAddress("LcP_PZ", &LcP_PZ, &b_LcP_PZ);
   fChain->SetBranchAddress("LcP_M", &LcP_M, &b_LcP_M);
   fChain->SetBranchAddress("LcP_ID", &LcP_ID, &b_LcP_ID);
   fChain->SetBranchAddress("LcP_PIDe", &LcP_PIDe, &b_LcP_PIDe);
   fChain->SetBranchAddress("LcP_PIDmu", &LcP_PIDmu, &b_LcP_PIDmu);
   fChain->SetBranchAddress("LcP_PIDK", &LcP_PIDK, &b_LcP_PIDK);
   fChain->SetBranchAddress("LcP_PIDp", &LcP_PIDp, &b_LcP_PIDp);
   fChain->SetBranchAddress("LcP_ProbNNe", &LcP_ProbNNe, &b_LcP_ProbNNe);
   fChain->SetBranchAddress("LcP_ProbNNk", &LcP_ProbNNk, &b_LcP_ProbNNk);
   fChain->SetBranchAddress("LcP_ProbNNp", &LcP_ProbNNp, &b_LcP_ProbNNp);
   fChain->SetBranchAddress("LcP_ProbNNpi", &LcP_ProbNNpi, &b_LcP_ProbNNpi);
   fChain->SetBranchAddress("LcP_ProbNNmu", &LcP_ProbNNmu, &b_LcP_ProbNNmu);
   fChain->SetBranchAddress("LcP_ProbNNghost", &LcP_ProbNNghost, &b_LcP_ProbNNghost);
   fChain->SetBranchAddress("LcP_hasMuon", &LcP_hasMuon, &b_LcP_hasMuon);
   fChain->SetBranchAddress("LcP_isMuon", &LcP_isMuon, &b_LcP_isMuon);
   fChain->SetBranchAddress("LcP_hasRich", &LcP_hasRich, &b_LcP_hasRich);
   fChain->SetBranchAddress("LcP_hasCalo", &LcP_hasCalo, &b_LcP_hasCalo);
   fChain->SetBranchAddress("LcP_PP_CombDLLe", &LcP_PP_CombDLLe, &b_LcP_PP_CombDLLe);
   fChain->SetBranchAddress("LcP_PP_CombDLLmu", &LcP_PP_CombDLLmu, &b_LcP_PP_CombDLLmu);
   fChain->SetBranchAddress("LcP_PP_CombDLLpi", &LcP_PP_CombDLLpi, &b_LcP_PP_CombDLLpi);
   fChain->SetBranchAddress("LcP_PP_CombDLLk", &LcP_PP_CombDLLk, &b_LcP_PP_CombDLLk);
   fChain->SetBranchAddress("LcP_PP_CombDLLp", &LcP_PP_CombDLLp, &b_LcP_PP_CombDLLp);
   fChain->SetBranchAddress("LcP_PP_CombDLLd", &LcP_PP_CombDLLd, &b_LcP_PP_CombDLLd);
   fChain->SetBranchAddress("LcP_PP_ProbNNe", &LcP_PP_ProbNNe, &b_LcP_PP_ProbNNe);
   fChain->SetBranchAddress("LcP_PP_ProbNNmu", &LcP_PP_ProbNNmu, &b_LcP_PP_ProbNNmu);
   fChain->SetBranchAddress("LcP_PP_ProbNNpi", &LcP_PP_ProbNNpi, &b_LcP_PP_ProbNNpi);
   fChain->SetBranchAddress("LcP_PP_ProbNNk", &LcP_PP_ProbNNk, &b_LcP_PP_ProbNNk);
   fChain->SetBranchAddress("LcP_PP_ProbNNp", &LcP_PP_ProbNNp, &b_LcP_PP_ProbNNp);
   fChain->SetBranchAddress("LcP_PP_ProbNNghost", &LcP_PP_ProbNNghost, &b_LcP_PP_ProbNNghost);
   fChain->SetBranchAddress("LcP_L0Global_Dec", &LcP_L0Global_Dec, &b_LcP_L0Global_Dec);
   fChain->SetBranchAddress("LcP_L0Global_TIS", &LcP_L0Global_TIS, &b_LcP_L0Global_TIS);
   fChain->SetBranchAddress("LcP_L0Global_TOS", &LcP_L0Global_TOS, &b_LcP_L0Global_TOS);
   fChain->SetBranchAddress("LcP_Hlt1Global_Dec", &LcP_Hlt1Global_Dec, &b_LcP_Hlt1Global_Dec);
   fChain->SetBranchAddress("LcP_Hlt1Global_TIS", &LcP_Hlt1Global_TIS, &b_LcP_Hlt1Global_TIS);
   fChain->SetBranchAddress("LcP_Hlt1Global_TOS", &LcP_Hlt1Global_TOS, &b_LcP_Hlt1Global_TOS);
   fChain->SetBranchAddress("LcP_Hlt1Phys_Dec", &LcP_Hlt1Phys_Dec, &b_LcP_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LcP_Hlt1Phys_TIS", &LcP_Hlt1Phys_TIS, &b_LcP_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LcP_Hlt1Phys_TOS", &LcP_Hlt1Phys_TOS, &b_LcP_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LcP_Hlt2Global_Dec", &LcP_Hlt2Global_Dec, &b_LcP_Hlt2Global_Dec);
   fChain->SetBranchAddress("LcP_Hlt2Global_TIS", &LcP_Hlt2Global_TIS, &b_LcP_Hlt2Global_TIS);
   fChain->SetBranchAddress("LcP_Hlt2Global_TOS", &LcP_Hlt2Global_TOS, &b_LcP_Hlt2Global_TOS);
   fChain->SetBranchAddress("LcP_Hlt2Phys_Dec", &LcP_Hlt2Phys_Dec, &b_LcP_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LcP_Hlt2Phys_TIS", &LcP_Hlt2Phys_TIS, &b_LcP_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LcP_Hlt2Phys_TOS", &LcP_Hlt2Phys_TOS, &b_LcP_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LcP_L0HadronDecision_Dec", &LcP_L0HadronDecision_Dec, &b_LcP_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LcP_L0HadronDecision_TIS", &LcP_L0HadronDecision_TIS, &b_LcP_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LcP_L0HadronDecision_TOS", &LcP_L0HadronDecision_TOS, &b_LcP_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LcP_L0MuonDecision_Dec", &LcP_L0MuonDecision_Dec, &b_LcP_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LcP_L0MuonDecision_TIS", &LcP_L0MuonDecision_TIS, &b_LcP_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LcP_L0MuonDecision_TOS", &LcP_L0MuonDecision_TOS, &b_LcP_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LcP_L0ElectronDecision_Dec", &LcP_L0ElectronDecision_Dec, &b_LcP_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LcP_L0ElectronDecision_TIS", &LcP_L0ElectronDecision_TIS, &b_LcP_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LcP_L0ElectronDecision_TOS", &LcP_L0ElectronDecision_TOS, &b_LcP_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LcP_L0PhotonDecision_Dec", &LcP_L0PhotonDecision_Dec, &b_LcP_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LcP_L0PhotonDecision_TIS", &LcP_L0PhotonDecision_TIS, &b_LcP_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LcP_L0PhotonDecision_TOS", &LcP_L0PhotonDecision_TOS, &b_LcP_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LcP_L0DiMuonDecision_Dec", &LcP_L0DiMuonDecision_Dec, &b_LcP_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LcP_L0DiMuonDecision_TIS", &LcP_L0DiMuonDecision_TIS, &b_LcP_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LcP_L0DiMuonDecision_TOS", &LcP_L0DiMuonDecision_TOS, &b_LcP_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LcP_L0DiHadronDecision_Dec", &LcP_L0DiHadronDecision_Dec, &b_LcP_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LcP_L0DiHadronDecision_TIS", &LcP_L0DiHadronDecision_TIS, &b_LcP_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LcP_L0DiHadronDecision_TOS", &LcP_L0DiHadronDecision_TOS, &b_LcP_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LcP_Hlt1TrackAllL0Decision_Dec", &LcP_Hlt1TrackAllL0Decision_Dec, &b_LcP_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LcP_Hlt1TrackAllL0Decision_TIS", &LcP_Hlt1TrackAllL0Decision_TIS, &b_LcP_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LcP_Hlt1TrackAllL0Decision_TOS", &LcP_Hlt1TrackAllL0Decision_TOS, &b_LcP_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LcP_Hlt2Topo2BodyBBDTDecision_Dec", &LcP_Hlt2Topo2BodyBBDTDecision_Dec, &b_LcP_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcP_Hlt2Topo2BodyBBDTDecision_TIS", &LcP_Hlt2Topo2BodyBBDTDecision_TIS, &b_LcP_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcP_Hlt2Topo2BodyBBDTDecision_TOS", &LcP_Hlt2Topo2BodyBBDTDecision_TOS, &b_LcP_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcP_Hlt2Topo3BodyBBDTDecision_Dec", &LcP_Hlt2Topo3BodyBBDTDecision_Dec, &b_LcP_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcP_Hlt2Topo3BodyBBDTDecision_TIS", &LcP_Hlt2Topo3BodyBBDTDecision_TIS, &b_LcP_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcP_Hlt2Topo3BodyBBDTDecision_TOS", &LcP_Hlt2Topo3BodyBBDTDecision_TOS, &b_LcP_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcP_Hlt2Topo4BodyBBDTDecision_Dec", &LcP_Hlt2Topo4BodyBBDTDecision_Dec, &b_LcP_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcP_Hlt2Topo4BodyBBDTDecision_TIS", &LcP_Hlt2Topo4BodyBBDTDecision_TIS, &b_LcP_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcP_Hlt2Topo4BodyBBDTDecision_TOS", &LcP_Hlt2Topo4BodyBBDTDecision_TOS, &b_LcP_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcP_Hlt2IncPhiDecision_Dec", &LcP_Hlt2IncPhiDecision_Dec, &b_LcP_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LcP_Hlt2IncPhiDecision_TIS", &LcP_Hlt2IncPhiDecision_TIS, &b_LcP_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LcP_Hlt2IncPhiDecision_TOS", &LcP_Hlt2IncPhiDecision_TOS, &b_LcP_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LcP_TRACK_Type", &LcP_TRACK_Type, &b_LcP_TRACK_Type);
   fChain->SetBranchAddress("LcP_TRACK_Key", &LcP_TRACK_Key, &b_LcP_TRACK_Key);
   fChain->SetBranchAddress("LcP_TRACK_CHI2NDOF", &LcP_TRACK_CHI2NDOF, &b_LcP_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LcP_TRACK_PCHI2", &LcP_TRACK_PCHI2, &b_LcP_TRACK_PCHI2);
   fChain->SetBranchAddress("LcP_TRACK_MatchCHI2", &LcP_TRACK_MatchCHI2, &b_LcP_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LcP_TRACK_GhostProb", &LcP_TRACK_GhostProb, &b_LcP_TRACK_GhostProb);
   fChain->SetBranchAddress("LcP_TRACK_CloneDist", &LcP_TRACK_CloneDist, &b_LcP_TRACK_CloneDist);
   fChain->SetBranchAddress("LcP_TRACK_Likelihood", &LcP_TRACK_Likelihood, &b_LcP_TRACK_Likelihood);
   fChain->SetBranchAddress("LcK_DTF_CHI2NDOF", &LcK_DTF_CHI2NDOF, &b_LcK_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LcK_DTF_CTAU", &LcK_DTF_CTAU, &b_LcK_DTF_CTAU);
   fChain->SetBranchAddress("LcK_DTF_CTAUS", &LcK_DTF_CTAUS, &b_LcK_DTF_CTAUS);
   fChain->SetBranchAddress("LcK_DTF_M", &LcK_DTF_M, &b_LcK_DTF_M);
   fChain->SetBranchAddress("LcK_LOKI_DIRA", &LcK_LOKI_DIRA, &b_LcK_LOKI_DIRA);
   fChain->SetBranchAddress("LcK_LOKI_FDCHI2", &LcK_LOKI_FDCHI2, &b_LcK_LOKI_FDCHI2);
   fChain->SetBranchAddress("LcK_LOKI_FDS", &LcK_LOKI_FDS, &b_LcK_LOKI_FDS);
   fChain->SetBranchAddress("LcK_LV01", &LcK_LV01, &b_LcK_LV01);
   fChain->SetBranchAddress("LcK_LV02", &LcK_LV02, &b_LcK_LV02);
   fChain->SetBranchAddress("LcK_Lc_DTF_CTAU", &LcK_Lc_DTF_CTAU, &b_LcK_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LcK_Lc_DTF_CTAUS", &LcK_Lc_DTF_CTAUS, &b_LcK_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNe", &LcK_MC12TuneV2_ProbNNe, &b_LcK_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNmu", &LcK_MC12TuneV2_ProbNNmu, &b_LcK_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNpi", &LcK_MC12TuneV2_ProbNNpi, &b_LcK_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNk", &LcK_MC12TuneV2_ProbNNk, &b_LcK_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNp", &LcK_MC12TuneV2_ProbNNp, &b_LcK_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LcK_MC12TuneV2_ProbNNghost", &LcK_MC12TuneV2_ProbNNghost, &b_LcK_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNe", &LcK_MC12TuneV3_ProbNNe, &b_LcK_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNmu", &LcK_MC12TuneV3_ProbNNmu, &b_LcK_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNpi", &LcK_MC12TuneV3_ProbNNpi, &b_LcK_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNk", &LcK_MC12TuneV3_ProbNNk, &b_LcK_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNp", &LcK_MC12TuneV3_ProbNNp, &b_LcK_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LcK_MC12TuneV3_ProbNNghost", &LcK_MC12TuneV3_ProbNNghost, &b_LcK_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNe", &LcK_MC12TuneV4_ProbNNe, &b_LcK_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNmu", &LcK_MC12TuneV4_ProbNNmu, &b_LcK_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNpi", &LcK_MC12TuneV4_ProbNNpi, &b_LcK_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNk", &LcK_MC12TuneV4_ProbNNk, &b_LcK_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNp", &LcK_MC12TuneV4_ProbNNp, &b_LcK_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LcK_MC12TuneV4_ProbNNghost", &LcK_MC12TuneV4_ProbNNghost, &b_LcK_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNe", &LcK_MC15TuneV1_ProbNNe, &b_LcK_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNmu", &LcK_MC15TuneV1_ProbNNmu, &b_LcK_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNpi", &LcK_MC15TuneV1_ProbNNpi, &b_LcK_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNk", &LcK_MC15TuneV1_ProbNNk, &b_LcK_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNp", &LcK_MC15TuneV1_ProbNNp, &b_LcK_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LcK_MC15TuneV1_ProbNNghost", &LcK_MC15TuneV1_ProbNNghost, &b_LcK_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LcK_OWNPV_X", &LcK_OWNPV_X, &b_LcK_OWNPV_X);
   fChain->SetBranchAddress("LcK_OWNPV_Y", &LcK_OWNPV_Y, &b_LcK_OWNPV_Y);
   fChain->SetBranchAddress("LcK_OWNPV_Z", &LcK_OWNPV_Z, &b_LcK_OWNPV_Z);
   fChain->SetBranchAddress("LcK_OWNPV_XERR", &LcK_OWNPV_XERR, &b_LcK_OWNPV_XERR);
   fChain->SetBranchAddress("LcK_OWNPV_YERR", &LcK_OWNPV_YERR, &b_LcK_OWNPV_YERR);
   fChain->SetBranchAddress("LcK_OWNPV_ZERR", &LcK_OWNPV_ZERR, &b_LcK_OWNPV_ZERR);
   fChain->SetBranchAddress("LcK_OWNPV_CHI2", &LcK_OWNPV_CHI2, &b_LcK_OWNPV_CHI2);
   fChain->SetBranchAddress("LcK_OWNPV_NDOF", &LcK_OWNPV_NDOF, &b_LcK_OWNPV_NDOF);
   fChain->SetBranchAddress("LcK_OWNPV_COV_", LcK_OWNPV_COV_, &b_LcK_OWNPV_COV_);
   fChain->SetBranchAddress("LcK_IP_OWNPV", &LcK_IP_OWNPV, &b_LcK_IP_OWNPV);
   fChain->SetBranchAddress("LcK_IPCHI2_OWNPV", &LcK_IPCHI2_OWNPV, &b_LcK_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LcK_ORIVX_X", &LcK_ORIVX_X, &b_LcK_ORIVX_X);
   fChain->SetBranchAddress("LcK_ORIVX_Y", &LcK_ORIVX_Y, &b_LcK_ORIVX_Y);
   fChain->SetBranchAddress("LcK_ORIVX_Z", &LcK_ORIVX_Z, &b_LcK_ORIVX_Z);
   fChain->SetBranchAddress("LcK_ORIVX_XERR", &LcK_ORIVX_XERR, &b_LcK_ORIVX_XERR);
   fChain->SetBranchAddress("LcK_ORIVX_YERR", &LcK_ORIVX_YERR, &b_LcK_ORIVX_YERR);
   fChain->SetBranchAddress("LcK_ORIVX_ZERR", &LcK_ORIVX_ZERR, &b_LcK_ORIVX_ZERR);
   fChain->SetBranchAddress("LcK_ORIVX_CHI2", &LcK_ORIVX_CHI2, &b_LcK_ORIVX_CHI2);
   fChain->SetBranchAddress("LcK_ORIVX_NDOF", &LcK_ORIVX_NDOF, &b_LcK_ORIVX_NDOF);
   fChain->SetBranchAddress("LcK_ORIVX_COV_", LcK_ORIVX_COV_, &b_LcK_ORIVX_COV_);
   fChain->SetBranchAddress("LcK_P", &LcK_P, &b_LcK_P);
   fChain->SetBranchAddress("LcK_PT", &LcK_PT, &b_LcK_PT);
   fChain->SetBranchAddress("LcK_PE", &LcK_PE, &b_LcK_PE);
   fChain->SetBranchAddress("LcK_PX", &LcK_PX, &b_LcK_PX);
   fChain->SetBranchAddress("LcK_PY", &LcK_PY, &b_LcK_PY);
   fChain->SetBranchAddress("LcK_PZ", &LcK_PZ, &b_LcK_PZ);
   fChain->SetBranchAddress("LcK_M", &LcK_M, &b_LcK_M);
   fChain->SetBranchAddress("LcK_ID", &LcK_ID, &b_LcK_ID);
   fChain->SetBranchAddress("LcK_PIDe", &LcK_PIDe, &b_LcK_PIDe);
   fChain->SetBranchAddress("LcK_PIDmu", &LcK_PIDmu, &b_LcK_PIDmu);
   fChain->SetBranchAddress("LcK_PIDK", &LcK_PIDK, &b_LcK_PIDK);
   fChain->SetBranchAddress("LcK_PIDp", &LcK_PIDp, &b_LcK_PIDp);
   fChain->SetBranchAddress("LcK_ProbNNe", &LcK_ProbNNe, &b_LcK_ProbNNe);
   fChain->SetBranchAddress("LcK_ProbNNk", &LcK_ProbNNk, &b_LcK_ProbNNk);
   fChain->SetBranchAddress("LcK_ProbNNp", &LcK_ProbNNp, &b_LcK_ProbNNp);
   fChain->SetBranchAddress("LcK_ProbNNpi", &LcK_ProbNNpi, &b_LcK_ProbNNpi);
   fChain->SetBranchAddress("LcK_ProbNNmu", &LcK_ProbNNmu, &b_LcK_ProbNNmu);
   fChain->SetBranchAddress("LcK_ProbNNghost", &LcK_ProbNNghost, &b_LcK_ProbNNghost);
   fChain->SetBranchAddress("LcK_hasMuon", &LcK_hasMuon, &b_LcK_hasMuon);
   fChain->SetBranchAddress("LcK_isMuon", &LcK_isMuon, &b_LcK_isMuon);
   fChain->SetBranchAddress("LcK_hasRich", &LcK_hasRich, &b_LcK_hasRich);
   fChain->SetBranchAddress("LcK_hasCalo", &LcK_hasCalo, &b_LcK_hasCalo);
   fChain->SetBranchAddress("LcK_PP_CombDLLe", &LcK_PP_CombDLLe, &b_LcK_PP_CombDLLe);
   fChain->SetBranchAddress("LcK_PP_CombDLLmu", &LcK_PP_CombDLLmu, &b_LcK_PP_CombDLLmu);
   fChain->SetBranchAddress("LcK_PP_CombDLLpi", &LcK_PP_CombDLLpi, &b_LcK_PP_CombDLLpi);
   fChain->SetBranchAddress("LcK_PP_CombDLLk", &LcK_PP_CombDLLk, &b_LcK_PP_CombDLLk);
   fChain->SetBranchAddress("LcK_PP_CombDLLp", &LcK_PP_CombDLLp, &b_LcK_PP_CombDLLp);
   fChain->SetBranchAddress("LcK_PP_CombDLLd", &LcK_PP_CombDLLd, &b_LcK_PP_CombDLLd);
   fChain->SetBranchAddress("LcK_PP_ProbNNe", &LcK_PP_ProbNNe, &b_LcK_PP_ProbNNe);
   fChain->SetBranchAddress("LcK_PP_ProbNNmu", &LcK_PP_ProbNNmu, &b_LcK_PP_ProbNNmu);
   fChain->SetBranchAddress("LcK_PP_ProbNNpi", &LcK_PP_ProbNNpi, &b_LcK_PP_ProbNNpi);
   fChain->SetBranchAddress("LcK_PP_ProbNNk", &LcK_PP_ProbNNk, &b_LcK_PP_ProbNNk);
   fChain->SetBranchAddress("LcK_PP_ProbNNp", &LcK_PP_ProbNNp, &b_LcK_PP_ProbNNp);
   fChain->SetBranchAddress("LcK_PP_ProbNNghost", &LcK_PP_ProbNNghost, &b_LcK_PP_ProbNNghost);
   fChain->SetBranchAddress("LcK_L0Global_Dec", &LcK_L0Global_Dec, &b_LcK_L0Global_Dec);
   fChain->SetBranchAddress("LcK_L0Global_TIS", &LcK_L0Global_TIS, &b_LcK_L0Global_TIS);
   fChain->SetBranchAddress("LcK_L0Global_TOS", &LcK_L0Global_TOS, &b_LcK_L0Global_TOS);
   fChain->SetBranchAddress("LcK_Hlt1Global_Dec", &LcK_Hlt1Global_Dec, &b_LcK_Hlt1Global_Dec);
   fChain->SetBranchAddress("LcK_Hlt1Global_TIS", &LcK_Hlt1Global_TIS, &b_LcK_Hlt1Global_TIS);
   fChain->SetBranchAddress("LcK_Hlt1Global_TOS", &LcK_Hlt1Global_TOS, &b_LcK_Hlt1Global_TOS);
   fChain->SetBranchAddress("LcK_Hlt1Phys_Dec", &LcK_Hlt1Phys_Dec, &b_LcK_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LcK_Hlt1Phys_TIS", &LcK_Hlt1Phys_TIS, &b_LcK_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LcK_Hlt1Phys_TOS", &LcK_Hlt1Phys_TOS, &b_LcK_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LcK_Hlt2Global_Dec", &LcK_Hlt2Global_Dec, &b_LcK_Hlt2Global_Dec);
   fChain->SetBranchAddress("LcK_Hlt2Global_TIS", &LcK_Hlt2Global_TIS, &b_LcK_Hlt2Global_TIS);
   fChain->SetBranchAddress("LcK_Hlt2Global_TOS", &LcK_Hlt2Global_TOS, &b_LcK_Hlt2Global_TOS);
   fChain->SetBranchAddress("LcK_Hlt2Phys_Dec", &LcK_Hlt2Phys_Dec, &b_LcK_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LcK_Hlt2Phys_TIS", &LcK_Hlt2Phys_TIS, &b_LcK_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LcK_Hlt2Phys_TOS", &LcK_Hlt2Phys_TOS, &b_LcK_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LcK_L0HadronDecision_Dec", &LcK_L0HadronDecision_Dec, &b_LcK_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LcK_L0HadronDecision_TIS", &LcK_L0HadronDecision_TIS, &b_LcK_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LcK_L0HadronDecision_TOS", &LcK_L0HadronDecision_TOS, &b_LcK_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LcK_L0MuonDecision_Dec", &LcK_L0MuonDecision_Dec, &b_LcK_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LcK_L0MuonDecision_TIS", &LcK_L0MuonDecision_TIS, &b_LcK_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LcK_L0MuonDecision_TOS", &LcK_L0MuonDecision_TOS, &b_LcK_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LcK_L0ElectronDecision_Dec", &LcK_L0ElectronDecision_Dec, &b_LcK_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LcK_L0ElectronDecision_TIS", &LcK_L0ElectronDecision_TIS, &b_LcK_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LcK_L0ElectronDecision_TOS", &LcK_L0ElectronDecision_TOS, &b_LcK_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LcK_L0PhotonDecision_Dec", &LcK_L0PhotonDecision_Dec, &b_LcK_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LcK_L0PhotonDecision_TIS", &LcK_L0PhotonDecision_TIS, &b_LcK_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LcK_L0PhotonDecision_TOS", &LcK_L0PhotonDecision_TOS, &b_LcK_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LcK_L0DiMuonDecision_Dec", &LcK_L0DiMuonDecision_Dec, &b_LcK_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LcK_L0DiMuonDecision_TIS", &LcK_L0DiMuonDecision_TIS, &b_LcK_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LcK_L0DiMuonDecision_TOS", &LcK_L0DiMuonDecision_TOS, &b_LcK_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LcK_L0DiHadronDecision_Dec", &LcK_L0DiHadronDecision_Dec, &b_LcK_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LcK_L0DiHadronDecision_TIS", &LcK_L0DiHadronDecision_TIS, &b_LcK_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LcK_L0DiHadronDecision_TOS", &LcK_L0DiHadronDecision_TOS, &b_LcK_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LcK_Hlt1TrackAllL0Decision_Dec", &LcK_Hlt1TrackAllL0Decision_Dec, &b_LcK_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LcK_Hlt1TrackAllL0Decision_TIS", &LcK_Hlt1TrackAllL0Decision_TIS, &b_LcK_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LcK_Hlt1TrackAllL0Decision_TOS", &LcK_Hlt1TrackAllL0Decision_TOS, &b_LcK_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LcK_Hlt2Topo2BodyBBDTDecision_Dec", &LcK_Hlt2Topo2BodyBBDTDecision_Dec, &b_LcK_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcK_Hlt2Topo2BodyBBDTDecision_TIS", &LcK_Hlt2Topo2BodyBBDTDecision_TIS, &b_LcK_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcK_Hlt2Topo2BodyBBDTDecision_TOS", &LcK_Hlt2Topo2BodyBBDTDecision_TOS, &b_LcK_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcK_Hlt2Topo3BodyBBDTDecision_Dec", &LcK_Hlt2Topo3BodyBBDTDecision_Dec, &b_LcK_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcK_Hlt2Topo3BodyBBDTDecision_TIS", &LcK_Hlt2Topo3BodyBBDTDecision_TIS, &b_LcK_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcK_Hlt2Topo3BodyBBDTDecision_TOS", &LcK_Hlt2Topo3BodyBBDTDecision_TOS, &b_LcK_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcK_Hlt2Topo4BodyBBDTDecision_Dec", &LcK_Hlt2Topo4BodyBBDTDecision_Dec, &b_LcK_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcK_Hlt2Topo4BodyBBDTDecision_TIS", &LcK_Hlt2Topo4BodyBBDTDecision_TIS, &b_LcK_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcK_Hlt2Topo4BodyBBDTDecision_TOS", &LcK_Hlt2Topo4BodyBBDTDecision_TOS, &b_LcK_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcK_Hlt2IncPhiDecision_Dec", &LcK_Hlt2IncPhiDecision_Dec, &b_LcK_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LcK_Hlt2IncPhiDecision_TIS", &LcK_Hlt2IncPhiDecision_TIS, &b_LcK_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LcK_Hlt2IncPhiDecision_TOS", &LcK_Hlt2IncPhiDecision_TOS, &b_LcK_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LcK_TRACK_Type", &LcK_TRACK_Type, &b_LcK_TRACK_Type);
   fChain->SetBranchAddress("LcK_TRACK_Key", &LcK_TRACK_Key, &b_LcK_TRACK_Key);
   fChain->SetBranchAddress("LcK_TRACK_CHI2NDOF", &LcK_TRACK_CHI2NDOF, &b_LcK_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LcK_TRACK_PCHI2", &LcK_TRACK_PCHI2, &b_LcK_TRACK_PCHI2);
   fChain->SetBranchAddress("LcK_TRACK_MatchCHI2", &LcK_TRACK_MatchCHI2, &b_LcK_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LcK_TRACK_GhostProb", &LcK_TRACK_GhostProb, &b_LcK_TRACK_GhostProb);
   fChain->SetBranchAddress("LcK_TRACK_CloneDist", &LcK_TRACK_CloneDist, &b_LcK_TRACK_CloneDist);
   fChain->SetBranchAddress("LcK_TRACK_Likelihood", &LcK_TRACK_Likelihood, &b_LcK_TRACK_Likelihood);
   fChain->SetBranchAddress("LcPi_DTF_CHI2NDOF", &LcPi_DTF_CHI2NDOF, &b_LcPi_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LcPi_DTF_CTAU", &LcPi_DTF_CTAU, &b_LcPi_DTF_CTAU);
   fChain->SetBranchAddress("LcPi_DTF_CTAUS", &LcPi_DTF_CTAUS, &b_LcPi_DTF_CTAUS);
   fChain->SetBranchAddress("LcPi_DTF_M", &LcPi_DTF_M, &b_LcPi_DTF_M);
   fChain->SetBranchAddress("LcPi_LOKI_DIRA", &LcPi_LOKI_DIRA, &b_LcPi_LOKI_DIRA);
   fChain->SetBranchAddress("LcPi_LOKI_FDCHI2", &LcPi_LOKI_FDCHI2, &b_LcPi_LOKI_FDCHI2);
   fChain->SetBranchAddress("LcPi_LOKI_FDS", &LcPi_LOKI_FDS, &b_LcPi_LOKI_FDS);
   fChain->SetBranchAddress("LcPi_LV01", &LcPi_LV01, &b_LcPi_LV01);
   fChain->SetBranchAddress("LcPi_LV02", &LcPi_LV02, &b_LcPi_LV02);
   fChain->SetBranchAddress("LcPi_Lc_DTF_CTAU", &LcPi_Lc_DTF_CTAU, &b_LcPi_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LcPi_Lc_DTF_CTAUS", &LcPi_Lc_DTF_CTAUS, &b_LcPi_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNe", &LcPi_MC12TuneV2_ProbNNe, &b_LcPi_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNmu", &LcPi_MC12TuneV2_ProbNNmu, &b_LcPi_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNpi", &LcPi_MC12TuneV2_ProbNNpi, &b_LcPi_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNk", &LcPi_MC12TuneV2_ProbNNk, &b_LcPi_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNp", &LcPi_MC12TuneV2_ProbNNp, &b_LcPi_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LcPi_MC12TuneV2_ProbNNghost", &LcPi_MC12TuneV2_ProbNNghost, &b_LcPi_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNe", &LcPi_MC12TuneV3_ProbNNe, &b_LcPi_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNmu", &LcPi_MC12TuneV3_ProbNNmu, &b_LcPi_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNpi", &LcPi_MC12TuneV3_ProbNNpi, &b_LcPi_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNk", &LcPi_MC12TuneV3_ProbNNk, &b_LcPi_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNp", &LcPi_MC12TuneV3_ProbNNp, &b_LcPi_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LcPi_MC12TuneV3_ProbNNghost", &LcPi_MC12TuneV3_ProbNNghost, &b_LcPi_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNe", &LcPi_MC12TuneV4_ProbNNe, &b_LcPi_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNmu", &LcPi_MC12TuneV4_ProbNNmu, &b_LcPi_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNpi", &LcPi_MC12TuneV4_ProbNNpi, &b_LcPi_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNk", &LcPi_MC12TuneV4_ProbNNk, &b_LcPi_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNp", &LcPi_MC12TuneV4_ProbNNp, &b_LcPi_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LcPi_MC12TuneV4_ProbNNghost", &LcPi_MC12TuneV4_ProbNNghost, &b_LcPi_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNe", &LcPi_MC15TuneV1_ProbNNe, &b_LcPi_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNmu", &LcPi_MC15TuneV1_ProbNNmu, &b_LcPi_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNpi", &LcPi_MC15TuneV1_ProbNNpi, &b_LcPi_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNk", &LcPi_MC15TuneV1_ProbNNk, &b_LcPi_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNp", &LcPi_MC15TuneV1_ProbNNp, &b_LcPi_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LcPi_MC15TuneV1_ProbNNghost", &LcPi_MC15TuneV1_ProbNNghost, &b_LcPi_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LcPi_OWNPV_X", &LcPi_OWNPV_X, &b_LcPi_OWNPV_X);
   fChain->SetBranchAddress("LcPi_OWNPV_Y", &LcPi_OWNPV_Y, &b_LcPi_OWNPV_Y);
   fChain->SetBranchAddress("LcPi_OWNPV_Z", &LcPi_OWNPV_Z, &b_LcPi_OWNPV_Z);
   fChain->SetBranchAddress("LcPi_OWNPV_XERR", &LcPi_OWNPV_XERR, &b_LcPi_OWNPV_XERR);
   fChain->SetBranchAddress("LcPi_OWNPV_YERR", &LcPi_OWNPV_YERR, &b_LcPi_OWNPV_YERR);
   fChain->SetBranchAddress("LcPi_OWNPV_ZERR", &LcPi_OWNPV_ZERR, &b_LcPi_OWNPV_ZERR);
   fChain->SetBranchAddress("LcPi_OWNPV_CHI2", &LcPi_OWNPV_CHI2, &b_LcPi_OWNPV_CHI2);
   fChain->SetBranchAddress("LcPi_OWNPV_NDOF", &LcPi_OWNPV_NDOF, &b_LcPi_OWNPV_NDOF);
   fChain->SetBranchAddress("LcPi_OWNPV_COV_", LcPi_OWNPV_COV_, &b_LcPi_OWNPV_COV_);
   fChain->SetBranchAddress("LcPi_IP_OWNPV", &LcPi_IP_OWNPV, &b_LcPi_IP_OWNPV);
   fChain->SetBranchAddress("LcPi_IPCHI2_OWNPV", &LcPi_IPCHI2_OWNPV, &b_LcPi_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LcPi_ORIVX_X", &LcPi_ORIVX_X, &b_LcPi_ORIVX_X);
   fChain->SetBranchAddress("LcPi_ORIVX_Y", &LcPi_ORIVX_Y, &b_LcPi_ORIVX_Y);
   fChain->SetBranchAddress("LcPi_ORIVX_Z", &LcPi_ORIVX_Z, &b_LcPi_ORIVX_Z);
   fChain->SetBranchAddress("LcPi_ORIVX_XERR", &LcPi_ORIVX_XERR, &b_LcPi_ORIVX_XERR);
   fChain->SetBranchAddress("LcPi_ORIVX_YERR", &LcPi_ORIVX_YERR, &b_LcPi_ORIVX_YERR);
   fChain->SetBranchAddress("LcPi_ORIVX_ZERR", &LcPi_ORIVX_ZERR, &b_LcPi_ORIVX_ZERR);
   fChain->SetBranchAddress("LcPi_ORIVX_CHI2", &LcPi_ORIVX_CHI2, &b_LcPi_ORIVX_CHI2);
   fChain->SetBranchAddress("LcPi_ORIVX_NDOF", &LcPi_ORIVX_NDOF, &b_LcPi_ORIVX_NDOF);
   fChain->SetBranchAddress("LcPi_ORIVX_COV_", LcPi_ORIVX_COV_, &b_LcPi_ORIVX_COV_);
   fChain->SetBranchAddress("LcPi_P", &LcPi_P, &b_LcPi_P);
   fChain->SetBranchAddress("LcPi_PT", &LcPi_PT, &b_LcPi_PT);
   fChain->SetBranchAddress("LcPi_PE", &LcPi_PE, &b_LcPi_PE);
   fChain->SetBranchAddress("LcPi_PX", &LcPi_PX, &b_LcPi_PX);
   fChain->SetBranchAddress("LcPi_PY", &LcPi_PY, &b_LcPi_PY);
   fChain->SetBranchAddress("LcPi_PZ", &LcPi_PZ, &b_LcPi_PZ);
   fChain->SetBranchAddress("LcPi_M", &LcPi_M, &b_LcPi_M);
   fChain->SetBranchAddress("LcPi_ID", &LcPi_ID, &b_LcPi_ID);
   fChain->SetBranchAddress("LcPi_PIDe", &LcPi_PIDe, &b_LcPi_PIDe);
   fChain->SetBranchAddress("LcPi_PIDmu", &LcPi_PIDmu, &b_LcPi_PIDmu);
   fChain->SetBranchAddress("LcPi_PIDK", &LcPi_PIDK, &b_LcPi_PIDK);
   fChain->SetBranchAddress("LcPi_PIDp", &LcPi_PIDp, &b_LcPi_PIDp);
   fChain->SetBranchAddress("LcPi_ProbNNe", &LcPi_ProbNNe, &b_LcPi_ProbNNe);
   fChain->SetBranchAddress("LcPi_ProbNNk", &LcPi_ProbNNk, &b_LcPi_ProbNNk);
   fChain->SetBranchAddress("LcPi_ProbNNp", &LcPi_ProbNNp, &b_LcPi_ProbNNp);
   fChain->SetBranchAddress("LcPi_ProbNNpi", &LcPi_ProbNNpi, &b_LcPi_ProbNNpi);
   fChain->SetBranchAddress("LcPi_ProbNNmu", &LcPi_ProbNNmu, &b_LcPi_ProbNNmu);
   fChain->SetBranchAddress("LcPi_ProbNNghost", &LcPi_ProbNNghost, &b_LcPi_ProbNNghost);
   fChain->SetBranchAddress("LcPi_hasMuon", &LcPi_hasMuon, &b_LcPi_hasMuon);
   fChain->SetBranchAddress("LcPi_isMuon", &LcPi_isMuon, &b_LcPi_isMuon);
   fChain->SetBranchAddress("LcPi_hasRich", &LcPi_hasRich, &b_LcPi_hasRich);
   fChain->SetBranchAddress("LcPi_hasCalo", &LcPi_hasCalo, &b_LcPi_hasCalo);
   fChain->SetBranchAddress("LcPi_PP_CombDLLe", &LcPi_PP_CombDLLe, &b_LcPi_PP_CombDLLe);
   fChain->SetBranchAddress("LcPi_PP_CombDLLmu", &LcPi_PP_CombDLLmu, &b_LcPi_PP_CombDLLmu);
   fChain->SetBranchAddress("LcPi_PP_CombDLLpi", &LcPi_PP_CombDLLpi, &b_LcPi_PP_CombDLLpi);
   fChain->SetBranchAddress("LcPi_PP_CombDLLk", &LcPi_PP_CombDLLk, &b_LcPi_PP_CombDLLk);
   fChain->SetBranchAddress("LcPi_PP_CombDLLp", &LcPi_PP_CombDLLp, &b_LcPi_PP_CombDLLp);
   fChain->SetBranchAddress("LcPi_PP_CombDLLd", &LcPi_PP_CombDLLd, &b_LcPi_PP_CombDLLd);
   fChain->SetBranchAddress("LcPi_PP_ProbNNe", &LcPi_PP_ProbNNe, &b_LcPi_PP_ProbNNe);
   fChain->SetBranchAddress("LcPi_PP_ProbNNmu", &LcPi_PP_ProbNNmu, &b_LcPi_PP_ProbNNmu);
   fChain->SetBranchAddress("LcPi_PP_ProbNNpi", &LcPi_PP_ProbNNpi, &b_LcPi_PP_ProbNNpi);
   fChain->SetBranchAddress("LcPi_PP_ProbNNk", &LcPi_PP_ProbNNk, &b_LcPi_PP_ProbNNk);
   fChain->SetBranchAddress("LcPi_PP_ProbNNp", &LcPi_PP_ProbNNp, &b_LcPi_PP_ProbNNp);
   fChain->SetBranchAddress("LcPi_PP_ProbNNghost", &LcPi_PP_ProbNNghost, &b_LcPi_PP_ProbNNghost);
   fChain->SetBranchAddress("LcPi_L0Global_Dec", &LcPi_L0Global_Dec, &b_LcPi_L0Global_Dec);
   fChain->SetBranchAddress("LcPi_L0Global_TIS", &LcPi_L0Global_TIS, &b_LcPi_L0Global_TIS);
   fChain->SetBranchAddress("LcPi_L0Global_TOS", &LcPi_L0Global_TOS, &b_LcPi_L0Global_TOS);
   fChain->SetBranchAddress("LcPi_Hlt1Global_Dec", &LcPi_Hlt1Global_Dec, &b_LcPi_Hlt1Global_Dec);
   fChain->SetBranchAddress("LcPi_Hlt1Global_TIS", &LcPi_Hlt1Global_TIS, &b_LcPi_Hlt1Global_TIS);
   fChain->SetBranchAddress("LcPi_Hlt1Global_TOS", &LcPi_Hlt1Global_TOS, &b_LcPi_Hlt1Global_TOS);
   fChain->SetBranchAddress("LcPi_Hlt1Phys_Dec", &LcPi_Hlt1Phys_Dec, &b_LcPi_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LcPi_Hlt1Phys_TIS", &LcPi_Hlt1Phys_TIS, &b_LcPi_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LcPi_Hlt1Phys_TOS", &LcPi_Hlt1Phys_TOS, &b_LcPi_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2Global_Dec", &LcPi_Hlt2Global_Dec, &b_LcPi_Hlt2Global_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2Global_TIS", &LcPi_Hlt2Global_TIS, &b_LcPi_Hlt2Global_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2Global_TOS", &LcPi_Hlt2Global_TOS, &b_LcPi_Hlt2Global_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2Phys_Dec", &LcPi_Hlt2Phys_Dec, &b_LcPi_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2Phys_TIS", &LcPi_Hlt2Phys_TIS, &b_LcPi_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2Phys_TOS", &LcPi_Hlt2Phys_TOS, &b_LcPi_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LcPi_L0HadronDecision_Dec", &LcPi_L0HadronDecision_Dec, &b_LcPi_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0HadronDecision_TIS", &LcPi_L0HadronDecision_TIS, &b_LcPi_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0HadronDecision_TOS", &LcPi_L0HadronDecision_TOS, &b_LcPi_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LcPi_L0MuonDecision_Dec", &LcPi_L0MuonDecision_Dec, &b_LcPi_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0MuonDecision_TIS", &LcPi_L0MuonDecision_TIS, &b_LcPi_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0MuonDecision_TOS", &LcPi_L0MuonDecision_TOS, &b_LcPi_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LcPi_L0ElectronDecision_Dec", &LcPi_L0ElectronDecision_Dec, &b_LcPi_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0ElectronDecision_TIS", &LcPi_L0ElectronDecision_TIS, &b_LcPi_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0ElectronDecision_TOS", &LcPi_L0ElectronDecision_TOS, &b_LcPi_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LcPi_L0PhotonDecision_Dec", &LcPi_L0PhotonDecision_Dec, &b_LcPi_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0PhotonDecision_TIS", &LcPi_L0PhotonDecision_TIS, &b_LcPi_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0PhotonDecision_TOS", &LcPi_L0PhotonDecision_TOS, &b_LcPi_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LcPi_L0DiMuonDecision_Dec", &LcPi_L0DiMuonDecision_Dec, &b_LcPi_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0DiMuonDecision_TIS", &LcPi_L0DiMuonDecision_TIS, &b_LcPi_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0DiMuonDecision_TOS", &LcPi_L0DiMuonDecision_TOS, &b_LcPi_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LcPi_L0DiHadronDecision_Dec", &LcPi_L0DiHadronDecision_Dec, &b_LcPi_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LcPi_L0DiHadronDecision_TIS", &LcPi_L0DiHadronDecision_TIS, &b_LcPi_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LcPi_L0DiHadronDecision_TOS", &LcPi_L0DiHadronDecision_TOS, &b_LcPi_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LcPi_Hlt1TrackAllL0Decision_Dec", &LcPi_Hlt1TrackAllL0Decision_Dec, &b_LcPi_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LcPi_Hlt1TrackAllL0Decision_TIS", &LcPi_Hlt1TrackAllL0Decision_TIS, &b_LcPi_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LcPi_Hlt1TrackAllL0Decision_TOS", &LcPi_Hlt1TrackAllL0Decision_TOS, &b_LcPi_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo2BodyBBDTDecision_Dec", &LcPi_Hlt2Topo2BodyBBDTDecision_Dec, &b_LcPi_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2Topo2BodyBBDTDecision_TIS", &LcPi_Hlt2Topo2BodyBBDTDecision_TIS, &b_LcPi_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo2BodyBBDTDecision_TOS", &LcPi_Hlt2Topo2BodyBBDTDecision_TOS, &b_LcPi_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo3BodyBBDTDecision_Dec", &LcPi_Hlt2Topo3BodyBBDTDecision_Dec, &b_LcPi_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2Topo3BodyBBDTDecision_TIS", &LcPi_Hlt2Topo3BodyBBDTDecision_TIS, &b_LcPi_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo3BodyBBDTDecision_TOS", &LcPi_Hlt2Topo3BodyBBDTDecision_TOS, &b_LcPi_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo4BodyBBDTDecision_Dec", &LcPi_Hlt2Topo4BodyBBDTDecision_Dec, &b_LcPi_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2Topo4BodyBBDTDecision_TIS", &LcPi_Hlt2Topo4BodyBBDTDecision_TIS, &b_LcPi_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2Topo4BodyBBDTDecision_TOS", &LcPi_Hlt2Topo4BodyBBDTDecision_TOS, &b_LcPi_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LcPi_Hlt2IncPhiDecision_Dec", &LcPi_Hlt2IncPhiDecision_Dec, &b_LcPi_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LcPi_Hlt2IncPhiDecision_TIS", &LcPi_Hlt2IncPhiDecision_TIS, &b_LcPi_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LcPi_Hlt2IncPhiDecision_TOS", &LcPi_Hlt2IncPhiDecision_TOS, &b_LcPi_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LcPi_TRACK_Type", &LcPi_TRACK_Type, &b_LcPi_TRACK_Type);
   fChain->SetBranchAddress("LcPi_TRACK_Key", &LcPi_TRACK_Key, &b_LcPi_TRACK_Key);
   fChain->SetBranchAddress("LcPi_TRACK_CHI2NDOF", &LcPi_TRACK_CHI2NDOF, &b_LcPi_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LcPi_TRACK_PCHI2", &LcPi_TRACK_PCHI2, &b_LcPi_TRACK_PCHI2);
   fChain->SetBranchAddress("LcPi_TRACK_MatchCHI2", &LcPi_TRACK_MatchCHI2, &b_LcPi_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LcPi_TRACK_GhostProb", &LcPi_TRACK_GhostProb, &b_LcPi_TRACK_GhostProb);
   fChain->SetBranchAddress("LcPi_TRACK_CloneDist", &LcPi_TRACK_CloneDist, &b_LcPi_TRACK_CloneDist);
   fChain->SetBranchAddress("LcPi_TRACK_Likelihood", &LcPi_TRACK_Likelihood, &b_LcPi_TRACK_Likelihood);
   fChain->SetBranchAddress("a1_DTF_CHI2NDOF", &a1_DTF_CHI2NDOF, &b_a1_DTF_CHI2NDOF);
   fChain->SetBranchAddress("a1_DTF_CTAU", &a1_DTF_CTAU, &b_a1_DTF_CTAU);
   fChain->SetBranchAddress("a1_DTF_CTAUS", &a1_DTF_CTAUS, &b_a1_DTF_CTAUS);
   fChain->SetBranchAddress("a1_DTF_M", &a1_DTF_M, &b_a1_DTF_M);
   fChain->SetBranchAddress("a1_LOKI_DIRA", &a1_LOKI_DIRA, &b_a1_LOKI_DIRA);
   fChain->SetBranchAddress("a1_LOKI_FDCHI2", &a1_LOKI_FDCHI2, &b_a1_LOKI_FDCHI2);
   fChain->SetBranchAddress("a1_LOKI_FDS", &a1_LOKI_FDS, &b_a1_LOKI_FDS);
   fChain->SetBranchAddress("a1_LV01", &a1_LV01, &b_a1_LV01);
   fChain->SetBranchAddress("a1_LV02", &a1_LV02, &b_a1_LV02);
   fChain->SetBranchAddress("a1_Lc_DTF_CTAU", &a1_Lc_DTF_CTAU, &b_a1_Lc_DTF_CTAU);
   fChain->SetBranchAddress("a1_Lc_DTF_CTAUS", &a1_Lc_DTF_CTAUS, &b_a1_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("a1_ENDVERTEX_X", &a1_ENDVERTEX_X, &b_a1_ENDVERTEX_X);
   fChain->SetBranchAddress("a1_ENDVERTEX_Y", &a1_ENDVERTEX_Y, &b_a1_ENDVERTEX_Y);
   fChain->SetBranchAddress("a1_ENDVERTEX_Z", &a1_ENDVERTEX_Z, &b_a1_ENDVERTEX_Z);
   fChain->SetBranchAddress("a1_ENDVERTEX_XERR", &a1_ENDVERTEX_XERR, &b_a1_ENDVERTEX_XERR);
   fChain->SetBranchAddress("a1_ENDVERTEX_YERR", &a1_ENDVERTEX_YERR, &b_a1_ENDVERTEX_YERR);
   fChain->SetBranchAddress("a1_ENDVERTEX_ZERR", &a1_ENDVERTEX_ZERR, &b_a1_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("a1_ENDVERTEX_CHI2", &a1_ENDVERTEX_CHI2, &b_a1_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("a1_ENDVERTEX_NDOF", &a1_ENDVERTEX_NDOF, &b_a1_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("a1_ENDVERTEX_COV_", a1_ENDVERTEX_COV_, &b_a1_ENDVERTEX_COV_);
   fChain->SetBranchAddress("a1_OWNPV_X", &a1_OWNPV_X, &b_a1_OWNPV_X);
   fChain->SetBranchAddress("a1_OWNPV_Y", &a1_OWNPV_Y, &b_a1_OWNPV_Y);
   fChain->SetBranchAddress("a1_OWNPV_Z", &a1_OWNPV_Z, &b_a1_OWNPV_Z);
   fChain->SetBranchAddress("a1_OWNPV_XERR", &a1_OWNPV_XERR, &b_a1_OWNPV_XERR);
   fChain->SetBranchAddress("a1_OWNPV_YERR", &a1_OWNPV_YERR, &b_a1_OWNPV_YERR);
   fChain->SetBranchAddress("a1_OWNPV_ZERR", &a1_OWNPV_ZERR, &b_a1_OWNPV_ZERR);
   fChain->SetBranchAddress("a1_OWNPV_CHI2", &a1_OWNPV_CHI2, &b_a1_OWNPV_CHI2);
   fChain->SetBranchAddress("a1_OWNPV_NDOF", &a1_OWNPV_NDOF, &b_a1_OWNPV_NDOF);
   fChain->SetBranchAddress("a1_OWNPV_COV_", a1_OWNPV_COV_, &b_a1_OWNPV_COV_);
   fChain->SetBranchAddress("a1_IP_OWNPV", &a1_IP_OWNPV, &b_a1_IP_OWNPV);
   fChain->SetBranchAddress("a1_IPCHI2_OWNPV", &a1_IPCHI2_OWNPV, &b_a1_IPCHI2_OWNPV);
   fChain->SetBranchAddress("a1_FD_OWNPV", &a1_FD_OWNPV, &b_a1_FD_OWNPV);
   fChain->SetBranchAddress("a1_FDCHI2_OWNPV", &a1_FDCHI2_OWNPV, &b_a1_FDCHI2_OWNPV);
   fChain->SetBranchAddress("a1_DIRA_OWNPV", &a1_DIRA_OWNPV, &b_a1_DIRA_OWNPV);
   fChain->SetBranchAddress("a1_ORIVX_X", &a1_ORIVX_X, &b_a1_ORIVX_X);
   fChain->SetBranchAddress("a1_ORIVX_Y", &a1_ORIVX_Y, &b_a1_ORIVX_Y);
   fChain->SetBranchAddress("a1_ORIVX_Z", &a1_ORIVX_Z, &b_a1_ORIVX_Z);
   fChain->SetBranchAddress("a1_ORIVX_XERR", &a1_ORIVX_XERR, &b_a1_ORIVX_XERR);
   fChain->SetBranchAddress("a1_ORIVX_YERR", &a1_ORIVX_YERR, &b_a1_ORIVX_YERR);
   fChain->SetBranchAddress("a1_ORIVX_ZERR", &a1_ORIVX_ZERR, &b_a1_ORIVX_ZERR);
   fChain->SetBranchAddress("a1_ORIVX_CHI2", &a1_ORIVX_CHI2, &b_a1_ORIVX_CHI2);
   fChain->SetBranchAddress("a1_ORIVX_NDOF", &a1_ORIVX_NDOF, &b_a1_ORIVX_NDOF);
   fChain->SetBranchAddress("a1_ORIVX_COV_", a1_ORIVX_COV_, &b_a1_ORIVX_COV_);
   fChain->SetBranchAddress("a1_FD_ORIVX", &a1_FD_ORIVX, &b_a1_FD_ORIVX);
   fChain->SetBranchAddress("a1_FDCHI2_ORIVX", &a1_FDCHI2_ORIVX, &b_a1_FDCHI2_ORIVX);
   fChain->SetBranchAddress("a1_DIRA_ORIVX", &a1_DIRA_ORIVX, &b_a1_DIRA_ORIVX);
   fChain->SetBranchAddress("a1_P", &a1_P, &b_a1_P);
   fChain->SetBranchAddress("a1_PT", &a1_PT, &b_a1_PT);
   fChain->SetBranchAddress("a1_PE", &a1_PE, &b_a1_PE);
   fChain->SetBranchAddress("a1_PX", &a1_PX, &b_a1_PX);
   fChain->SetBranchAddress("a1_PY", &a1_PY, &b_a1_PY);
   fChain->SetBranchAddress("a1_PZ", &a1_PZ, &b_a1_PZ);
   fChain->SetBranchAddress("a1_MM", &a1_MM, &b_a1_MM);
   fChain->SetBranchAddress("a1_MMERR", &a1_MMERR, &b_a1_MMERR);
   fChain->SetBranchAddress("a1_M", &a1_M, &b_a1_M);
   fChain->SetBranchAddress("a1_ID", &a1_ID, &b_a1_ID);
   fChain->SetBranchAddress("a1_TAU", &a1_TAU, &b_a1_TAU);
   fChain->SetBranchAddress("a1_TAUERR", &a1_TAUERR, &b_a1_TAUERR);
   fChain->SetBranchAddress("a1_TAUCHI2", &a1_TAUCHI2, &b_a1_TAUCHI2);
   fChain->SetBranchAddress("a1_L0Global_Dec", &a1_L0Global_Dec, &b_a1_L0Global_Dec);
   fChain->SetBranchAddress("a1_L0Global_TIS", &a1_L0Global_TIS, &b_a1_L0Global_TIS);
   fChain->SetBranchAddress("a1_L0Global_TOS", &a1_L0Global_TOS, &b_a1_L0Global_TOS);
   fChain->SetBranchAddress("a1_Hlt1Global_Dec", &a1_Hlt1Global_Dec, &b_a1_Hlt1Global_Dec);
   fChain->SetBranchAddress("a1_Hlt1Global_TIS", &a1_Hlt1Global_TIS, &b_a1_Hlt1Global_TIS);
   fChain->SetBranchAddress("a1_Hlt1Global_TOS", &a1_Hlt1Global_TOS, &b_a1_Hlt1Global_TOS);
   fChain->SetBranchAddress("a1_Hlt1Phys_Dec", &a1_Hlt1Phys_Dec, &b_a1_Hlt1Phys_Dec);
   fChain->SetBranchAddress("a1_Hlt1Phys_TIS", &a1_Hlt1Phys_TIS, &b_a1_Hlt1Phys_TIS);
   fChain->SetBranchAddress("a1_Hlt1Phys_TOS", &a1_Hlt1Phys_TOS, &b_a1_Hlt1Phys_TOS);
   fChain->SetBranchAddress("a1_Hlt2Global_Dec", &a1_Hlt2Global_Dec, &b_a1_Hlt2Global_Dec);
   fChain->SetBranchAddress("a1_Hlt2Global_TIS", &a1_Hlt2Global_TIS, &b_a1_Hlt2Global_TIS);
   fChain->SetBranchAddress("a1_Hlt2Global_TOS", &a1_Hlt2Global_TOS, &b_a1_Hlt2Global_TOS);
   fChain->SetBranchAddress("a1_Hlt2Phys_Dec", &a1_Hlt2Phys_Dec, &b_a1_Hlt2Phys_Dec);
   fChain->SetBranchAddress("a1_Hlt2Phys_TIS", &a1_Hlt2Phys_TIS, &b_a1_Hlt2Phys_TIS);
   fChain->SetBranchAddress("a1_Hlt2Phys_TOS", &a1_Hlt2Phys_TOS, &b_a1_Hlt2Phys_TOS);
   fChain->SetBranchAddress("a1_L0HadronDecision_Dec", &a1_L0HadronDecision_Dec, &b_a1_L0HadronDecision_Dec);
   fChain->SetBranchAddress("a1_L0HadronDecision_TIS", &a1_L0HadronDecision_TIS, &b_a1_L0HadronDecision_TIS);
   fChain->SetBranchAddress("a1_L0HadronDecision_TOS", &a1_L0HadronDecision_TOS, &b_a1_L0HadronDecision_TOS);
   fChain->SetBranchAddress("a1_L0MuonDecision_Dec", &a1_L0MuonDecision_Dec, &b_a1_L0MuonDecision_Dec);
   fChain->SetBranchAddress("a1_L0MuonDecision_TIS", &a1_L0MuonDecision_TIS, &b_a1_L0MuonDecision_TIS);
   fChain->SetBranchAddress("a1_L0MuonDecision_TOS", &a1_L0MuonDecision_TOS, &b_a1_L0MuonDecision_TOS);
   fChain->SetBranchAddress("a1_L0ElectronDecision_Dec", &a1_L0ElectronDecision_Dec, &b_a1_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("a1_L0ElectronDecision_TIS", &a1_L0ElectronDecision_TIS, &b_a1_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("a1_L0ElectronDecision_TOS", &a1_L0ElectronDecision_TOS, &b_a1_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("a1_L0PhotonDecision_Dec", &a1_L0PhotonDecision_Dec, &b_a1_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("a1_L0PhotonDecision_TIS", &a1_L0PhotonDecision_TIS, &b_a1_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("a1_L0PhotonDecision_TOS", &a1_L0PhotonDecision_TOS, &b_a1_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("a1_L0DiMuonDecision_Dec", &a1_L0DiMuonDecision_Dec, &b_a1_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("a1_L0DiMuonDecision_TIS", &a1_L0DiMuonDecision_TIS, &b_a1_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("a1_L0DiMuonDecision_TOS", &a1_L0DiMuonDecision_TOS, &b_a1_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("a1_L0DiHadronDecision_Dec", &a1_L0DiHadronDecision_Dec, &b_a1_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("a1_L0DiHadronDecision_TIS", &a1_L0DiHadronDecision_TIS, &b_a1_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("a1_L0DiHadronDecision_TOS", &a1_L0DiHadronDecision_TOS, &b_a1_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("a1_Hlt1TrackAllL0Decision_Dec", &a1_Hlt1TrackAllL0Decision_Dec, &b_a1_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("a1_Hlt1TrackAllL0Decision_TIS", &a1_Hlt1TrackAllL0Decision_TIS, &b_a1_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("a1_Hlt1TrackAllL0Decision_TOS", &a1_Hlt1TrackAllL0Decision_TOS, &b_a1_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("a1_Hlt2Topo2BodyBBDTDecision_Dec", &a1_Hlt2Topo2BodyBBDTDecision_Dec, &b_a1_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("a1_Hlt2Topo2BodyBBDTDecision_TIS", &a1_Hlt2Topo2BodyBBDTDecision_TIS, &b_a1_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("a1_Hlt2Topo2BodyBBDTDecision_TOS", &a1_Hlt2Topo2BodyBBDTDecision_TOS, &b_a1_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("a1_Hlt2Topo3BodyBBDTDecision_Dec", &a1_Hlt2Topo3BodyBBDTDecision_Dec, &b_a1_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("a1_Hlt2Topo3BodyBBDTDecision_TIS", &a1_Hlt2Topo3BodyBBDTDecision_TIS, &b_a1_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("a1_Hlt2Topo3BodyBBDTDecision_TOS", &a1_Hlt2Topo3BodyBBDTDecision_TOS, &b_a1_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("a1_Hlt2Topo4BodyBBDTDecision_Dec", &a1_Hlt2Topo4BodyBBDTDecision_Dec, &b_a1_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("a1_Hlt2Topo4BodyBBDTDecision_TIS", &a1_Hlt2Topo4BodyBBDTDecision_TIS, &b_a1_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("a1_Hlt2Topo4BodyBBDTDecision_TOS", &a1_Hlt2Topo4BodyBBDTDecision_TOS, &b_a1_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("a1_Hlt2IncPhiDecision_Dec", &a1_Hlt2IncPhiDecision_Dec, &b_a1_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("a1_Hlt2IncPhiDecision_TIS", &a1_Hlt2IncPhiDecision_TIS, &b_a1_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("a1_Hlt2IncPhiDecision_TOS", &a1_Hlt2IncPhiDecision_TOS, &b_a1_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LbKp_DTF_CHI2NDOF", &LbKp_DTF_CHI2NDOF, &b_LbKp_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LbKp_DTF_CTAU", &LbKp_DTF_CTAU, &b_LbKp_DTF_CTAU);
   fChain->SetBranchAddress("LbKp_DTF_CTAUS", &LbKp_DTF_CTAUS, &b_LbKp_DTF_CTAUS);
   fChain->SetBranchAddress("LbKp_DTF_M", &LbKp_DTF_M, &b_LbKp_DTF_M);
   fChain->SetBranchAddress("LbKp_LOKI_DIRA", &LbKp_LOKI_DIRA, &b_LbKp_LOKI_DIRA);
   fChain->SetBranchAddress("LbKp_LOKI_FDCHI2", &LbKp_LOKI_FDCHI2, &b_LbKp_LOKI_FDCHI2);
   fChain->SetBranchAddress("LbKp_LOKI_FDS", &LbKp_LOKI_FDS, &b_LbKp_LOKI_FDS);
   fChain->SetBranchAddress("LbKp_LV01", &LbKp_LV01, &b_LbKp_LV01);
   fChain->SetBranchAddress("LbKp_LV02", &LbKp_LV02, &b_LbKp_LV02);
   fChain->SetBranchAddress("LbKp_Lc_DTF_CTAU", &LbKp_Lc_DTF_CTAU, &b_LbKp_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LbKp_Lc_DTF_CTAUS", &LbKp_Lc_DTF_CTAUS, &b_LbKp_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNe", &LbKp_MC12TuneV2_ProbNNe, &b_LbKp_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNmu", &LbKp_MC12TuneV2_ProbNNmu, &b_LbKp_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNpi", &LbKp_MC12TuneV2_ProbNNpi, &b_LbKp_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNk", &LbKp_MC12TuneV2_ProbNNk, &b_LbKp_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNp", &LbKp_MC12TuneV2_ProbNNp, &b_LbKp_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LbKp_MC12TuneV2_ProbNNghost", &LbKp_MC12TuneV2_ProbNNghost, &b_LbKp_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNe", &LbKp_MC12TuneV3_ProbNNe, &b_LbKp_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNmu", &LbKp_MC12TuneV3_ProbNNmu, &b_LbKp_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNpi", &LbKp_MC12TuneV3_ProbNNpi, &b_LbKp_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNk", &LbKp_MC12TuneV3_ProbNNk, &b_LbKp_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNp", &LbKp_MC12TuneV3_ProbNNp, &b_LbKp_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LbKp_MC12TuneV3_ProbNNghost", &LbKp_MC12TuneV3_ProbNNghost, &b_LbKp_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNe", &LbKp_MC12TuneV4_ProbNNe, &b_LbKp_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNmu", &LbKp_MC12TuneV4_ProbNNmu, &b_LbKp_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNpi", &LbKp_MC12TuneV4_ProbNNpi, &b_LbKp_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNk", &LbKp_MC12TuneV4_ProbNNk, &b_LbKp_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNp", &LbKp_MC12TuneV4_ProbNNp, &b_LbKp_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LbKp_MC12TuneV4_ProbNNghost", &LbKp_MC12TuneV4_ProbNNghost, &b_LbKp_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNe", &LbKp_MC15TuneV1_ProbNNe, &b_LbKp_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNmu", &LbKp_MC15TuneV1_ProbNNmu, &b_LbKp_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNpi", &LbKp_MC15TuneV1_ProbNNpi, &b_LbKp_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNk", &LbKp_MC15TuneV1_ProbNNk, &b_LbKp_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNp", &LbKp_MC15TuneV1_ProbNNp, &b_LbKp_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LbKp_MC15TuneV1_ProbNNghost", &LbKp_MC15TuneV1_ProbNNghost, &b_LbKp_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LbKp_OWNPV_X", &LbKp_OWNPV_X, &b_LbKp_OWNPV_X);
   fChain->SetBranchAddress("LbKp_OWNPV_Y", &LbKp_OWNPV_Y, &b_LbKp_OWNPV_Y);
   fChain->SetBranchAddress("LbKp_OWNPV_Z", &LbKp_OWNPV_Z, &b_LbKp_OWNPV_Z);
   fChain->SetBranchAddress("LbKp_OWNPV_XERR", &LbKp_OWNPV_XERR, &b_LbKp_OWNPV_XERR);
   fChain->SetBranchAddress("LbKp_OWNPV_YERR", &LbKp_OWNPV_YERR, &b_LbKp_OWNPV_YERR);
   fChain->SetBranchAddress("LbKp_OWNPV_ZERR", &LbKp_OWNPV_ZERR, &b_LbKp_OWNPV_ZERR);
   fChain->SetBranchAddress("LbKp_OWNPV_CHI2", &LbKp_OWNPV_CHI2, &b_LbKp_OWNPV_CHI2);
   fChain->SetBranchAddress("LbKp_OWNPV_NDOF", &LbKp_OWNPV_NDOF, &b_LbKp_OWNPV_NDOF);
   fChain->SetBranchAddress("LbKp_OWNPV_COV_", LbKp_OWNPV_COV_, &b_LbKp_OWNPV_COV_);
   fChain->SetBranchAddress("LbKp_IP_OWNPV", &LbKp_IP_OWNPV, &b_LbKp_IP_OWNPV);
   fChain->SetBranchAddress("LbKp_IPCHI2_OWNPV", &LbKp_IPCHI2_OWNPV, &b_LbKp_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LbKp_ORIVX_X", &LbKp_ORIVX_X, &b_LbKp_ORIVX_X);
   fChain->SetBranchAddress("LbKp_ORIVX_Y", &LbKp_ORIVX_Y, &b_LbKp_ORIVX_Y);
   fChain->SetBranchAddress("LbKp_ORIVX_Z", &LbKp_ORIVX_Z, &b_LbKp_ORIVX_Z);
   fChain->SetBranchAddress("LbKp_ORIVX_XERR", &LbKp_ORIVX_XERR, &b_LbKp_ORIVX_XERR);
   fChain->SetBranchAddress("LbKp_ORIVX_YERR", &LbKp_ORIVX_YERR, &b_LbKp_ORIVX_YERR);
   fChain->SetBranchAddress("LbKp_ORIVX_ZERR", &LbKp_ORIVX_ZERR, &b_LbKp_ORIVX_ZERR);
   fChain->SetBranchAddress("LbKp_ORIVX_CHI2", &LbKp_ORIVX_CHI2, &b_LbKp_ORIVX_CHI2);
   fChain->SetBranchAddress("LbKp_ORIVX_NDOF", &LbKp_ORIVX_NDOF, &b_LbKp_ORIVX_NDOF);
   fChain->SetBranchAddress("LbKp_ORIVX_COV_", LbKp_ORIVX_COV_, &b_LbKp_ORIVX_COV_);
   fChain->SetBranchAddress("LbKp_P", &LbKp_P, &b_LbKp_P);
   fChain->SetBranchAddress("LbKp_PT", &LbKp_PT, &b_LbKp_PT);
   fChain->SetBranchAddress("LbKp_PE", &LbKp_PE, &b_LbKp_PE);
   fChain->SetBranchAddress("LbKp_PX", &LbKp_PX, &b_LbKp_PX);
   fChain->SetBranchAddress("LbKp_PY", &LbKp_PY, &b_LbKp_PY);
   fChain->SetBranchAddress("LbKp_PZ", &LbKp_PZ, &b_LbKp_PZ);
   fChain->SetBranchAddress("LbKp_M", &LbKp_M, &b_LbKp_M);
   fChain->SetBranchAddress("LbKp_ID", &LbKp_ID, &b_LbKp_ID);
   fChain->SetBranchAddress("LbKp_PIDe", &LbKp_PIDe, &b_LbKp_PIDe);
   fChain->SetBranchAddress("LbKp_PIDmu", &LbKp_PIDmu, &b_LbKp_PIDmu);
   fChain->SetBranchAddress("LbKp_PIDK", &LbKp_PIDK, &b_LbKp_PIDK);
   fChain->SetBranchAddress("LbKp_PIDp", &LbKp_PIDp, &b_LbKp_PIDp);
   fChain->SetBranchAddress("LbKp_ProbNNe", &LbKp_ProbNNe, &b_LbKp_ProbNNe);
   fChain->SetBranchAddress("LbKp_ProbNNk", &LbKp_ProbNNk, &b_LbKp_ProbNNk);
   fChain->SetBranchAddress("LbKp_ProbNNp", &LbKp_ProbNNp, &b_LbKp_ProbNNp);
   fChain->SetBranchAddress("LbKp_ProbNNpi", &LbKp_ProbNNpi, &b_LbKp_ProbNNpi);
   fChain->SetBranchAddress("LbKp_ProbNNmu", &LbKp_ProbNNmu, &b_LbKp_ProbNNmu);
   fChain->SetBranchAddress("LbKp_ProbNNghost", &LbKp_ProbNNghost, &b_LbKp_ProbNNghost);
   fChain->SetBranchAddress("LbKp_hasMuon", &LbKp_hasMuon, &b_LbKp_hasMuon);
   fChain->SetBranchAddress("LbKp_isMuon", &LbKp_isMuon, &b_LbKp_isMuon);
   fChain->SetBranchAddress("LbKp_hasRich", &LbKp_hasRich, &b_LbKp_hasRich);
   fChain->SetBranchAddress("LbKp_hasCalo", &LbKp_hasCalo, &b_LbKp_hasCalo);
   fChain->SetBranchAddress("LbKp_PP_CombDLLe", &LbKp_PP_CombDLLe, &b_LbKp_PP_CombDLLe);
   fChain->SetBranchAddress("LbKp_PP_CombDLLmu", &LbKp_PP_CombDLLmu, &b_LbKp_PP_CombDLLmu);
   fChain->SetBranchAddress("LbKp_PP_CombDLLpi", &LbKp_PP_CombDLLpi, &b_LbKp_PP_CombDLLpi);
   fChain->SetBranchAddress("LbKp_PP_CombDLLk", &LbKp_PP_CombDLLk, &b_LbKp_PP_CombDLLk);
   fChain->SetBranchAddress("LbKp_PP_CombDLLp", &LbKp_PP_CombDLLp, &b_LbKp_PP_CombDLLp);
   fChain->SetBranchAddress("LbKp_PP_CombDLLd", &LbKp_PP_CombDLLd, &b_LbKp_PP_CombDLLd);
   fChain->SetBranchAddress("LbKp_PP_ProbNNe", &LbKp_PP_ProbNNe, &b_LbKp_PP_ProbNNe);
   fChain->SetBranchAddress("LbKp_PP_ProbNNmu", &LbKp_PP_ProbNNmu, &b_LbKp_PP_ProbNNmu);
   fChain->SetBranchAddress("LbKp_PP_ProbNNpi", &LbKp_PP_ProbNNpi, &b_LbKp_PP_ProbNNpi);
   fChain->SetBranchAddress("LbKp_PP_ProbNNk", &LbKp_PP_ProbNNk, &b_LbKp_PP_ProbNNk);
   fChain->SetBranchAddress("LbKp_PP_ProbNNp", &LbKp_PP_ProbNNp, &b_LbKp_PP_ProbNNp);
   fChain->SetBranchAddress("LbKp_PP_ProbNNghost", &LbKp_PP_ProbNNghost, &b_LbKp_PP_ProbNNghost);
   fChain->SetBranchAddress("LbKp_L0Global_Dec", &LbKp_L0Global_Dec, &b_LbKp_L0Global_Dec);
   fChain->SetBranchAddress("LbKp_L0Global_TIS", &LbKp_L0Global_TIS, &b_LbKp_L0Global_TIS);
   fChain->SetBranchAddress("LbKp_L0Global_TOS", &LbKp_L0Global_TOS, &b_LbKp_L0Global_TOS);
   fChain->SetBranchAddress("LbKp_Hlt1Global_Dec", &LbKp_Hlt1Global_Dec, &b_LbKp_Hlt1Global_Dec);
   fChain->SetBranchAddress("LbKp_Hlt1Global_TIS", &LbKp_Hlt1Global_TIS, &b_LbKp_Hlt1Global_TIS);
   fChain->SetBranchAddress("LbKp_Hlt1Global_TOS", &LbKp_Hlt1Global_TOS, &b_LbKp_Hlt1Global_TOS);
   fChain->SetBranchAddress("LbKp_Hlt1Phys_Dec", &LbKp_Hlt1Phys_Dec, &b_LbKp_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LbKp_Hlt1Phys_TIS", &LbKp_Hlt1Phys_TIS, &b_LbKp_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LbKp_Hlt1Phys_TOS", &LbKp_Hlt1Phys_TOS, &b_LbKp_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2Global_Dec", &LbKp_Hlt2Global_Dec, &b_LbKp_Hlt2Global_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2Global_TIS", &LbKp_Hlt2Global_TIS, &b_LbKp_Hlt2Global_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2Global_TOS", &LbKp_Hlt2Global_TOS, &b_LbKp_Hlt2Global_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2Phys_Dec", &LbKp_Hlt2Phys_Dec, &b_LbKp_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2Phys_TIS", &LbKp_Hlt2Phys_TIS, &b_LbKp_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2Phys_TOS", &LbKp_Hlt2Phys_TOS, &b_LbKp_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LbKp_L0HadronDecision_Dec", &LbKp_L0HadronDecision_Dec, &b_LbKp_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0HadronDecision_TIS", &LbKp_L0HadronDecision_TIS, &b_LbKp_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0HadronDecision_TOS", &LbKp_L0HadronDecision_TOS, &b_LbKp_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LbKp_L0MuonDecision_Dec", &LbKp_L0MuonDecision_Dec, &b_LbKp_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0MuonDecision_TIS", &LbKp_L0MuonDecision_TIS, &b_LbKp_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0MuonDecision_TOS", &LbKp_L0MuonDecision_TOS, &b_LbKp_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LbKp_L0ElectronDecision_Dec", &LbKp_L0ElectronDecision_Dec, &b_LbKp_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0ElectronDecision_TIS", &LbKp_L0ElectronDecision_TIS, &b_LbKp_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0ElectronDecision_TOS", &LbKp_L0ElectronDecision_TOS, &b_LbKp_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LbKp_L0PhotonDecision_Dec", &LbKp_L0PhotonDecision_Dec, &b_LbKp_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0PhotonDecision_TIS", &LbKp_L0PhotonDecision_TIS, &b_LbKp_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0PhotonDecision_TOS", &LbKp_L0PhotonDecision_TOS, &b_LbKp_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LbKp_L0DiMuonDecision_Dec", &LbKp_L0DiMuonDecision_Dec, &b_LbKp_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0DiMuonDecision_TIS", &LbKp_L0DiMuonDecision_TIS, &b_LbKp_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0DiMuonDecision_TOS", &LbKp_L0DiMuonDecision_TOS, &b_LbKp_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LbKp_L0DiHadronDecision_Dec", &LbKp_L0DiHadronDecision_Dec, &b_LbKp_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LbKp_L0DiHadronDecision_TIS", &LbKp_L0DiHadronDecision_TIS, &b_LbKp_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LbKp_L0DiHadronDecision_TOS", &LbKp_L0DiHadronDecision_TOS, &b_LbKp_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LbKp_Hlt1TrackAllL0Decision_Dec", &LbKp_Hlt1TrackAllL0Decision_Dec, &b_LbKp_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LbKp_Hlt1TrackAllL0Decision_TIS", &LbKp_Hlt1TrackAllL0Decision_TIS, &b_LbKp_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LbKp_Hlt1TrackAllL0Decision_TOS", &LbKp_Hlt1TrackAllL0Decision_TOS, &b_LbKp_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo2BodyBBDTDecision_Dec", &LbKp_Hlt2Topo2BodyBBDTDecision_Dec, &b_LbKp_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2Topo2BodyBBDTDecision_TIS", &LbKp_Hlt2Topo2BodyBBDTDecision_TIS, &b_LbKp_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo2BodyBBDTDecision_TOS", &LbKp_Hlt2Topo2BodyBBDTDecision_TOS, &b_LbKp_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo3BodyBBDTDecision_Dec", &LbKp_Hlt2Topo3BodyBBDTDecision_Dec, &b_LbKp_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2Topo3BodyBBDTDecision_TIS", &LbKp_Hlt2Topo3BodyBBDTDecision_TIS, &b_LbKp_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo3BodyBBDTDecision_TOS", &LbKp_Hlt2Topo3BodyBBDTDecision_TOS, &b_LbKp_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo4BodyBBDTDecision_Dec", &LbKp_Hlt2Topo4BodyBBDTDecision_Dec, &b_LbKp_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2Topo4BodyBBDTDecision_TIS", &LbKp_Hlt2Topo4BodyBBDTDecision_TIS, &b_LbKp_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2Topo4BodyBBDTDecision_TOS", &LbKp_Hlt2Topo4BodyBBDTDecision_TOS, &b_LbKp_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKp_Hlt2IncPhiDecision_Dec", &LbKp_Hlt2IncPhiDecision_Dec, &b_LbKp_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LbKp_Hlt2IncPhiDecision_TIS", &LbKp_Hlt2IncPhiDecision_TIS, &b_LbKp_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LbKp_Hlt2IncPhiDecision_TOS", &LbKp_Hlt2IncPhiDecision_TOS, &b_LbKp_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LbKp_TRACK_Type", &LbKp_TRACK_Type, &b_LbKp_TRACK_Type);
   fChain->SetBranchAddress("LbKp_TRACK_Key", &LbKp_TRACK_Key, &b_LbKp_TRACK_Key);
   fChain->SetBranchAddress("LbKp_TRACK_CHI2NDOF", &LbKp_TRACK_CHI2NDOF, &b_LbKp_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LbKp_TRACK_PCHI2", &LbKp_TRACK_PCHI2, &b_LbKp_TRACK_PCHI2);
   fChain->SetBranchAddress("LbKp_TRACK_MatchCHI2", &LbKp_TRACK_MatchCHI2, &b_LbKp_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LbKp_TRACK_GhostProb", &LbKp_TRACK_GhostProb, &b_LbKp_TRACK_GhostProb);
   fChain->SetBranchAddress("LbKp_TRACK_CloneDist", &LbKp_TRACK_CloneDist, &b_LbKp_TRACK_CloneDist);
   fChain->SetBranchAddress("LbKp_TRACK_Likelihood", &LbKp_TRACK_Likelihood, &b_LbKp_TRACK_Likelihood);
   fChain->SetBranchAddress("LbKm_DTF_CHI2NDOF", &LbKm_DTF_CHI2NDOF, &b_LbKm_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LbKm_DTF_CTAU", &LbKm_DTF_CTAU, &b_LbKm_DTF_CTAU);
   fChain->SetBranchAddress("LbKm_DTF_CTAUS", &LbKm_DTF_CTAUS, &b_LbKm_DTF_CTAUS);
   fChain->SetBranchAddress("LbKm_DTF_M", &LbKm_DTF_M, &b_LbKm_DTF_M);
   fChain->SetBranchAddress("LbKm_LOKI_DIRA", &LbKm_LOKI_DIRA, &b_LbKm_LOKI_DIRA);
   fChain->SetBranchAddress("LbKm_LOKI_FDCHI2", &LbKm_LOKI_FDCHI2, &b_LbKm_LOKI_FDCHI2);
   fChain->SetBranchAddress("LbKm_LOKI_FDS", &LbKm_LOKI_FDS, &b_LbKm_LOKI_FDS);
   fChain->SetBranchAddress("LbKm_LV01", &LbKm_LV01, &b_LbKm_LV01);
   fChain->SetBranchAddress("LbKm_LV02", &LbKm_LV02, &b_LbKm_LV02);
   fChain->SetBranchAddress("LbKm_Lc_DTF_CTAU", &LbKm_Lc_DTF_CTAU, &b_LbKm_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LbKm_Lc_DTF_CTAUS", &LbKm_Lc_DTF_CTAUS, &b_LbKm_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNe", &LbKm_MC12TuneV2_ProbNNe, &b_LbKm_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNmu", &LbKm_MC12TuneV2_ProbNNmu, &b_LbKm_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNpi", &LbKm_MC12TuneV2_ProbNNpi, &b_LbKm_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNk", &LbKm_MC12TuneV2_ProbNNk, &b_LbKm_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNp", &LbKm_MC12TuneV2_ProbNNp, &b_LbKm_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LbKm_MC12TuneV2_ProbNNghost", &LbKm_MC12TuneV2_ProbNNghost, &b_LbKm_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNe", &LbKm_MC12TuneV3_ProbNNe, &b_LbKm_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNmu", &LbKm_MC12TuneV3_ProbNNmu, &b_LbKm_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNpi", &LbKm_MC12TuneV3_ProbNNpi, &b_LbKm_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNk", &LbKm_MC12TuneV3_ProbNNk, &b_LbKm_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNp", &LbKm_MC12TuneV3_ProbNNp, &b_LbKm_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LbKm_MC12TuneV3_ProbNNghost", &LbKm_MC12TuneV3_ProbNNghost, &b_LbKm_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNe", &LbKm_MC12TuneV4_ProbNNe, &b_LbKm_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNmu", &LbKm_MC12TuneV4_ProbNNmu, &b_LbKm_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNpi", &LbKm_MC12TuneV4_ProbNNpi, &b_LbKm_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNk", &LbKm_MC12TuneV4_ProbNNk, &b_LbKm_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNp", &LbKm_MC12TuneV4_ProbNNp, &b_LbKm_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LbKm_MC12TuneV4_ProbNNghost", &LbKm_MC12TuneV4_ProbNNghost, &b_LbKm_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNe", &LbKm_MC15TuneV1_ProbNNe, &b_LbKm_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNmu", &LbKm_MC15TuneV1_ProbNNmu, &b_LbKm_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNpi", &LbKm_MC15TuneV1_ProbNNpi, &b_LbKm_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNk", &LbKm_MC15TuneV1_ProbNNk, &b_LbKm_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNp", &LbKm_MC15TuneV1_ProbNNp, &b_LbKm_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LbKm_MC15TuneV1_ProbNNghost", &LbKm_MC15TuneV1_ProbNNghost, &b_LbKm_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LbKm_OWNPV_X", &LbKm_OWNPV_X, &b_LbKm_OWNPV_X);
   fChain->SetBranchAddress("LbKm_OWNPV_Y", &LbKm_OWNPV_Y, &b_LbKm_OWNPV_Y);
   fChain->SetBranchAddress("LbKm_OWNPV_Z", &LbKm_OWNPV_Z, &b_LbKm_OWNPV_Z);
   fChain->SetBranchAddress("LbKm_OWNPV_XERR", &LbKm_OWNPV_XERR, &b_LbKm_OWNPV_XERR);
   fChain->SetBranchAddress("LbKm_OWNPV_YERR", &LbKm_OWNPV_YERR, &b_LbKm_OWNPV_YERR);
   fChain->SetBranchAddress("LbKm_OWNPV_ZERR", &LbKm_OWNPV_ZERR, &b_LbKm_OWNPV_ZERR);
   fChain->SetBranchAddress("LbKm_OWNPV_CHI2", &LbKm_OWNPV_CHI2, &b_LbKm_OWNPV_CHI2);
   fChain->SetBranchAddress("LbKm_OWNPV_NDOF", &LbKm_OWNPV_NDOF, &b_LbKm_OWNPV_NDOF);
   fChain->SetBranchAddress("LbKm_OWNPV_COV_", LbKm_OWNPV_COV_, &b_LbKm_OWNPV_COV_);
   fChain->SetBranchAddress("LbKm_IP_OWNPV", &LbKm_IP_OWNPV, &b_LbKm_IP_OWNPV);
   fChain->SetBranchAddress("LbKm_IPCHI2_OWNPV", &LbKm_IPCHI2_OWNPV, &b_LbKm_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LbKm_ORIVX_X", &LbKm_ORIVX_X, &b_LbKm_ORIVX_X);
   fChain->SetBranchAddress("LbKm_ORIVX_Y", &LbKm_ORIVX_Y, &b_LbKm_ORIVX_Y);
   fChain->SetBranchAddress("LbKm_ORIVX_Z", &LbKm_ORIVX_Z, &b_LbKm_ORIVX_Z);
   fChain->SetBranchAddress("LbKm_ORIVX_XERR", &LbKm_ORIVX_XERR, &b_LbKm_ORIVX_XERR);
   fChain->SetBranchAddress("LbKm_ORIVX_YERR", &LbKm_ORIVX_YERR, &b_LbKm_ORIVX_YERR);
   fChain->SetBranchAddress("LbKm_ORIVX_ZERR", &LbKm_ORIVX_ZERR, &b_LbKm_ORIVX_ZERR);
   fChain->SetBranchAddress("LbKm_ORIVX_CHI2", &LbKm_ORIVX_CHI2, &b_LbKm_ORIVX_CHI2);
   fChain->SetBranchAddress("LbKm_ORIVX_NDOF", &LbKm_ORIVX_NDOF, &b_LbKm_ORIVX_NDOF);
   fChain->SetBranchAddress("LbKm_ORIVX_COV_", LbKm_ORIVX_COV_, &b_LbKm_ORIVX_COV_);
   fChain->SetBranchAddress("LbKm_P", &LbKm_P, &b_LbKm_P);
   fChain->SetBranchAddress("LbKm_PT", &LbKm_PT, &b_LbKm_PT);
   fChain->SetBranchAddress("LbKm_PE", &LbKm_PE, &b_LbKm_PE);
   fChain->SetBranchAddress("LbKm_PX", &LbKm_PX, &b_LbKm_PX);
   fChain->SetBranchAddress("LbKm_PY", &LbKm_PY, &b_LbKm_PY);
   fChain->SetBranchAddress("LbKm_PZ", &LbKm_PZ, &b_LbKm_PZ);
   fChain->SetBranchAddress("LbKm_M", &LbKm_M, &b_LbKm_M);
   fChain->SetBranchAddress("LbKm_ID", &LbKm_ID, &b_LbKm_ID);
   fChain->SetBranchAddress("LbKm_PIDe", &LbKm_PIDe, &b_LbKm_PIDe);
   fChain->SetBranchAddress("LbKm_PIDmu", &LbKm_PIDmu, &b_LbKm_PIDmu);
   fChain->SetBranchAddress("LbKm_PIDK", &LbKm_PIDK, &b_LbKm_PIDK);
   fChain->SetBranchAddress("LbKm_PIDp", &LbKm_PIDp, &b_LbKm_PIDp);
   fChain->SetBranchAddress("LbKm_ProbNNe", &LbKm_ProbNNe, &b_LbKm_ProbNNe);
   fChain->SetBranchAddress("LbKm_ProbNNk", &LbKm_ProbNNk, &b_LbKm_ProbNNk);
   fChain->SetBranchAddress("LbKm_ProbNNp", &LbKm_ProbNNp, &b_LbKm_ProbNNp);
   fChain->SetBranchAddress("LbKm_ProbNNpi", &LbKm_ProbNNpi, &b_LbKm_ProbNNpi);
   fChain->SetBranchAddress("LbKm_ProbNNmu", &LbKm_ProbNNmu, &b_LbKm_ProbNNmu);
   fChain->SetBranchAddress("LbKm_ProbNNghost", &LbKm_ProbNNghost, &b_LbKm_ProbNNghost);
   fChain->SetBranchAddress("LbKm_hasMuon", &LbKm_hasMuon, &b_LbKm_hasMuon);
   fChain->SetBranchAddress("LbKm_isMuon", &LbKm_isMuon, &b_LbKm_isMuon);
   fChain->SetBranchAddress("LbKm_hasRich", &LbKm_hasRich, &b_LbKm_hasRich);
   fChain->SetBranchAddress("LbKm_hasCalo", &LbKm_hasCalo, &b_LbKm_hasCalo);
   fChain->SetBranchAddress("LbKm_PP_CombDLLe", &LbKm_PP_CombDLLe, &b_LbKm_PP_CombDLLe);
   fChain->SetBranchAddress("LbKm_PP_CombDLLmu", &LbKm_PP_CombDLLmu, &b_LbKm_PP_CombDLLmu);
   fChain->SetBranchAddress("LbKm_PP_CombDLLpi", &LbKm_PP_CombDLLpi, &b_LbKm_PP_CombDLLpi);
   fChain->SetBranchAddress("LbKm_PP_CombDLLk", &LbKm_PP_CombDLLk, &b_LbKm_PP_CombDLLk);
   fChain->SetBranchAddress("LbKm_PP_CombDLLp", &LbKm_PP_CombDLLp, &b_LbKm_PP_CombDLLp);
   fChain->SetBranchAddress("LbKm_PP_CombDLLd", &LbKm_PP_CombDLLd, &b_LbKm_PP_CombDLLd);
   fChain->SetBranchAddress("LbKm_PP_ProbNNe", &LbKm_PP_ProbNNe, &b_LbKm_PP_ProbNNe);
   fChain->SetBranchAddress("LbKm_PP_ProbNNmu", &LbKm_PP_ProbNNmu, &b_LbKm_PP_ProbNNmu);
   fChain->SetBranchAddress("LbKm_PP_ProbNNpi", &LbKm_PP_ProbNNpi, &b_LbKm_PP_ProbNNpi);
   fChain->SetBranchAddress("LbKm_PP_ProbNNk", &LbKm_PP_ProbNNk, &b_LbKm_PP_ProbNNk);
   fChain->SetBranchAddress("LbKm_PP_ProbNNp", &LbKm_PP_ProbNNp, &b_LbKm_PP_ProbNNp);
   fChain->SetBranchAddress("LbKm_PP_ProbNNghost", &LbKm_PP_ProbNNghost, &b_LbKm_PP_ProbNNghost);
   fChain->SetBranchAddress("LbKm_L0Global_Dec", &LbKm_L0Global_Dec, &b_LbKm_L0Global_Dec);
   fChain->SetBranchAddress("LbKm_L0Global_TIS", &LbKm_L0Global_TIS, &b_LbKm_L0Global_TIS);
   fChain->SetBranchAddress("LbKm_L0Global_TOS", &LbKm_L0Global_TOS, &b_LbKm_L0Global_TOS);
   fChain->SetBranchAddress("LbKm_Hlt1Global_Dec", &LbKm_Hlt1Global_Dec, &b_LbKm_Hlt1Global_Dec);
   fChain->SetBranchAddress("LbKm_Hlt1Global_TIS", &LbKm_Hlt1Global_TIS, &b_LbKm_Hlt1Global_TIS);
   fChain->SetBranchAddress("LbKm_Hlt1Global_TOS", &LbKm_Hlt1Global_TOS, &b_LbKm_Hlt1Global_TOS);
   fChain->SetBranchAddress("LbKm_Hlt1Phys_Dec", &LbKm_Hlt1Phys_Dec, &b_LbKm_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LbKm_Hlt1Phys_TIS", &LbKm_Hlt1Phys_TIS, &b_LbKm_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LbKm_Hlt1Phys_TOS", &LbKm_Hlt1Phys_TOS, &b_LbKm_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2Global_Dec", &LbKm_Hlt2Global_Dec, &b_LbKm_Hlt2Global_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2Global_TIS", &LbKm_Hlt2Global_TIS, &b_LbKm_Hlt2Global_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2Global_TOS", &LbKm_Hlt2Global_TOS, &b_LbKm_Hlt2Global_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2Phys_Dec", &LbKm_Hlt2Phys_Dec, &b_LbKm_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2Phys_TIS", &LbKm_Hlt2Phys_TIS, &b_LbKm_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2Phys_TOS", &LbKm_Hlt2Phys_TOS, &b_LbKm_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LbKm_L0HadronDecision_Dec", &LbKm_L0HadronDecision_Dec, &b_LbKm_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0HadronDecision_TIS", &LbKm_L0HadronDecision_TIS, &b_LbKm_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0HadronDecision_TOS", &LbKm_L0HadronDecision_TOS, &b_LbKm_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LbKm_L0MuonDecision_Dec", &LbKm_L0MuonDecision_Dec, &b_LbKm_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0MuonDecision_TIS", &LbKm_L0MuonDecision_TIS, &b_LbKm_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0MuonDecision_TOS", &LbKm_L0MuonDecision_TOS, &b_LbKm_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LbKm_L0ElectronDecision_Dec", &LbKm_L0ElectronDecision_Dec, &b_LbKm_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0ElectronDecision_TIS", &LbKm_L0ElectronDecision_TIS, &b_LbKm_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0ElectronDecision_TOS", &LbKm_L0ElectronDecision_TOS, &b_LbKm_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LbKm_L0PhotonDecision_Dec", &LbKm_L0PhotonDecision_Dec, &b_LbKm_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0PhotonDecision_TIS", &LbKm_L0PhotonDecision_TIS, &b_LbKm_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0PhotonDecision_TOS", &LbKm_L0PhotonDecision_TOS, &b_LbKm_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LbKm_L0DiMuonDecision_Dec", &LbKm_L0DiMuonDecision_Dec, &b_LbKm_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0DiMuonDecision_TIS", &LbKm_L0DiMuonDecision_TIS, &b_LbKm_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0DiMuonDecision_TOS", &LbKm_L0DiMuonDecision_TOS, &b_LbKm_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LbKm_L0DiHadronDecision_Dec", &LbKm_L0DiHadronDecision_Dec, &b_LbKm_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LbKm_L0DiHadronDecision_TIS", &LbKm_L0DiHadronDecision_TIS, &b_LbKm_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LbKm_L0DiHadronDecision_TOS", &LbKm_L0DiHadronDecision_TOS, &b_LbKm_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LbKm_Hlt1TrackAllL0Decision_Dec", &LbKm_Hlt1TrackAllL0Decision_Dec, &b_LbKm_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LbKm_Hlt1TrackAllL0Decision_TIS", &LbKm_Hlt1TrackAllL0Decision_TIS, &b_LbKm_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LbKm_Hlt1TrackAllL0Decision_TOS", &LbKm_Hlt1TrackAllL0Decision_TOS, &b_LbKm_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo2BodyBBDTDecision_Dec", &LbKm_Hlt2Topo2BodyBBDTDecision_Dec, &b_LbKm_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2Topo2BodyBBDTDecision_TIS", &LbKm_Hlt2Topo2BodyBBDTDecision_TIS, &b_LbKm_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo2BodyBBDTDecision_TOS", &LbKm_Hlt2Topo2BodyBBDTDecision_TOS, &b_LbKm_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo3BodyBBDTDecision_Dec", &LbKm_Hlt2Topo3BodyBBDTDecision_Dec, &b_LbKm_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2Topo3BodyBBDTDecision_TIS", &LbKm_Hlt2Topo3BodyBBDTDecision_TIS, &b_LbKm_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo3BodyBBDTDecision_TOS", &LbKm_Hlt2Topo3BodyBBDTDecision_TOS, &b_LbKm_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo4BodyBBDTDecision_Dec", &LbKm_Hlt2Topo4BodyBBDTDecision_Dec, &b_LbKm_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2Topo4BodyBBDTDecision_TIS", &LbKm_Hlt2Topo4BodyBBDTDecision_TIS, &b_LbKm_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2Topo4BodyBBDTDecision_TOS", &LbKm_Hlt2Topo4BodyBBDTDecision_TOS, &b_LbKm_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbKm_Hlt2IncPhiDecision_Dec", &LbKm_Hlt2IncPhiDecision_Dec, &b_LbKm_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LbKm_Hlt2IncPhiDecision_TIS", &LbKm_Hlt2IncPhiDecision_TIS, &b_LbKm_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LbKm_Hlt2IncPhiDecision_TOS", &LbKm_Hlt2IncPhiDecision_TOS, &b_LbKm_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LbKm_TRACK_Type", &LbKm_TRACK_Type, &b_LbKm_TRACK_Type);
   fChain->SetBranchAddress("LbKm_TRACK_Key", &LbKm_TRACK_Key, &b_LbKm_TRACK_Key);
   fChain->SetBranchAddress("LbKm_TRACK_CHI2NDOF", &LbKm_TRACK_CHI2NDOF, &b_LbKm_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LbKm_TRACK_PCHI2", &LbKm_TRACK_PCHI2, &b_LbKm_TRACK_PCHI2);
   fChain->SetBranchAddress("LbKm_TRACK_MatchCHI2", &LbKm_TRACK_MatchCHI2, &b_LbKm_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LbKm_TRACK_GhostProb", &LbKm_TRACK_GhostProb, &b_LbKm_TRACK_GhostProb);
   fChain->SetBranchAddress("LbKm_TRACK_CloneDist", &LbKm_TRACK_CloneDist, &b_LbKm_TRACK_CloneDist);
   fChain->SetBranchAddress("LbKm_TRACK_Likelihood", &LbKm_TRACK_Likelihood, &b_LbKm_TRACK_Likelihood);
   fChain->SetBranchAddress("LbPi_DTF_CHI2NDOF", &LbPi_DTF_CHI2NDOF, &b_LbPi_DTF_CHI2NDOF);
   fChain->SetBranchAddress("LbPi_DTF_CTAU", &LbPi_DTF_CTAU, &b_LbPi_DTF_CTAU);
   fChain->SetBranchAddress("LbPi_DTF_CTAUS", &LbPi_DTF_CTAUS, &b_LbPi_DTF_CTAUS);
   fChain->SetBranchAddress("LbPi_DTF_M", &LbPi_DTF_M, &b_LbPi_DTF_M);
   fChain->SetBranchAddress("LbPi_LOKI_DIRA", &LbPi_LOKI_DIRA, &b_LbPi_LOKI_DIRA);
   fChain->SetBranchAddress("LbPi_LOKI_FDCHI2", &LbPi_LOKI_FDCHI2, &b_LbPi_LOKI_FDCHI2);
   fChain->SetBranchAddress("LbPi_LOKI_FDS", &LbPi_LOKI_FDS, &b_LbPi_LOKI_FDS);
   fChain->SetBranchAddress("LbPi_LV01", &LbPi_LV01, &b_LbPi_LV01);
   fChain->SetBranchAddress("LbPi_LV02", &LbPi_LV02, &b_LbPi_LV02);
   fChain->SetBranchAddress("LbPi_Lc_DTF_CTAU", &LbPi_Lc_DTF_CTAU, &b_LbPi_Lc_DTF_CTAU);
   fChain->SetBranchAddress("LbPi_Lc_DTF_CTAUS", &LbPi_Lc_DTF_CTAUS, &b_LbPi_Lc_DTF_CTAUS);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNe", &LbPi_MC12TuneV2_ProbNNe, &b_LbPi_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNmu", &LbPi_MC12TuneV2_ProbNNmu, &b_LbPi_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNpi", &LbPi_MC12TuneV2_ProbNNpi, &b_LbPi_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNk", &LbPi_MC12TuneV2_ProbNNk, &b_LbPi_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNp", &LbPi_MC12TuneV2_ProbNNp, &b_LbPi_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("LbPi_MC12TuneV2_ProbNNghost", &LbPi_MC12TuneV2_ProbNNghost, &b_LbPi_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNe", &LbPi_MC12TuneV3_ProbNNe, &b_LbPi_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNmu", &LbPi_MC12TuneV3_ProbNNmu, &b_LbPi_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNpi", &LbPi_MC12TuneV3_ProbNNpi, &b_LbPi_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNk", &LbPi_MC12TuneV3_ProbNNk, &b_LbPi_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNp", &LbPi_MC12TuneV3_ProbNNp, &b_LbPi_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("LbPi_MC12TuneV3_ProbNNghost", &LbPi_MC12TuneV3_ProbNNghost, &b_LbPi_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNe", &LbPi_MC12TuneV4_ProbNNe, &b_LbPi_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNmu", &LbPi_MC12TuneV4_ProbNNmu, &b_LbPi_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNpi", &LbPi_MC12TuneV4_ProbNNpi, &b_LbPi_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNk", &LbPi_MC12TuneV4_ProbNNk, &b_LbPi_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNp", &LbPi_MC12TuneV4_ProbNNp, &b_LbPi_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("LbPi_MC12TuneV4_ProbNNghost", &LbPi_MC12TuneV4_ProbNNghost, &b_LbPi_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNe", &LbPi_MC15TuneV1_ProbNNe, &b_LbPi_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNmu", &LbPi_MC15TuneV1_ProbNNmu, &b_LbPi_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNpi", &LbPi_MC15TuneV1_ProbNNpi, &b_LbPi_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNk", &LbPi_MC15TuneV1_ProbNNk, &b_LbPi_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNp", &LbPi_MC15TuneV1_ProbNNp, &b_LbPi_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("LbPi_MC15TuneV1_ProbNNghost", &LbPi_MC15TuneV1_ProbNNghost, &b_LbPi_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("LbPi_OWNPV_X", &LbPi_OWNPV_X, &b_LbPi_OWNPV_X);
   fChain->SetBranchAddress("LbPi_OWNPV_Y", &LbPi_OWNPV_Y, &b_LbPi_OWNPV_Y);
   fChain->SetBranchAddress("LbPi_OWNPV_Z", &LbPi_OWNPV_Z, &b_LbPi_OWNPV_Z);
   fChain->SetBranchAddress("LbPi_OWNPV_XERR", &LbPi_OWNPV_XERR, &b_LbPi_OWNPV_XERR);
   fChain->SetBranchAddress("LbPi_OWNPV_YERR", &LbPi_OWNPV_YERR, &b_LbPi_OWNPV_YERR);
   fChain->SetBranchAddress("LbPi_OWNPV_ZERR", &LbPi_OWNPV_ZERR, &b_LbPi_OWNPV_ZERR);
   fChain->SetBranchAddress("LbPi_OWNPV_CHI2", &LbPi_OWNPV_CHI2, &b_LbPi_OWNPV_CHI2);
   fChain->SetBranchAddress("LbPi_OWNPV_NDOF", &LbPi_OWNPV_NDOF, &b_LbPi_OWNPV_NDOF);
   fChain->SetBranchAddress("LbPi_OWNPV_COV_", LbPi_OWNPV_COV_, &b_LbPi_OWNPV_COV_);
   fChain->SetBranchAddress("LbPi_IP_OWNPV", &LbPi_IP_OWNPV, &b_LbPi_IP_OWNPV);
   fChain->SetBranchAddress("LbPi_IPCHI2_OWNPV", &LbPi_IPCHI2_OWNPV, &b_LbPi_IPCHI2_OWNPV);
   fChain->SetBranchAddress("LbPi_ORIVX_X", &LbPi_ORIVX_X, &b_LbPi_ORIVX_X);
   fChain->SetBranchAddress("LbPi_ORIVX_Y", &LbPi_ORIVX_Y, &b_LbPi_ORIVX_Y);
   fChain->SetBranchAddress("LbPi_ORIVX_Z", &LbPi_ORIVX_Z, &b_LbPi_ORIVX_Z);
   fChain->SetBranchAddress("LbPi_ORIVX_XERR", &LbPi_ORIVX_XERR, &b_LbPi_ORIVX_XERR);
   fChain->SetBranchAddress("LbPi_ORIVX_YERR", &LbPi_ORIVX_YERR, &b_LbPi_ORIVX_YERR);
   fChain->SetBranchAddress("LbPi_ORIVX_ZERR", &LbPi_ORIVX_ZERR, &b_LbPi_ORIVX_ZERR);
   fChain->SetBranchAddress("LbPi_ORIVX_CHI2", &LbPi_ORIVX_CHI2, &b_LbPi_ORIVX_CHI2);
   fChain->SetBranchAddress("LbPi_ORIVX_NDOF", &LbPi_ORIVX_NDOF, &b_LbPi_ORIVX_NDOF);
   fChain->SetBranchAddress("LbPi_ORIVX_COV_", LbPi_ORIVX_COV_, &b_LbPi_ORIVX_COV_);
   fChain->SetBranchAddress("LbPi_P", &LbPi_P, &b_LbPi_P);
   fChain->SetBranchAddress("LbPi_PT", &LbPi_PT, &b_LbPi_PT);
   fChain->SetBranchAddress("LbPi_PE", &LbPi_PE, &b_LbPi_PE);
   fChain->SetBranchAddress("LbPi_PX", &LbPi_PX, &b_LbPi_PX);
   fChain->SetBranchAddress("LbPi_PY", &LbPi_PY, &b_LbPi_PY);
   fChain->SetBranchAddress("LbPi_PZ", &LbPi_PZ, &b_LbPi_PZ);
   fChain->SetBranchAddress("LbPi_M", &LbPi_M, &b_LbPi_M);
   fChain->SetBranchAddress("LbPi_ID", &LbPi_ID, &b_LbPi_ID);
   fChain->SetBranchAddress("LbPi_PIDe", &LbPi_PIDe, &b_LbPi_PIDe);
   fChain->SetBranchAddress("LbPi_PIDmu", &LbPi_PIDmu, &b_LbPi_PIDmu);
   fChain->SetBranchAddress("LbPi_PIDK", &LbPi_PIDK, &b_LbPi_PIDK);
   fChain->SetBranchAddress("LbPi_PIDp", &LbPi_PIDp, &b_LbPi_PIDp);
   fChain->SetBranchAddress("LbPi_ProbNNe", &LbPi_ProbNNe, &b_LbPi_ProbNNe);
   fChain->SetBranchAddress("LbPi_ProbNNk", &LbPi_ProbNNk, &b_LbPi_ProbNNk);
   fChain->SetBranchAddress("LbPi_ProbNNp", &LbPi_ProbNNp, &b_LbPi_ProbNNp);
   fChain->SetBranchAddress("LbPi_ProbNNpi", &LbPi_ProbNNpi, &b_LbPi_ProbNNpi);
   fChain->SetBranchAddress("LbPi_ProbNNmu", &LbPi_ProbNNmu, &b_LbPi_ProbNNmu);
   fChain->SetBranchAddress("LbPi_ProbNNghost", &LbPi_ProbNNghost, &b_LbPi_ProbNNghost);
   fChain->SetBranchAddress("LbPi_hasMuon", &LbPi_hasMuon, &b_LbPi_hasMuon);
   fChain->SetBranchAddress("LbPi_isMuon", &LbPi_isMuon, &b_LbPi_isMuon);
   fChain->SetBranchAddress("LbPi_hasRich", &LbPi_hasRich, &b_LbPi_hasRich);
   fChain->SetBranchAddress("LbPi_hasCalo", &LbPi_hasCalo, &b_LbPi_hasCalo);
   fChain->SetBranchAddress("LbPi_PP_CombDLLe", &LbPi_PP_CombDLLe, &b_LbPi_PP_CombDLLe);
   fChain->SetBranchAddress("LbPi_PP_CombDLLmu", &LbPi_PP_CombDLLmu, &b_LbPi_PP_CombDLLmu);
   fChain->SetBranchAddress("LbPi_PP_CombDLLpi", &LbPi_PP_CombDLLpi, &b_LbPi_PP_CombDLLpi);
   fChain->SetBranchAddress("LbPi_PP_CombDLLk", &LbPi_PP_CombDLLk, &b_LbPi_PP_CombDLLk);
   fChain->SetBranchAddress("LbPi_PP_CombDLLp", &LbPi_PP_CombDLLp, &b_LbPi_PP_CombDLLp);
   fChain->SetBranchAddress("LbPi_PP_CombDLLd", &LbPi_PP_CombDLLd, &b_LbPi_PP_CombDLLd);
   fChain->SetBranchAddress("LbPi_PP_ProbNNe", &LbPi_PP_ProbNNe, &b_LbPi_PP_ProbNNe);
   fChain->SetBranchAddress("LbPi_PP_ProbNNmu", &LbPi_PP_ProbNNmu, &b_LbPi_PP_ProbNNmu);
   fChain->SetBranchAddress("LbPi_PP_ProbNNpi", &LbPi_PP_ProbNNpi, &b_LbPi_PP_ProbNNpi);
   fChain->SetBranchAddress("LbPi_PP_ProbNNk", &LbPi_PP_ProbNNk, &b_LbPi_PP_ProbNNk);
   fChain->SetBranchAddress("LbPi_PP_ProbNNp", &LbPi_PP_ProbNNp, &b_LbPi_PP_ProbNNp);
   fChain->SetBranchAddress("LbPi_PP_ProbNNghost", &LbPi_PP_ProbNNghost, &b_LbPi_PP_ProbNNghost);
   fChain->SetBranchAddress("LbPi_L0Global_Dec", &LbPi_L0Global_Dec, &b_LbPi_L0Global_Dec);
   fChain->SetBranchAddress("LbPi_L0Global_TIS", &LbPi_L0Global_TIS, &b_LbPi_L0Global_TIS);
   fChain->SetBranchAddress("LbPi_L0Global_TOS", &LbPi_L0Global_TOS, &b_LbPi_L0Global_TOS);
   fChain->SetBranchAddress("LbPi_Hlt1Global_Dec", &LbPi_Hlt1Global_Dec, &b_LbPi_Hlt1Global_Dec);
   fChain->SetBranchAddress("LbPi_Hlt1Global_TIS", &LbPi_Hlt1Global_TIS, &b_LbPi_Hlt1Global_TIS);
   fChain->SetBranchAddress("LbPi_Hlt1Global_TOS", &LbPi_Hlt1Global_TOS, &b_LbPi_Hlt1Global_TOS);
   fChain->SetBranchAddress("LbPi_Hlt1Phys_Dec", &LbPi_Hlt1Phys_Dec, &b_LbPi_Hlt1Phys_Dec);
   fChain->SetBranchAddress("LbPi_Hlt1Phys_TIS", &LbPi_Hlt1Phys_TIS, &b_LbPi_Hlt1Phys_TIS);
   fChain->SetBranchAddress("LbPi_Hlt1Phys_TOS", &LbPi_Hlt1Phys_TOS, &b_LbPi_Hlt1Phys_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2Global_Dec", &LbPi_Hlt2Global_Dec, &b_LbPi_Hlt2Global_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2Global_TIS", &LbPi_Hlt2Global_TIS, &b_LbPi_Hlt2Global_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2Global_TOS", &LbPi_Hlt2Global_TOS, &b_LbPi_Hlt2Global_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2Phys_Dec", &LbPi_Hlt2Phys_Dec, &b_LbPi_Hlt2Phys_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2Phys_TIS", &LbPi_Hlt2Phys_TIS, &b_LbPi_Hlt2Phys_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2Phys_TOS", &LbPi_Hlt2Phys_TOS, &b_LbPi_Hlt2Phys_TOS);
   fChain->SetBranchAddress("LbPi_L0HadronDecision_Dec", &LbPi_L0HadronDecision_Dec, &b_LbPi_L0HadronDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0HadronDecision_TIS", &LbPi_L0HadronDecision_TIS, &b_LbPi_L0HadronDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0HadronDecision_TOS", &LbPi_L0HadronDecision_TOS, &b_LbPi_L0HadronDecision_TOS);
   fChain->SetBranchAddress("LbPi_L0MuonDecision_Dec", &LbPi_L0MuonDecision_Dec, &b_LbPi_L0MuonDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0MuonDecision_TIS", &LbPi_L0MuonDecision_TIS, &b_LbPi_L0MuonDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0MuonDecision_TOS", &LbPi_L0MuonDecision_TOS, &b_LbPi_L0MuonDecision_TOS);
   fChain->SetBranchAddress("LbPi_L0ElectronDecision_Dec", &LbPi_L0ElectronDecision_Dec, &b_LbPi_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0ElectronDecision_TIS", &LbPi_L0ElectronDecision_TIS, &b_LbPi_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0ElectronDecision_TOS", &LbPi_L0ElectronDecision_TOS, &b_LbPi_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("LbPi_L0PhotonDecision_Dec", &LbPi_L0PhotonDecision_Dec, &b_LbPi_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0PhotonDecision_TIS", &LbPi_L0PhotonDecision_TIS, &b_LbPi_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0PhotonDecision_TOS", &LbPi_L0PhotonDecision_TOS, &b_LbPi_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("LbPi_L0DiMuonDecision_Dec", &LbPi_L0DiMuonDecision_Dec, &b_LbPi_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0DiMuonDecision_TIS", &LbPi_L0DiMuonDecision_TIS, &b_LbPi_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0DiMuonDecision_TOS", &LbPi_L0DiMuonDecision_TOS, &b_LbPi_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("LbPi_L0DiHadronDecision_Dec", &LbPi_L0DiHadronDecision_Dec, &b_LbPi_L0DiHadronDecision_Dec);
   fChain->SetBranchAddress("LbPi_L0DiHadronDecision_TIS", &LbPi_L0DiHadronDecision_TIS, &b_LbPi_L0DiHadronDecision_TIS);
   fChain->SetBranchAddress("LbPi_L0DiHadronDecision_TOS", &LbPi_L0DiHadronDecision_TOS, &b_LbPi_L0DiHadronDecision_TOS);
   fChain->SetBranchAddress("LbPi_Hlt1TrackAllL0Decision_Dec", &LbPi_Hlt1TrackAllL0Decision_Dec, &b_LbPi_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("LbPi_Hlt1TrackAllL0Decision_TIS", &LbPi_Hlt1TrackAllL0Decision_TIS, &b_LbPi_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("LbPi_Hlt1TrackAllL0Decision_TOS", &LbPi_Hlt1TrackAllL0Decision_TOS, &b_LbPi_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo2BodyBBDTDecision_Dec", &LbPi_Hlt2Topo2BodyBBDTDecision_Dec, &b_LbPi_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2Topo2BodyBBDTDecision_TIS", &LbPi_Hlt2Topo2BodyBBDTDecision_TIS, &b_LbPi_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo2BodyBBDTDecision_TOS", &LbPi_Hlt2Topo2BodyBBDTDecision_TOS, &b_LbPi_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo3BodyBBDTDecision_Dec", &LbPi_Hlt2Topo3BodyBBDTDecision_Dec, &b_LbPi_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2Topo3BodyBBDTDecision_TIS", &LbPi_Hlt2Topo3BodyBBDTDecision_TIS, &b_LbPi_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo3BodyBBDTDecision_TOS", &LbPi_Hlt2Topo3BodyBBDTDecision_TOS, &b_LbPi_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo4BodyBBDTDecision_Dec", &LbPi_Hlt2Topo4BodyBBDTDecision_Dec, &b_LbPi_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2Topo4BodyBBDTDecision_TIS", &LbPi_Hlt2Topo4BodyBBDTDecision_TIS, &b_LbPi_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2Topo4BodyBBDTDecision_TOS", &LbPi_Hlt2Topo4BodyBBDTDecision_TOS, &b_LbPi_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("LbPi_Hlt2IncPhiDecision_Dec", &LbPi_Hlt2IncPhiDecision_Dec, &b_LbPi_Hlt2IncPhiDecision_Dec);
   fChain->SetBranchAddress("LbPi_Hlt2IncPhiDecision_TIS", &LbPi_Hlt2IncPhiDecision_TIS, &b_LbPi_Hlt2IncPhiDecision_TIS);
   fChain->SetBranchAddress("LbPi_Hlt2IncPhiDecision_TOS", &LbPi_Hlt2IncPhiDecision_TOS, &b_LbPi_Hlt2IncPhiDecision_TOS);
   fChain->SetBranchAddress("LbPi_TRACK_Type", &LbPi_TRACK_Type, &b_LbPi_TRACK_Type);
   fChain->SetBranchAddress("LbPi_TRACK_Key", &LbPi_TRACK_Key, &b_LbPi_TRACK_Key);
   fChain->SetBranchAddress("LbPi_TRACK_CHI2NDOF", &LbPi_TRACK_CHI2NDOF, &b_LbPi_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("LbPi_TRACK_PCHI2", &LbPi_TRACK_PCHI2, &b_LbPi_TRACK_PCHI2);
   fChain->SetBranchAddress("LbPi_TRACK_MatchCHI2", &LbPi_TRACK_MatchCHI2, &b_LbPi_TRACK_MatchCHI2);
   fChain->SetBranchAddress("LbPi_TRACK_GhostProb", &LbPi_TRACK_GhostProb, &b_LbPi_TRACK_GhostProb);
   fChain->SetBranchAddress("LbPi_TRACK_CloneDist", &LbPi_TRACK_CloneDist, &b_LbPi_TRACK_CloneDist);
   fChain->SetBranchAddress("LbPi_TRACK_Likelihood", &LbPi_TRACK_Likelihood, &b_LbPi_TRACK_Likelihood);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0HadronDecision", &L0HadronDecision, &b_L0HadronDecision);
   fChain->SetBranchAddress("L0MuonDecision", &L0MuonDecision, &b_L0MuonDecision);
   fChain->SetBranchAddress("L0ElectronDecision", &L0ElectronDecision, &b_L0ElectronDecision);
   fChain->SetBranchAddress("L0PhotonDecision", &L0PhotonDecision, &b_L0PhotonDecision);
   fChain->SetBranchAddress("L0DiMuonDecision", &L0DiMuonDecision, &b_L0DiMuonDecision);
   fChain->SetBranchAddress("L0DiHadronDecision", &L0DiHadronDecision, &b_L0DiHadronDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1TrackAllL0Decision", &Hlt1TrackAllL0Decision, &b_Hlt1TrackAllL0Decision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2Topo2BodyBBDTDecision", &Hlt2Topo2BodyBBDTDecision, &b_Hlt2Topo2BodyBBDTDecision);
   fChain->SetBranchAddress("Hlt2Topo3BodyBBDTDecision", &Hlt2Topo3BodyBBDTDecision, &b_Hlt2Topo3BodyBBDTDecision);
   fChain->SetBranchAddress("Hlt2Topo4BodyBBDTDecision", &Hlt2Topo4BodyBBDTDecision, &b_Hlt2Topo4BodyBBDTDecision);
   fChain->SetBranchAddress("Hlt2IncPhiDecision", &Hlt2IncPhiDecision, &b_Hlt2IncPhiDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   Notify();
}

Bool_t DecayTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DecayTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DecayTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
//#endif // #ifdef DecayTree_cxx
