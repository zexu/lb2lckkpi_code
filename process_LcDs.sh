date +%F" "%H:%M:%S > ouput2.log

##########################
preCut_data(){
   source ./select/Lb2LcDs/data/pre_cut/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo " pre cut data !" >> ouput2.log
}

veto_data(){
   source ./select/Lb2LcDs/data/clone_track/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo " veto data !" >> ouput2.log
}

mul_data(){
   source ./select/Lb2LcDs/data/mul_can/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo " mul veto data !" >> ouput2.log
}

###########################

preCut_mc(){
   source ./select/Lb2LcDs/mc/pre/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo " pre cut mc !" >> ouput2.log
}

veto_mc(){
   source ./select/Lb2LcDs/mc/clone_track/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo " veto mc !" >> ouput2.log
}

pre_gen(){
   source ./select/Lb2LcDs/mc/pre_gen/run.sh
   date +%F" "%H:%M:%S >> output2.log
   echo " gen level mc !" >> output2.log
}
#############################


doTrain(){
   source ./select/Lb2LcKKPi/data/tmva/train.sh
#   rm -rf ./select/Lb2LJpsiPPi/data/run1/newtmva/weights
   mv weights ./select/Lb2LcKKPi/data/tmva/
   date +%F" "%H:%M:%S >> ouput2.log
   echo "finish train " >> ouput2.log
}


bdtTo_data(){
   source   ./select/Lb2LcKKPi/data/tmva/rundata_ds.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "add bdt to data !" >> ouput2.log
}

doSplit_data(){
   source  ./RootFiles/Lb2LcDs/data/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "split data !" >> ouput2.log
}



##############################################################

doFit_mc(){
   source ./Yield/Lb2LcDs/mc_fit/doFit.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "MC Fit !" >> ouput2.log
}

doFit_data(){
   source ./Yield/Lb2LcDs/Lb2LcDs_bkg_more/doFit.sh $1
   date +%F" "%H:%M:%S >> ouput2.log
   echo "Fit with weight !" >> ouput2.log
}

doSplit_weight(){
   source  ./RootFiles/Lb2LcDs/data/runweight.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "split weight !" >> ouput2.log
}

##############################################################

pty_weight_mc(){
   source ./select/Lb2LcDs/mc/pty_weight/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "pty weight mc " >> ouput2.log
}


ntrack_weight_mc(){
   source ./select/Lb2LcDs/mc/ntrack_weight/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "ntrack weight mc " >> ouput2.log
}


dalize_weight_mc(){
   source ./select/Lb2LcDs/mc/dalitz_weight/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "dalitz weight mc " >> ouput2.log
}

poli_weight_mc(){
   source ./select/Lb2LcDs/mc/poli_weight/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "poli weight mc " >> ouput2.log
}

bdtTo_mc(){
   source  ./select/Lb2LcKKPi/data/tmva/runmc_ds.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "add bdt to mc !" >> ouput2.log
}


tracks_mc(){
   source ./select/Lb2LcDs/mc/track/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "tracks mc " >> ouput2.log
}

doSplit_mc(){
   source  ./RootFiles/Lb2LcDs/mc/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "split mc !" >> ouput2.log
}

trigger_mc(){
   source ./select/Lb2LcDs/mc/trigger/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "trigger mc " >> ouput2.log
}



pid_mc(){
   source ./select/Lb2LcDs/mc/pid/run.sh
   date +%F" "%H:%M:%S >> ouput2.log
   echo "tracks mc " >> ouput2.log
}



######################################

#preCut_data 
#veto_data 
#mul_data 
#
#preCut_mc
#veto_mc 

#pre_gen
######################################



#bdtTo_mc
#bdtTo_data

#doFit_mc 
#doFit_data 0.09


######################################
# The Efficiency
######################

#tracks_mc
#trigger_mc
#pid_mc

pty_weight_mc
ntrack_weight_mc
dalize_weight_mc
poli_weight_mc







