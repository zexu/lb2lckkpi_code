date +%F" "%H:%M:%S > output1.log

##########################
preCut_data(){
   source ./select/Lb2LcKKPi/data/pre_cut/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " pre cut data !" >> output1.log
}

veto_data(){
   source ./select/Lb2LcKKPi/data/clone_track/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " veto data !" >> output1.log
}

mul_data(){
   source ./select/Lb2LcKKPi/data/mul_can/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " mul veto data !" >> output1.log
}

###########################

preCut_mc(){
   source ./select/Lb2LcKKPi/mc/pre/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " pre cut mc !" >> output1.log
}
veto_mc(){
   source ./select/Lb2LcKKPi/mc/clone_track/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " veto mc !" >> output1.log
}

preCut_mc_kkst(){
   source ./select/Lb2LcKKst/mc/pre/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " pre cut mc !" >> output1.log
}
veto_mc_kkst(){
   source ./select/Lb2LcKKst/mc/clone_track/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " veto mc !" >> output1.log
}

preCut_mc_a1(){
   source ./select/Lb2Lca1/mc/pre/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " pre cut mc !" >> output1.log
}
veto_mc_a1(){
   source ./select/Lb2Lca1/mc/clone_track/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " veto mc !" >> output1.log
}



pre_gen(){
   source ./select/Lb2LcKKPi/mc/pre_gen/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo " gen level mc !" >> output1.log
}

#############################


doTrain(){
   source ./select/Lb2LcKKPi/data/tmva/train.sh
#   rm -rf ./select/Lb2LJpsiPPi/data/run1/newtmva/weights
   mv weights ./select/Lb2LcKKPi/data/tmva/
   date +%F" "%H:%M:%S >> output1.log
   echo "finish train " >> output1.log
}

rought_fit(){
   source ./Yield/Lb2LcKKPi/rought_yield_Lb2LcKKPi/doFit.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "rought fit " >> output1.log
}

bdtTo_data(){
   source   ./select/Lb2LcKKPi/data/tmva/rundata.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "add bdt to data !" >> output1.log
}

bdtTo_mc(){
   source  ./select/Lb2LcKKPi/data/tmva/runmc.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "add bdt to mc !" >> output1.log
}

bdtTo_mc_kkst(){
   source  ./select/Lb2LcKKPi/data/tmva/runmc_kkst.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "add bdt to mc !" >> output1.log
}

bdtTo_mc_a1(){
   source  ./select/Lb2LcKKPi/data/tmva/runmc_a1.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "add bdt to mc !" >> output1.log
}


##############################################################

doFit_mc(){
   source ./Yield/Lb2LcKKPi/mc_fit/doFit.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "MC Fit !" >> output1.log
}

combine_fit(){
   source ./Yield/combine_mc/doFit.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "combine Fit !" >> output1.log
}

doFit_data(){
   source ./Yield/Lb2LcKKPi/Lb2LcKKPi_bkg/doFit.sh $1
   date +%F" "%H:%M:%S >> output1.log
   echo "Fit with weight !" >> output1.log
}

doSplit_weight(){
   source  ./RootFiles/Lb2Lckkpi/data/runweight.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "split weight !" >> output1.log
}

##############################################################

pty_weight_mc(){
   source ./select/Lb2LcKKPi/mc/pty_weight/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "pty weight mc " >> output1.log
}


ntrack_weight_mc(){
   source ./select/Lb2LcKKPi/mc/ntrack_weight/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "ntrack weight mc " >> output1.log
}


dalize_weight_mc(){
   source ./select/Lb2LcKKPi/mc/dalitz_weight/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "dalitz weight mc " >> output1.log
}

poli_weight_mc(){
   source ./select/Lb2LcKKPi/mc/poli_weight/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "poli weight mc " >> output1.log
}



tracks_mc(){
   source ./select/Lb2LcKKPi/mc/track/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "tracks mc " >> output1.log
}

doSplit_mc(){
   source  ./RootFiles/Lb2Lckkpi/mc/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "split mc !" >> output1.log
}

trigger_mc(){
   source ./select/Lb2LcKKPi/mc/trigger/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "trigger mc " >> output1.log
}



pid_mc(){
   source ./select/Lb2LcKKPi/mc/pid/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "tracks mc " >> output1.log
}

draw_kkpi(){
   source ./RootFiles/Lb2Lckkpi/mc/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "draw kkpi " >> output1.log
}

draw_kkst(){
   source ./RootFiles/Lb2LcKKst/mc/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "draw kkst " >> output1.log
}

draw_a1(){
   source ./RootFiles/Lb2Lca1/mc/run.sh
   date +%F" "%H:%M:%S >> output1.log
   echo "draw a1 " >> output1.log
}


######################################

#preCut_data 
#veto_data 
#mul_data 
#
#preCut_mc
#veto_mc 
#preCut_mc_kkst
#veto_mc_kkst 
#preCut_mc_a1
#veto_mc_a1 
#
#pre_gen

######################################

#doTrain
#rought_fit

##bdtTo_data
##bdtTo_mc
#bdtTo_mc_kkst
#bdtTo_mc_a1

#######################################
#doFit_mc 0.35 
doFit_data 0.35

######################################
# The Efficiency
######################

tracks_mc
trigger_mc
pid_mc

pty_weight_mc
ntrack_weight_mc
dalize_weight_mc

#draw_kkpi
#draw_kkst
#draw_a1
#combine_fit

poli_weight_mc







