#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TH2D.h"
#include "TH1.h"

using namespace std;

void trigger_pi( const char* choose ) {
//chain the root files 
	TFile f_trigger("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcPi/mc/trigger/trigger_weight.root");
	TH1F *ratio1;
	TH1F *ratio2;
	ratio1 = (TH1F*)f_trigger.Get("h_data_TIS_eff");
	ratio2 = (TH1F*)f_trigger.Get("h_data_newTOS_eff");


	TString name = choose;

	TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger.root";
      TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root";

	TFile *file = new TFile(filenew,"recreate");
	TChain *chain = new TChain("DecayTree");
	chain->Add(fileold);
	int nentries = chain->GetEntries();
//define the object

	Double_t Lc_Dau_Max_PT; chain->SetBranchAddress("Lc_Dau_Max_PT",&Lc_Dau_Max_PT);
	Double_t lab0_PT;       chain->SetBranchAddress("lab0_PT",&lab0_PT);

	Bool_t lab1_L0HadronDecision_TOS;   chain->SetBranchAddress("lab1_L0HadronDecision_TOS",&lab1_L0HadronDecision_TOS);
	Bool_t lab0_L0Global_TIS;           chain->SetBranchAddress("lab0_L0Global_TIS",&lab0_L0Global_TIS);

#if 1
//create a new file and clone a tree, but do not copy entries.
	TTree *newtree = chain->CloneTree(0);
#endif

	Double_t tis_eff;		newtree->Branch("tis_eff",&tis_eff,"tis_eff/D");
	Double_t tis_eff_err;	newtree->Branch("tis_eff_err",&tis_eff_err,"tis_eff_err/D");
	Double_t tos_eff;		newtree->Branch("tos_eff",&tos_eff,"tos_eff/D");
	Double_t tos_eff_err;	newtree->Branch("tos_eff_err",&tos_eff_err,"tos_eff_err/D");

	Int_t bin1;
	Int_t bin2;

#if 1
	//for (Long64_t jentry=0; jentry<nentries; jentry++){
	for (Long64_t jentry=0; jentry<nentries; jentry++){

		chain->GetEntry(jentry);
		//if (jentry%100==0) cout << "jentry = " << jentry << endl;

		bin1 = ratio1->GetXaxis()->FindBin(lab0_PT);
		bin2 = ratio2->GetXaxis()->FindBin(Lc_Dau_Max_PT);

		tis_eff = ratio1->GetBinContent(bin1);
		tis_eff_err = ratio1->GetBinError(bin1);

		tos_eff = ratio2->GetBinContent(bin2)*(!lab0_L0Global_TIS && lab1_L0HadronDecision_TOS);
		tos_eff_err = ratio2->GetBinError(bin2)*(!lab0_L0Global_TIS && lab1_L0HadronDecision_TOS);

		newtree->Fill();
	
	}	
	std::cout << "new number: " << newtree->GetEntries() << std::endl;
	newtree->Write();
      file->Close();
#endif


	delete file;
	delete chain;


}

	





