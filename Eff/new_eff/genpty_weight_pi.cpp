//#include"TRatioPlot.h"

#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"


void genpty_weight_pi( int choose ){

   TH1::SetDefaultSumw2();

   int kptBin =6;
   int kyBin = 4;
   
   Double_t ptBin[] = {0,5000, 8000, 11000, 16000, 20000, 50000};
   Double_t yBin[] = {2, 3.5, 3.75, 4, 6};

   int knTracks = 5;
   Double_t nBin[] = {0, 100, 200, 300, 400, 800};

   TChain* chdata_Pi = new TChain();
   chdata_Pi->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/data/with_weight/Lb2Lcpi_pre_clone_BDTG_weight.root/DecayTree");
   
   TChain* chmc_Pi = new TChain();
   chmc_Pi->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down11/Lb2Lcpi_pre_clone.root/DecayTree");
   chmc_Pi->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up11/Lb2Lcpi_pre_clone.root/DecayTree");
   chmc_Pi->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down12/Lb2Lcpi_pre_clone.root/DecayTree");
   chmc_Pi->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up12/Lb2Lcpi_pre_clone.root/DecayTree");


   TH2F* hd = new TH2F("hd","hd",kptBin, ptBin, kyBin, yBin);
   chdata_Pi->Project("hd","(0.5*log((lab0_PE+lab0_P)/(lab0_PE-lab0_P))):lab0_PT","Sweight_sig");  
   hd->Sumw2();
   hd->Scale(1./hd->Integral());
   
   TH2F* hmc = new TH2F("hmc","hmc",kptBin, ptBin, kyBin, yBin);
   chmc_Pi->Project("hmc","(0.5*log((lab0_PE+lab0_P)/(lab0_PE-lab0_P))):lab0_PT");  
   hmc->Sumw2();
   hmc->Scale(1./hmc->Integral());
  
   TH2F *pty_ratio = new TH2F("pty","",kptBin, ptBin, kyBin, yBin);
   pty_ratio->GetXaxis()->SetTitle("P_{T}");
   pty_ratio->GetYaxis()->SetTitle("y");
   pty_ratio->Divide(hd,hmc);
   pty_ratio->SaveAs("/home/xuzh/work/PentaQuark/Lb2Lc/system_error/pty/rerange/weight_plot/pty_rerange_pi.root","recreate");
   
}
