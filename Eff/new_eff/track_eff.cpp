#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

#include "track_eff.h"

double eff_err[2];

void ratio(int x, int y){

   TH1F *h1 = new TH1F("h1","",1,0,1);
   h1->SetBinContent(1,x);
   h1->SetBinError(1,TMath::Sqrt(x));
   TH1F *h2 = new TH1F("h2","",1,0,1);
   h2->SetBinContent(1,y);
   h2->SetBinError(1,TMath::Sqrt(y));
   TH1F *h3 = new TH1F("h3","",1,0,1);
   h3->Divide(h1,h2);
   eff_err[0] = h3->GetBinContent(1);
   eff_err[1] = h3->GetBinError(1);

   delete h1;
   delete h2;
   delete h3;
}


using namespace std;
//void sel_eff( vector< double> &vec ){
int track_eff(vector<double>& vec){

   TChain* kkpi_tot = new TChain();
#if 1 
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");


#endif
#if 0 
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
#endif


   TH1F *kkpi_up = new TH1F("kkpi_up","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up","weight*rtrack");
   TH1F *kkpi_down = new TH1F("kkpi_down","",10,-5000,5000);
   kkpi_tot->Project("kkpi_down","weight");
   TH1F *kkpi_up_error = new TH1F("kkpi_up_error","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up_error","weight*rtrack*weight*(1-rtrack)");
   
   ratio(kkpi_up->GetMean()*kkpi_up->GetEntries(),kkpi_down->GetMean()*kkpi_down->GetEntries());
   vec.push_back(eff_err[0]);
   ratio(sqrt(abs(kkpi_up_error->GetMean()*kkpi_up_error->GetEntries())),kkpi_down->GetMean()*kkpi_down->GetEntries());
   vec.push_back(eff_err[0]);


   TChain* pi_tot = new TChain();
#if 1
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
#endif
#if 0
   pi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down12/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up12/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root/DecayTree");
#endif


   TH1F *pi_up = new TH1F("pi_up","",10,-5000,5000);
   pi_tot->Project("pi_up","weight*rtrack");
   TH1F *pi_down = new TH1F("pi_down","",10,-5000,5000);
   pi_tot->Project("pi_down","weight");
   TH1F *pi_up_error = new TH1F("pi_up_error","",10,-5000,5000);
   pi_tot->Project("pi_up_error","weight*rtrack*weight*(1-rtrack)");
   
   ratio(pi_up->GetMean()*pi_up->GetEntries(),pi_down->GetMean()*pi_down->GetEntries());
   vec.push_back(eff_err[0]);
   ratio(sqrt(abs(kkpi_up_error->GetMean()*kkpi_up_error->GetEntries())),kkpi_down->GetMean()*kkpi_down->GetEntries());
   vec.push_back(eff_err[0]);
   

#if 0
   cout << "debug: " << vec[0] << "---" << vec[1] << endl;
   
#endif


   return 0;
}







