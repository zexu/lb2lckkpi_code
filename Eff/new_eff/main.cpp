#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

#include "pty_weight_pi.h"
#include "pty_weight_kkpi.h"
#include "ntracks_weight_pi.h"
#include "ntracks_weight_kkpi.h"
#include "dalitz_weight_pi.h"
#include "dalitz_weight_kkpi.h"
#include "poli_weight_pi.h"
#include "poli_weight_kkpi.h"
#include "tracks_pi.h"
#include "tracks_kkpi.h"
#include "split_pi.h"
#include "split_kkpi.h"
#include "trigger_pi.h"
#include "trigger_kkpi.h"

#include "pid_pi_down11.h"
#include "pid_pi_up11.h"
#include "pid_kkpi_down11.h"
#include "pid_kkpi_up11.h"
#include "pid_pi_down12.h"
#include "pid_pi_up12.h"
#include "pid_kkpi_down12.h"
#include "pid_kkpi_up12.h"

#include "sel_eff.h"
#include "track_eff.h"
#include "trigger_eff.h"
#include "pid_eff.h"

#include "genpty_weight_pi.h"
#include "genpty_weight_kkpi.h"

using namespace std;

#if 1
void ratio(vector<double>& vec){
   TH1F* h1 = new TH1F("h1","",1,0,1);
   h1->SetBinContent(1,vec[0]);
   h1->SetBinError(1,vec[1]);
   TH1F* h2 = new TH1F("h2","",1,0,1);
   h2->SetBinContent(1,vec[2]);
   h2->SetBinError(1,vec[3]);
   TH1F* h3 = new TH1F("h3","",1,0,1);
   h3->Divide(h1,h2);
   vec.push_back(h3->GetBinContent(1));
   vec.push_back(h3->GetBinError(1));

   delete h1;
   delete h2;
   delete h3;
}
#endif


#if 1
void ratio2(vector<double>& vec){
   TH1F* h1 = new TH1F("h1","",1,0,1);
   h1->SetBinContent(1,vec[0]);
   h1->SetBinError(1,vec[1]);
   TH1F* h2 = new TH1F("h2","",1,0,1);
   h2->SetBinContent(1,vec[2]);
   h2->SetBinError(1,vec[3]);
   TH1F* h3 = new TH1F("h3","",1,0,1);
   h3->Divide(h1,h2);
   vec.push_back(h3->GetBinContent(1));
   vec.push_back(h3->GetBinError(1));

   h1->SetBinContent(1,vec[4]);
   h1->SetBinError(1,vec[5]);
   h2->SetBinContent(1,vec[6]);
   h2->SetBinError(1,vec[7]);
   h3->Divide(h1,h2);
   vec.push_back(h3->GetBinContent(1));
   vec.push_back(h3->GetBinError(1));

   delete h1;
   delete h2;
   delete h3;
}
#endif



int main(int argc, char **argv) {

   cout << "Start to Work !!! " << endl;

#if 0
   pty_weight_pi("down11");
   pty_weight_pi("up11");
   pty_weight_pi("down12");
   pty_weight_pi("up12");
   pty_weight_kkpi("down11");
   pty_weight_kkpi("up11");
   pty_weight_kkpi("down12");
   pty_weight_kkpi("up12");
   cout << "-------------------------------------- pty weight finished" << endl;
////////////////////////////////////////////////////

   ntracks_weight_pi("down11");
   ntracks_weight_pi("up11");
   ntracks_weight_pi("down12");
   ntracks_weight_pi("up12");
   ntracks_weight_kkpi("down11");
   ntracks_weight_kkpi("up11");
   ntracks_weight_kkpi("down12");
   ntracks_weight_kkpi("up12");
   cout << "-------------------------------------- tracks weight finished" << endl;
////////////////////////////////////////////////////

   dalitz_weight_pi("down11");
   dalitz_weight_pi("up11");
   dalitz_weight_pi("down12");
   dalitz_weight_pi("up12");
   dalitz_weight_kkpi("down11");
   dalitz_weight_kkpi("up11");
   dalitz_weight_kkpi("down12");
   dalitz_weight_kkpi("up12");
   cout << "--------------------------------------- dalitz weight finished" << endl;
////////////////////////////////////////////////////

   poli_weight_pi("down11");
   poli_weight_pi("up11");
   poli_weight_pi("down12");
   poli_weight_pi("up12");
   poli_weight_kkpi("down11");
   poli_weight_kkpi("up11");
   poli_weight_kkpi("down12");
   poli_weight_kkpi("up12");
   cout << "--------------------------------------- poli weight finished" << endl;
////////////////////////////////////////////////////

   tracks_pi("down11");
   tracks_pi("up11");
   tracks_pi("down12");
   tracks_pi("up12");
   tracks_kkpi("down11");
   tracks_kkpi("up11");
   tracks_kkpi("down12");
   tracks_kkpi("up12");
   cout << "--------------------------------------- tracking efficiency add " << endl;
////////////////////////////////////////////////////

//   trigger_pi("down11");
//   trigger_pi("up11");
//   trigger_pi("down12");
//   trigger_pi("up12");
//   trigger_kkpi("down11");
//   trigger_kkpi("up11");
 //  trigger_kkpi("down12");
//   trigger_kkpi("up12");
//   cout << "--------------------------------------- trigger efficiency add " << endl;
////////////////////////////////////////////////////

//   pid_pi_down11("");
 //  pid_pi_up11("");
 //  pid_pi_down12("");
//   pid_pi_up12("");
//   pid_kkpi_down11("");
//   pid_kkpi_up11("");
//   pid_kkpi_down12("");
 //  pid_kkpi_up12("");
 //  cout << "--------------------------------------- pid efficiency add " << endl;
////////////////////////////////////////////////////
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
   cout << "acceptrance efficiency is calculated !" << endl;
   double acc_kkpi = 0.16014;
   double acc_kkpi_err = 0.00060;
   double acc_pi = 0.1482;
   double acc_pi_err = 0.000092;
   // LcDs : 0.1482 +/- 0.000092
   vector<double> vec_acc{acc_kkpi,acc_kkpi_err,acc_pi,acc_pi_err};
   ratio(vec_acc);
   cout << vec_acc[0] << "---"<< vec_acc[1] << "---" << vec_acc[2] << "---"<< vec_acc[3] << endl;
   cout << vec_acc[4] << "---" << vec_acc[5] << endl;
//////////////////////////////////////////////////////////////////////////////////////////////////

   vector<double> vec_sel;
   vec_sel.clear();
   cout << sel_eff(vec_sel) << " --- selection efficiency is calculated !"  << endl;
   ratio(vec_sel);
   cout << vec_sel[0] << "---"<< vec_sel[1] << "---" << vec_sel[2] << "---"<< vec_sel[3] << endl;
   cout << vec_sel[4] << "---"<< vec_sel[5] << endl;
//////////////////////////////////////////////////////////////////////////////////////////////////

   vector<double> vec_track;
   vec_track.clear();
   cout << track_eff(vec_track) << " --- track efficiency is calculated !"  << endl;
   ratio(vec_track);
   cout << vec_track[0] << "---" << vec_track[1] << "---" << vec_track[2] << "---" << vec_track[3]  << endl;
   cout << vec_track[4] << "---" << vec_track[5] << endl;
//////////////////////////////////////////////////////////////////////////////////////////////////

   vector<double> vec_trigger;
   vec_trigger.clear();
   cout << trigger_eff(vec_trigger) << " --- trigger efficiency is calculated !"  << endl;
   ratio(vec_trigger);
   cout << vec_trigger[0] << "---" << vec_trigger[1] << "---" << vec_trigger[2] << "---" << vec_trigger[3] << endl;
   cout << vec_trigger[4] << "---" << vec_trigger[5] << endl;
//////////////////////////////////////////////////////////////////////////////////////////////////

   vector<double> vec_pid;
   vec_pid.clear();
   cout << pid_eff(vec_pid) << " --- pid efficiency is calculated !"  << endl;
   ratio(vec_pid);
   cout << vec_pid[0] << "---" << vec_pid[1] << "---" << vec_pid[2] << "---" << vec_pid[3] << endl;
   cout << vec_pid[4] << "---" << vec_pid[5] << endl;
////////////////////////////////////////////////////////////////////////////////////////////////////

   double tis_tot = vec_acc[4]*vec_sel[4]*vec_track[4]*vec_trigger[4]*vec_pid[4];
   //double tos_tot = vec_acc[4]*vec_sel[4]*vec_track[4]*vec_trigger[10]*vec_pid[10];

   cout << "Eff: " << tis_tot << " +-" 
	<< tis_tot * sqrt( pow(vec_acc[5]/vec_acc[4],2) 
			+ pow(vec_sel[5]/vec_sel[4],2) 
			+ pow(vec_track[5]/vec_track[4],2) 
			+ pow(vec_trigger[5]/vec_trigger[4],2) 
			+ pow(vec_pid[5]/vec_pid[4],2) ) << endl;


   TString newTxt = "eff_tot.txt";
   ofstream out(newTxt);

   out << "Eff: " << tis_tot << " +-"
	<< tis_tot * sqrt( pow(vec_acc[5]/vec_acc[4],2)
		  + pow(vec_sel[5]/vec_sel[4],2)
		  + pow(vec_track[5]/vec_track[4],2)
		  + pow(vec_trigger[5]/vec_trigger[4],2)
		  + pow(vec_pid[5]/vec_pid[4],2) ) << endl;

   return 0;
}






