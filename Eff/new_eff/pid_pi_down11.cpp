
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"


void pid_pi_down11( const char* choose ){

//Get The TTree 
   TChain *chain = new TChain("DecayTree");
   TString name = choose;
   TString rootname = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/down11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger.root";
   chain->Add(rootname);
   TTree *NoPID_Tree = (TTree*)chain->CopyTree("");



   TFile p_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_P_Strip21r1_MagDown_default_P_ETA_nTracks.root");
   TFile k_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_K_Strip21r1_MagDown_default_P_ETA_nTracks.root");
   TFile k_lb_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_K_Strip21r1_MagDown_default_P_ETA_nTracks_PID2.root");
   TFile pi_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_Pi_Strip21r1_MagDown_default_P_ETA_nTracks_PID20.root");
   TFile pi_lb_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_Pi_Strip21r1_MagDown_default_P_ETA_nTracks_PID10.root");

//Get the histograms from the files
   TH3F *heff_p_lc;		heff_p_lc = (TH3F*)p_file.Get("P_DLLp>-10_All");
   TH3F *heff_k_lc;		heff_k_lc = (TH3F*)k_file.Get("K_DLLK>-10_All");
   TH3F *heff_k_lb;		heff_k_lb = (TH3F*)k_lb_file.Get("K_DLLK>-2_All");
   TH3F *heff_pi_lc;		heff_pi_lc = (TH3F*)pi_file.Get("Pi_DLLK<20_All");
   TH3F *heff_pi_bach;		heff_pi_bach = (TH3F*)pi_lb_file.Get("Pi_DLLK<10_All");


  //Event loop for efficiency of each event 
  Int_t nentry = NoPID_Tree->GetEntries();
  std::cout << "Number of entries: "<<nentry<<std::endl;
 
  Int_t ntracks;			NoPID_Tree->SetBranchAddress("nTracks",&ntracks); 
  Double_t p_lc_p;		NoPID_Tree->SetBranchAddress("lab2_P",&p_lc_p);
  Double_t p_lc_eta;		NoPID_Tree->SetBranchAddress("p_lc_ETA",&p_lc_eta);
  Double_t k_lc_p; 		NoPID_Tree->SetBranchAddress("lab3_P",&k_lc_p);
  Double_t k_lc_eta;		NoPID_Tree->SetBranchAddress("k_lc_ETA",&k_lc_eta);
  Double_t pi_lc_p; 		NoPID_Tree->SetBranchAddress("lab4_P",&pi_lc_p);
  Double_t pi_lc_eta;		NoPID_Tree->SetBranchAddress("pi_lc_ETA",&pi_lc_eta);
  Double_t pi_bach_p; 		NoPID_Tree->SetBranchAddress("lab5_P",&pi_bach_p);
  Double_t pi_bach_eta;		NoPID_Tree->SetBranchAddress("pi_bach_ETA",&pi_bach_eta);
  Double_t weight_pty; 		NoPID_Tree->SetBranchAddress("weight_pty",&weight_pty);
  Double_t weight_ntracks;	NoPID_Tree->SetBranchAddress("weight_ntracks",&weight_ntracks);
  Double_t total_weight;
  std::cout << "SetBranchAddress successful"<<std::endl;

  Float_t eff_p_lc, eff_k_lc, eff_pi_lc, eff_pi_bach, eff_evt;
  Float_t eff_p_lc_mean, eff_p_bach_mean, eff_pbar_bach_mean, eff_k_lc_mean, eff_pi_bach_mean, eff_pi_lc_mean;
  Float_t eff_tot, eff_ave;
  Int_t binp_p_lc, bineta_p_lc, binp_k_lc, bineta_k_lc, binp_pi_lc,bineta_pi_lc, binp_pi_bach, bineta_pi_bach, binntrk, binp_p_bach, bineta_p_bach, binp_pbar_bach, bineta_pbar_bach;
  Float_t err_p_lc, err_k_lc, err_pi_lc, err_pi_bach, err_evt, err_pbar_bach,err_p_bach;
  Float_t err_tot, err_ave;  
  Int_t bintrk;

  std::cout << "Event loop start: "<< std::endl; 
  

  double eff_mean_p_lc,eff_mean_k_lc,eff_mean_pi_lc,eff_mean_pi_bach,eff_err_p_lc,eff_err_k_lc,eff_err_pi_lc,eff_err_pi_bach,eff_p_lc_ran,eff_k_lc_ran,eff_pi_lc_ran,eff_pi_bach_ran;


  eff_tot = 0.;
  Int_t eff_evtnum = 0;
  err_tot = 0.;


  TString newfile = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/down11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger_pid.root";
  TFile* f1 = new TFile( newfile ,"recreate");

   TTree* newtree = NoPID_Tree->CloneTree(0);
   Double_t pideff;		newtree->Branch("pideff",&pideff,"pideff/D");
   Double_t pideff_err;		newtree->Branch("pideff_err",&pideff_err,"pideff_err/D");
   Double_t pideff2;		newtree->Branch("pideff2",&pideff2,"pideff2/D");
   
   
   for(int j=0;j<nentry;j++){
    // cout << j << "/"<<nentry<<endl;  
     
     NoPID_Tree->GetEntry(j);
     bintrk = heff_p_lc->GetZaxis()->FindBin((float)ntracks);
#if 1
     //cout << p_lc_p << " --  " << p_lc_eta << endl;
     binp_p_lc = heff_p_lc->GetXaxis()->FindBin(p_lc_p);
     bineta_p_lc = heff_p_lc->GetYaxis()->FindBin(p_lc_eta);

     binp_k_lc = heff_k_lc->GetXaxis()->FindBin(k_lc_p);
     bineta_k_lc = heff_k_lc->GetYaxis()->FindBin(k_lc_eta);
	  
     binp_pi_lc = heff_pi_lc->GetXaxis()->FindBin(pi_lc_p);
     bineta_pi_lc = heff_pi_lc->GetYaxis()->FindBin(pi_lc_eta);
	  
     binp_pi_bach = heff_pi_bach->GetXaxis()->FindBin(pi_bach_p);
     bineta_pi_bach = heff_pi_bach->GetYaxis()->FindBin(pi_bach_eta); 


     eff_p_lc = heff_p_lc->GetBinContent(binp_p_lc, bineta_p_lc, bintrk);
     eff_k_lc = heff_k_lc->GetBinContent(binp_k_lc, bineta_k_lc, bintrk);
     eff_pi_lc = heff_pi_lc->GetBinContent(binp_pi_lc, bineta_pi_lc, bintrk);
     eff_pi_bach = heff_pi_bach->GetBinContent(binp_pi_bach, bineta_pi_bach, bintrk);

     err_p_lc = heff_p_lc->GetBinError(binp_p_lc, bineta_p_lc, bintrk);
     err_k_lc = heff_k_lc->GetBinError(binp_k_lc, bineta_k_lc, bintrk);
     err_pi_lc = heff_pi_lc->GetBinError(binp_pi_lc, bineta_pi_lc, bintrk);
     err_pi_bach = heff_pi_bach->GetBinError(binp_pi_bach, bineta_pi_bach, bintrk);

     eff_evt = eff_p_lc*eff_k_lc*eff_pi_lc*eff_pi_bach;
     pideff2 = eff_p_lc*eff_k_lc*eff_pi_lc*eff_pi_bach;
    
//     if(eff_p_lc>0. && eff_k_lc>0. && eff_pi_lc>0. && eff_pi_bach>0. &&eff_evt<1.) eff_evtnum += 1;
   
     if(eff_p_lc>0. && eff_k_lc>0. && eff_pi_lc>0. && eff_pi_bach>0. &&eff_evt<1.) eff_evtnum += 1;
     if(eff_evt<0){
	  pideff = 0;
     }
     else if(eff_evt>1){
	  pideff = 1;
     }
     else{
     pideff = eff_evt;
     }
     
     pideff_err = pideff*sqrt( (TMath::Power(err_p_lc/eff_p_lc,2))
		  	+ (TMath::Power(err_k_lc/eff_k_lc,2))
			+ (TMath::Power(err_pi_lc/eff_pi_lc,2))
			+ (TMath::Power(err_pi_bach/eff_pi_bach,2)) );
     //cout << pideff_err << endl;
     
     newtree->Fill();
#endif
  }

#if 0
  TString newTxt = "/home/xuzh/work/PentaQuark/Lb2Lc/Eff/tot/pid/Lb2Lcpi_PID_down_" + name + ".txt";
  ofstream out(newTxt); 
  if (out.is_open()){ 
     out.clear();
     out << "pid eff: "<< eff_hist->GetMean() << "\n";
     out << "pid eff error: " << eff_hist->GetMeanError() <<"\n";
     out << "No weight pid eff : " << eff_hist_no->GetMean() << "\n";
     out << "No weight pid eff error: : " << eff_hist_no->GetMeanError() <<"\n";
  }
#endif

#if 1
  newtree->Write();
  f1->Close();
#endif

  delete chain;
  delete f1;
}
