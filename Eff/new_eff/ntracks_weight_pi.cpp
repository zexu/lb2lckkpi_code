
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"
void ntracks_weight_pi( const char* choose ){

   TFile* f = new TFile("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcPi/mc/ntrack_weight/ratio_ntracks.root");
   TH2F *h_ratio = (TH2F*)f->Get("nTracks");

   TString name = choose;
   TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty_ntrack.root";
   TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty.root/DecayTree";

   TString oldroot(fileold);
   TString newroot(filenew);

   TChain* tmp = new TChain();
   tmp->Add(oldroot);
   std::cout << tmp->GetEntries() << std::endl;
   Int_t nTrack; 	tmp->SetBranchAddress("nTracks",&nTrack);

   TFile *file = new TFile(newroot,"recreate");
   TTree* newtree_Pi = tmp->CloneTree(0);
   //cout << tmp->GetEntries() << "new tree: "<< newtree_Pi->GetEntries() << endl;

   Double_t weight_ntracks, weight_ntracks_err;
   newtree_Pi->Branch("weight_ntracks",&weight_ntracks,"weight_ntracks/D");
   newtree_Pi->Branch("weight_ntracks_err",&weight_ntracks_err,"weight_ntracks_err/D");

   for(int i=0; i<tmp->GetEntries(); i++){
	tmp->GetEntry(i);

	Int_t bin = h_ratio->FindBin(nTrack);
	weight_ntracks = h_ratio->GetBinContent(bin);
	weight_ntracks_err = h_ratio->GetBinError(bin);
	//std::cout << nTrack << "---" << weight_ntracks << std::endl;
	newtree_Pi->Fill();
   }
   std::cout << newtree_Pi->GetEntries() << " -new tree numbers:" << newtree_Pi->GetEntries() << std::endl;
   newtree_Pi->Write();
   file->Close();
  
   f->Close();
}
