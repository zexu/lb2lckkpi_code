
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

void split_kkpi( const char* choose ){

   TString name = choose;
   TChain *chain = new TChain("DecayTree");
   TString file = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root";
   chain->Add(file);
   std::cout << chain->GetEntries() << std::endl;

   TString tis = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/TIS/Lb2Lckkpi_pre_clone_pty_ntrack_dalitz_poli_tracks_TIS.root";
   TFile *file1 = new TFile(tis,"recreate");
   TTree *newtree1 = chain->CopyTree("Lb_L0Global_TIS==1");
   std::cout << newtree1->GetEntries() << std::endl;
   newtree1->Write();
   file1->Close();
   
   TString tos = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/TOS/Lb2Lckkpi_pre_clone_pty_ntrack_dalitz_poli_tracks_TOS.root";
   TFile *file2 = new TFile(tos,"recreate");
   TTree *newtree2 = chain->CopyTree("(Lb_L0Global_TIS!=1&&Lc_L0HadronDecision_TOS==1)");
   std::cout << newtree2->GetEntries() << std::endl;
   newtree2->Write();
   file2->Close();

}
