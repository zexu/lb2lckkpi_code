//#include"TRatioPlot.h"
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

void dalitz_weight_kkpi( const char* choose ){

   TFile* f = new TFile("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcKKPi/mc/dalitz_weight/ratio_dalitz.root");
   TH2F *h_ratio = (TH2F*) f->Get("dalitz");
   //f->Close();

   TString name = choose;
   TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz.root";
   TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack.root";
   
   TFile *file = new TFile(filenew,"recreate");
   TChain *oldchain = new TChain("DecayTree");
   oldchain->Add(fileold);

   Double_t mkp; 		oldchain->SetBranchAddress("mkp",&mkp);
   Double_t mkpi; 	oldchain->SetBranchAddress("mkpi",&mkpi);

   TTree* newtree = oldchain->CloneTree(0);
   Double_t weight_dalitz; 		newtree->Branch("weight_dalitz",&weight_dalitz,"weight_dalitz/D");
   Double_t weight_dalitz_err;	newtree->Branch("weight_dalitz_err",&weight_dalitz_err,"weight_dalitz_err/D");

   for(int i=0; i<oldchain->GetEntries(); i++){
       
	oldchain->GetEntry(i);

	int bin = h_ratio->FindBin(mkpi,mkp);
	weight_dalitz = h_ratio->GetBinContent(bin);
	weight_dalitz_err = h_ratio->GetBinError(bin);

	newtree->Fill();
   }
   std::cout << oldchain->GetEntries() << " -new tree numbers:" << newtree->GetEntries() << std::endl;
   newtree->Write();
   file->Close();

   f->Close();
}
