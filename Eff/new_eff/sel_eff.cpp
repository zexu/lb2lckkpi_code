#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

#include "sel_eff.h"

using namespace std;
//void sel_eff( vector< double> &vec ){
int sel_eff(vector<double>& vec){

   TChain* tKKPi_tot = new TChain();

   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/genLevel/down11/Lb2Lckkpi_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/genLevel/up11/Lb2Lckkpi_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/genLevel/down12/Lb2Lckkpi_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/genLevel/up12/Lb2Lckkpi_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");

   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/genLevel/down11/Lb2LcKKst_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/genLevel/up11/Lb2LcKKst_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/genLevel/down12/Lb2LcKKst_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/genLevel/up12/Lb2LcKKst_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");

   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/genLevel/down11/Lb2Lca1_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/genLevel/up11/Lb2Lca1_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/genLevel/down12/Lb2Lca1_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tKKPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/genLevel/up12/Lb2Lca1_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");


   TH1F *h1 = new TH1F("h1","",100,0,1000);
   tKKPi_tot->Project("h1","weight","");
   std::cout << "tKKPi_tot : " << h1->GetMean() << std::endl;
   Double_t num_LcKKPi_tot = h1->GetMean()*tKKPi_tot->GetEntries("");
   delete h1;
   
//   cout << "num_LcKKPi_tot: " << num_LcKKPi_tot << endl;

#if 1

   TChain* tKKPi_sel = new TChain();
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tKKPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   
   
   TH1F *h3 = new TH1F("h3","",100,0,1000);
   tKKPi_sel->Project("h3","weight","");
   std::cout << "tKKPi_sel : " << h3->GetMean() << std::endl;
   Double_t num_LcKKPi_sel = h3->GetMean()*tKKPi_sel->GetEntries("");
   delete h3;

//   Int_t num_LcPi_tot = 2079063;

   TChain* tPi_tot = new TChain();

   tPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/genLevel/down11/Lb2LcDs_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/genLevel/up11/Lb2LcDs_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/genLevel/down12/Lb2LcDs_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");
   tPi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/genLevel/up12/Lb2LcDs_pre_pty_ntrack_dalitz_poli.root/MCDecayTree");



   TH1F *h2 = new TH1F("h2","",100,0,1000);
   tPi_tot->Project("h2","weight","");
   std::cout << "tPi_tot : " << h2->GetMean() << std::endl;
   Double_t num_LcPi_tot = tPi_tot->GetEntries("")*h2->GetMean();
   delete h2;
  // num_LcPi_tot = 2079063;
  // std::cout << num_LcPi_tot << std::endl;


   TChain* tPi_sel = new TChain();
   tPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   tPi_sel->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");


   TH1F *h4 = new TH1F("h4","",100,0,1000);
   tPi_sel->Project("h4","weight","");
   std::cout << "tPi_sel : " << h4->GetMean() << std::endl;
   Double_t num_LcPi_sel = tPi_sel->GetEntries("")*h4->GetMean();
   delete h4;

   cout << "lckkpi : " << num_LcKKPi_sel << " --- " << num_LcKKPi_tot << endl;
   cout << "lcds : " << num_LcPi_sel << " --- " << num_LcPi_tot << endl;


   double p1 = double(num_LcKKPi_sel)/num_LcKKPi_tot;
   double e1 = sqrt(p1*(1-p1)/num_LcKKPi_tot);
   //cout << "eff_LcKKPi: " << p1 << " +- " << e1 << endl;

   double p2 = double(num_LcPi_sel)/num_LcPi_tot;
   double e2 = sqrt(p2*(1-p2)/num_LcPi_tot);
  // cout << "eff_LcPi: " << p2 << " +- " << e2 << endl;
  // cout << "relative efficiency: " << p1/p2 << "+-" << p1/p2*sqrt(e1*e1/p1/p1 + e2*e2*p2*p2/p1/p1/p1/p1)  <<endl;


//   ofstream out("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/tot/sel_eff.txt");
//   out.clear();
//   out << "eff_LcKKPi: " << p1 << " +- " << e1 << "\n";
//   out << "eff_LcPi: " << p2 << " +- " << e2 << "\n";
//   out << "relative efficiency: " << p1/p2 << "+-" << p1/p2*sqrt(e1*e1/p1/p1 + e2*e2/p2/p2)  << "\n";
#endif

   vec.push_back(p1);
   vec.push_back(e1);
   vec.push_back(p2);
   vec.push_back(e2);
   vec.push_back(p1/p2);
   vec.push_back(p1/p2*sqrt(e1*e1/p1/p1 + e2*e2/p2/p2));

   return 0;
}







