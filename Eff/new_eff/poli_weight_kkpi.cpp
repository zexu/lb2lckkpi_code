
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

void poli_weight_kkpi ( const char* choose ){

   TFile* f = new TFile("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcKKPi/mc/poli_weight/plot.root");
   TH2F *h_ratio = (TH2F*)f->Get("theta_ratio");

   TString name = choose;
   TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli.root";
   TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz.root/DecayTree";

   TString oldroot(fileold);
   TString newroot(filenew);

   TChain* tmp = new TChain();
   tmp->Add(oldroot);
   Double_t cosTheta; 	tmp->SetBranchAddress("cosTheta",&cosTheta);

   TFile *file = new TFile(newroot,"recreate");
   TTree* newtree_Pi = tmp->CloneTree(0);
   //cout << tmp->GetEntries() << "new tree: "<< newtree_Pi->GetEntries() << endl;

   Double_t weight_poli, weight_poli_err;
   newtree_Pi->Branch("weight_poli",&weight_poli,"weight_poli/D");
   newtree_Pi->Branch("weight_poli_err",&weight_poli_err,"weight_poli_err/D");
   Double_t weight;		newtree_Pi->Branch("weight",&weight,"weight/D");
   Double_t weight_err;		newtree_Pi->Branch("weight_err",&weight_err,"weight_err/D");


   Double_t weight_pty;			tmp->SetBranchAddress("weight_pty",&weight_pty);
   Double_t weight_pty_err;		tmp->SetBranchAddress("weight_pty_err",&weight_pty_err);
   Double_t weight_ntracks;		tmp->SetBranchAddress("weight_ntracks",&weight_ntracks);
   Double_t weight_ntracks_err;	tmp->SetBranchAddress("weight_ntracks_err",&weight_ntracks_err);
   Double_t weight_dalitz;		tmp->SetBranchAddress("weight_dalitz",&weight_dalitz);
   Double_t weight_dalitz_err;		tmp->SetBranchAddress("weight_dalitz_err",&weight_dalitz_err);


   for(int i=0; i<tmp->GetEntries(); i++){
	tmp->GetEntry(i);

	Int_t bin = h_ratio->FindBin(cosTheta);
	weight_poli = h_ratio->GetBinContent(bin);
	weight_poli_err = h_ratio->GetBinError(bin);
	//cout << nTrack << "---" << weight_ntracks << endl;

	weight = weight_pty*weight_ntracks*weight_dalitz*weight_poli;
	weight_err = weight*sqrt( pow( weight_pty_err/weight_pty , 2 ) 
					+ pow( weight_ntracks_err/weight_ntracks , 2 )
					+ pow( weight_dalitz_err/weight_dalitz , 2 )
					+ pow( weight_poli_err/weight_poli , 2 ) );
	
	newtree_Pi->Fill();
   }
   std::cout << tmp->GetEntries() << " -new tree numbers:" << newtree_Pi->GetEntries() << std::endl;
   newtree_Pi->Write();
   file->Close();

   f->Close();
}
