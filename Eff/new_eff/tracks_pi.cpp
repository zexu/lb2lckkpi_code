#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TH2D.h"
#include "TH1.h"

using namespace std;

void tracks_pi( const char* choose) {
//chain the root files 
	TFile f_track("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcPi/mc/track/ratio2012S20.root");
	TH2D *ratio;
	ratio = (TH2D*)f_track.Get("Ratio");
	
	TString name = choose;
	TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks.root";
      TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/sys_mc/"+name+"/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli.root";

	TFile *file = new TFile(filenew,"recreate");
	TChain *chain = new TChain("DecayTree");
	chain->Add(fileold);
	int nentries = chain->GetEntries();
//define the object

	Double_t lab2_P;		chain->SetBranchAddress("lab2_P",&lab2_P);
	Double_t p_lc_ETA;	chain->SetBranchAddress("p_lc_ETA",&p_lc_ETA);
	Double_t lab3_P;		chain->SetBranchAddress("lab3_P",&lab3_P);
	Double_t k_lc_ETA;	chain->SetBranchAddress("k_lc_ETA",&k_lc_ETA);
	Double_t lab4_P;		chain->SetBranchAddress("lab4_P",&lab4_P);
	Double_t pi_lc_ETA;	chain->SetBranchAddress("pi_lc_ETA",&pi_lc_ETA);
	Double_t lab5_P;		chain->SetBranchAddress("lab5_P",&lab5_P);
	Double_t pi_bach_ETA;	chain->SetBranchAddress("pi_bach_ETA",&pi_bach_ETA);


#if 1
//create a new file and clone a tree, but do not copy entries.
	TTree *newtree = chain->CloneTree(0);
#endif

	Double_t LcP_track;	newtree->Branch("LcP_track",&LcP_track,"LcP_track/D");
	Double_t LcK_track;	newtree->Branch("LcK_track",&LcK_track,"LcK_track/D");
	Double_t LcPi_track;	newtree->Branch("LcPi_track",&LcPi_track,"LcPi_track/D");
	Double_t LbPi_track;	newtree->Branch("LbPi_track",&LbPi_track,"LbPi_track/D");
	
	Double_t LcP_track_err;	newtree->Branch("LcP_track_err",&LcP_track_err,"LcP_track_err/D");
	Double_t LcK_track_err;	newtree->Branch("LcK_track_err",&LcK_track_err,"LcK_track_err/D");
	Double_t LcPi_track_err;	newtree->Branch("LcPi_track_err",&LcPi_track_err,"LcPi_track_err/D");
	Double_t LbPi_track_err;	newtree->Branch("LbPi_track_err",&LbPi_track_err,"LbPi_track_err/D");
	
	Double_t rtrack;		newtree->Branch("rtrack",&rtrack,"rtrack/D");
	Double_t rtrack_err;	newtree->Branch("rtrack_err",&rtrack_err,"rtrack_err/D");


	Int_t pbin;
	Int_t etabin;

#if 1
	//for (Long64_t jentry=0; jentry<nentries; jentry++){
	for (Long64_t jentry=0; jentry<nentries; jentry++){

		chain->GetEntry(jentry);
		//if (jentry%100==0) cout << "jentry = " << jentry << endl;

		pbin = ratio->GetXaxis()->FindBin(lab2_P/1000);
		etabin = ratio->GetYaxis()->FindBin(p_lc_ETA);
		LcP_track = ratio->GetBinContent(pbin,etabin);
		LcP_track_err = ratio->GetBinError(pbin,etabin);
//		cout << pbin << "---" << etabin << endl;

		pbin = ratio->GetXaxis()->FindBin(lab3_P/1000);
		etabin = ratio->GetYaxis()->FindBin(k_lc_ETA);
		LcK_track = ratio->GetBinContent(pbin,etabin);
		LcK_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(lab4_P/1000);
		etabin = ratio->GetYaxis()->FindBin(pi_lc_ETA);
		LcPi_track = ratio->GetBinContent(pbin,etabin);
		LcPi_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(lab5_P/1000);
		etabin = ratio->GetYaxis()->FindBin(pi_bach_ETA);
		LbPi_track = ratio->GetBinContent(pbin,etabin);
		LbPi_track_err = ratio->GetBinError(pbin,etabin);
	
		rtrack = LcP_track * LcK_track * LcPi_track * LbPi_track ;
		rtrack_err =sqrt( (pow(LcP_track_err/LcP_track,2)	
		   		+pow(LcK_track_err/LcK_track,2)
		   		+pow(LcPi_track_err/LcPi_track,2)
		   		+pow(LbPi_track_err/LbPi_track,2) )*pow(rtrack,2));

		newtree->Fill();
	
	}	
	std::cout << "new number: " << newtree->GetEntries() << std::endl;
	newtree->Write();
      file->Close();
#endif
}

	





