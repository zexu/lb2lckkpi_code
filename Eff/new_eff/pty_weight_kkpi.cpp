//#include"TRatioPlot.h"
#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

void pty_weight_kkpi( const char* choose ){

   TFile* f = new TFile("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcPi/mc/pty_weight/ratio_pty.root");
  // TFile* f = new TFile("/home/xuzh/work/PentaQuark/Lb2Lc/system_error/pty/rerange/weight_plot/pty_rerange.root");
   TH2F *h_ratio = (TH2F*)f->Get("pty");
   //f->Close();

   TString name = choose;
   TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty.root";
   TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG.root";
   
   TFile *file = new TFile(filenew,"recreate");
   TChain *oldchain = new TChain("DecayTree");
   oldchain->Add(fileold);

   Double_t Lb_P;	oldchain->SetBranchAddress("Lb_P",&Lb_P);
   Double_t Lb_PE;	oldchain->SetBranchAddress("Lb_PE",&Lb_PE);
   Double_t Lb_PT;	oldchain->SetBranchAddress("Lb_PT",&Lb_PT);


   TTree* newtree = oldchain->CloneTree(0);
   Double_t weight_pty; 		newtree->Branch("weight_pty",&weight_pty,"weight_pty/D");
   Double_t weight_pty_err;		newtree->Branch("weight_pty_err",&weight_pty_err,"weight_pty_err/D");

   Int_t bin;
   for(int i=0; i<oldchain->GetEntries(); i++){
       
	oldchain->GetEntry(i);
	bin = h_ratio->FindBin(Lb_PT,(0.5*TMath::Log((Lb_PE+Lb_P)/(Lb_PE-Lb_P))));
	weight_pty = h_ratio->GetBinContent(bin);
	weight_pty_err = h_ratio->GetBinError(bin);

	newtree->Fill();
   }
   std::cout << newtree->GetEntries() << " -new tree numbers:" << newtree->GetEntries() << std::endl;
   
   newtree->Write();
   file->Close(); 
   f->Close();

   delete f;
   delete file;
   delete oldchain;
}
