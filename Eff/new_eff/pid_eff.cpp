#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"

#include "track_eff.h"
#include "pid_eff.h"

double pideff_err[2];

void ratio2(int x, int y){

   TH1F *h1 = new TH1F("h1","",1,0,1);
   h1->SetBinContent(1,x);
   h1->SetBinError(1,TMath::Sqrt(x));
   TH1F *h2 = new TH1F("h2","",1,0,1);
   h2->SetBinContent(1,y);
   h2->SetBinError(1,TMath::Sqrt(y));
   TH1F *h3 = new TH1F("h3","",1,0,1);
   h3->Divide(h1,h2);
   pideff_err[0] = h3->GetBinContent(1);
   pideff_err[1] = h3->GetBinError(1);

   delete h1;
   delete h2;
   delete h3;
}


using namespace std;
//void sel_eff( vector< double> &vec ){
int pid_eff(vector<double>& vec){

   TChain* kkpi_tis = new TChain();
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");




   TH1F *kkpi_tis_up = new TH1F("kkpi_tis_up","",10,-5000,5000);
   kkpi_tis->Project("kkpi_tis_up","weight*pideff");
   TH1F *kkpi_tis_down = new TH1F("kkpi_tis_down","",10,-5000,5000);
   kkpi_tis->Project("kkpi_tis_down","weight");
   TH1F *kkpi_tis_up_error = new TH1F("kkpi_tis_up_error","",10,-5000,5000);
   kkpi_tis->Project("kkpi_tis_up_error","weight*pideff*weight*(1-pideff)");
 
   ratio2(kkpi_tis_up->GetMean()*kkpi_tis_up->GetEntries(),kkpi_tis_down->GetMean()*kkpi_tis_down->GetEntries());
   vec.push_back(pideff_err[0]);
   ratio2(sqrt(kkpi_tis_up_error->GetMean()*kkpi_tis_up_error->GetEntries()),kkpi_tis_down->GetMean()*kkpi_tis_down->GetEntries());
   vec.push_back(pideff_err[0]);


   TChain* pi_tis = new TChain();
   pi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tis->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   TH1F *pi_tis_up = new TH1F("pi_tis_up","",10,-5000,5000);
   pi_tis->Project("pi_tis_up","weight*pideff");
   TH1F *pi_tis_down = new TH1F("pi_tis_down","",10,-5000,5000);
   pi_tis->Project("pi_tis_down","weight");
   TH1F *pi_tis_up_error = new TH1F("pi_tis_up_error","",10,-5000,5000);
   pi_tis->Project("pi_tis_up_error","weight*pideff*weight*(1-pideff)");

   ratio2(pi_tis_up->GetMean()*pi_tis_up->GetEntries(),pi_tis_down->GetMean()*pi_tis_down->GetEntries());
   vec.push_back(pideff_err[0]);
   ratio2(sqrt(pi_tis_up_error->GetMean()*pi_tis_up_error->GetEntries()),pi_tis_down->GetMean()*pi_tis_down->GetEntries());
   vec.push_back(pideff_err[0]);


#if 0
   TChain* kkpi_tos = new TChain();
   kkpi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   kkpi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   kkpi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   kkpi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");

   TH1F *kkpi_tos_up = new TH1F("kkpi_tos_up","",10,-50,50);
   kkpi_tos->Project("kkpi_tos_up","weight*pideff");
   TH1F *kkpi_tos_down = new TH1F("kkpi_tos_down","",10,-50,50);
   kkpi_tos->Project("kkpi_tos_down","weight");
   ratio2(kkpi_tos_up->GetMean()*kkpi_tos_up->GetEntries(),kkpi_tos_down->GetMean()*kkpi_tos_down->GetEntries());
   vec.push_back(pideff_err[0]);
   vec.push_back(pideff_err[1]);


   TChain* pi_tos = new TChain();
   pi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   pi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up11/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   pi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/down12/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");
   pi_tos->Add("/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lcpi/mc/up12/Lb2Lcpi_pre_clone_pty_ntrack_dalitz_poli_tracks_trigger_pid.root/DecayTree");

   TH1F *pi_tos_up = new TH1F("pi_tos_up","",10,-50,50);
   pi_tos->Project("pi_tos_up","weight*pideff");
   TH1F *pi_tos_down = new TH1F("pi_tos_down","",10,-50,50);
   pi_tos->Project("pi_tos_down","weight");
   ratio2(pi_tos_up->GetMean()*pi_tos_up->GetEntries(),pi_tos_down->GetMean()*pi_tos_down->GetEntries());
   vec.push_back(pideff_err[0]);
   vec.push_back(pideff_err[1]);
#endif

   return 0;
}







