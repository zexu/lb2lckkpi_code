#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"
#include "TCanvas.h"
#include "TLegend.h"

#include "trigger_eff.h"

double trieff_err[2];

void ratio3(int x, int y){

   TH1F *h1 = new TH1F("h1","",1,0,1);
   h1->SetBinContent(1,x);
   h1->SetBinError(1,TMath::Sqrt(x));
   TH1F *h2 = new TH1F("h2","",1,0,1);
   h2->SetBinContent(1,y);
   h2->SetBinError(1,TMath::Sqrt(y));
   TH1F *h3 = new TH1F("h3","",1,0,1);
   h3->Divide(h1,h2);
   trieff_err[0] = h3->GetBinContent(1);
   trieff_err[1] = h3->GetBinError(1);

   delete h1;
   delete h2;
   delete h3;
}



using namespace std;
//void sel_eff( vector< double> &vec ){
int trigger_eff(vector<double>& vec){


   TChain* kkpi_tot = new TChain();
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up11/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/down12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lckkpi/mc/up12/Lb2Lckkpi_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up11/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/down12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcKKst/mc/up12/Lb2LcKKst_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up11/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/down12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   kkpi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2Lca1/mc/up12/Lb2Lca1_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");


   TH1F *kkpi_up_tis = new TH1F("kkpi_up_tis","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up_tis","weight*tos_eff");
   TH1F *kkpi_down_tis = new TH1F("kkpi_down_tis","",10,-5000,5000);
   kkpi_tot->Project("kkpi_down_tis","weight");
   TH1F *kkpi_up_tis_error = new TH1F("kkpi_up_tis_error","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up_tis_error","weight*tos_eff*weight*(1-tos_eff)");
   
   ratio3(kkpi_up_tis->GetMean()*kkpi_up_tis->GetEntries(),kkpi_down_tis->GetMean()*kkpi_down_tis->GetEntries());
   vec.push_back(trieff_err[0]);
   ratio3(sqrt(kkpi_up_tis_error->GetMean()*kkpi_up_tis_error->GetEntries()),kkpi_down_tis->GetMean()*kkpi_down_tis->GetEntries());
   //vec.push_back(trieff_err[0]);

   double over = kkpi_up_tis->GetMean()*kkpi_up_tis->GetEntries();
   double under = kkpi_down_tis->GetMean()*kkpi_down_tis->GetEntries();
   vec.push_back(1./under*sqrt(over*(1-over/under)));

   std::cout << "kkpi_up" << over 
	<< "  kkpi_down: " << under << std::endl;


   TChain* pi_tot = new TChain();
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up11/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/down12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");
   pi_tot->Add("/home/xuzh/work/PentaQuark/New_Lb2Lc/RootFiles/Lb2LcDs/mc/up12/Lb2LcDs_pre_clone_BDTG_tracks_trigger_pid_pty_ntrack_dalitz_poli.root/DecayTree");

   TH1F *pi_up_tis = new TH1F("pi_up_tis","",10,-5000,5000);
   pi_tot->Project("pi_up_tis","weight*tos_eff");
   TH1F *pi_down_tis = new TH1F("pi_down_tis","",10,-5000,5000);
   pi_tot->Project("pi_down_tis","weight");
   TH1F *pi_up_tis_error = new TH1F("pi_up_tis_error","",10,-5000,5000);
   pi_tot->Project("pi_up_tis_error","weight*tos_eff*weight*(1-tos_eff)");
  
   ratio3(pi_up_tis->GetMean()*pi_up_tis->GetEntries(),pi_down_tis->GetMean()*pi_down_tis->GetEntries());
   vec.push_back(trieff_err[0]);
   ratio3(sqrt(pi_up_tis_error->GetMean()*pi_up_tis_error->GetEntries()),pi_down_tis->GetMean()*pi_down_tis->GetEntries());
   //vec.push_back(trieff_err[0]);

   over = pi_up_tis->GetMean()*pi_up_tis->GetEntries();
   under = pi_down_tis->GetMean()*pi_down_tis->GetEntries();
   vec.push_back(1./under*sqrt(over*(1-over/under)));

   std::cout << "pi_up" << over 
	<< "  pi_down: " << under << std::endl;
   
   TH1F *kkpi_up_tos = new TH1F("kkpi_up_tos","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up_tos","weight*tos_eff");
   TH1F *kkpi_down_tos = new TH1F("kkpi_down_tos","",10,-5000,5000);
   kkpi_tot->Project("kkpi_down_tos","weight");
   TH1F *kkpi_up_tos_error = new TH1F("kkpi_up_tos_error","",10,-5000,5000);
   kkpi_tot->Project("kkpi_up_tos_error","weight*tos_eff*weight*(1-tos_eff)");
   
//   ratio3(kkpi_up_tos->GetMean()*kkpi_up_tos->GetEntries(),kkpi_down_tos->GetMean()*kkpi_down_tos->GetEntries());
//   vec.push_back(trieff_err[0]);
//   ratio3(sqrt(kkpi_up_tos_error->GetMean()*kkpi_up_tos_error->GetEntries()),kkpi_down_tos->GetMean()*kkpi_down_tos->GetEntries());
//   vec.push_back(trieff_err[0]);
   
   
   TH1F *pi_up_tos = new TH1F("pi_up_tos","",10,-5000,5000);
   pi_tot->Project("pi_up_tos","weight*tos_eff");
   TH1F *pi_down_tos = new TH1F("pi_down_tos","",10,-5000,5000);
   pi_tot->Project("pi_down_tos","weight");
   TH1F *pi_up_tos_error = new TH1F("pi_up_tos_error","",10,-5000,5000);
   pi_tot->Project("pi_up_tos_error","weight*tos_eff*weight*(1-tos_eff)");
   
//   ratio3(pi_up_tos->GetMean()*pi_up_tos->GetEntries(),pi_down_tos->GetMean()*pi_down_tos->GetEntries());
//   vec.push_back(trieff_err[0]);
//   ratio3(sqrt(pi_up_tos_error->GetMean()*pi_up_tos_error->GetEntries()),pi_down_tos->GetMean()*pi_down_tos->GetEntries());
//   vec.push_back(trieff_err[0]);

   delete kkpi_tot;
   delete pi_tot;

   delete kkpi_up_tis;
   delete kkpi_down_tis;
   delete pi_up_tis;
   delete pi_down_tis;
   delete kkpi_down_tos;
   delete kkpi_up_tos;
   delete pi_down_tos;
   delete pi_up_tos;
   
   
   return 0;
}







