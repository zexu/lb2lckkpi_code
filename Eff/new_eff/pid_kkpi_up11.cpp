

#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TRandom.h"
void pid_kkpi_up11( const char* choose ){

//Get The TTree 
   TChain *chain = new TChain("DecayTree");
   TString name = choose;
   TString rootname = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/up11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger.root";
   chain->Add(rootname);
   TTree *NoPID_Tree = (TTree*)chain->CopyTree("");

   TTree* newtree = NoPID_Tree->CloneTree(0);
   Double_t pideff;	newtree->Branch("pideff",&pideff,"pideff/D");
   Double_t pideff_err;	newtree->Branch("pideff_err",&pideff_err,"pideff_err/D");
   Double_t pideff2;	newtree->Branch("pideff2",&pideff2,"pideff2/D");


   TFile p_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_P_Strip21r1_MagUp_default_P_ETA_nTracks.root");
   TFile k_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_K_Strip21r1_MagUp_default_P_ETA_nTracks.root");
   TFile k_lb_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_K_Strip21r1_MagUp_default_P_ETA_nTracks_PID2.root");
   TFile pi_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_Pi_Strip21r1_MagUp_default_P_ETA_nTracks_PID20.root");
   TFile pi_lb_file("/home/xuzh/work/PentaQuark/Lb2Lc/Eff/PID/new/UraniaDev_v7r0/version2_11/PerfHists_Pi_Strip21r1_MagUp_default_P_ETA_nTracks_PID10.root");


//Get the histograms from the files
   TH3F *heff_p_lc;		heff_p_lc = (TH3F*)p_file.Get("P_DLLp>-10_All");
   TH3F *heff_k_lc;		heff_k_lc = (TH3F*)k_file.Get("K_DLLK>-10_All");
   TH3F *heff_k_bach;		heff_k_bach = (TH3F*)k_lb_file.Get("K_DLLK>-2_All");
   TH3F *heff_pi_lc;		heff_pi_lc = (TH3F*)pi_file.Get("Pi_DLLK<20_All");
   TH3F *heff_pi_bach;		heff_pi_bach = (TH3F*)pi_lb_file.Get("Pi_DLLK<10_All");


  //Event loop for efficiency of each event 
  Int_t nentry = NoPID_Tree->GetEntries();
  std::cout << "Number of entries: "<<nentry<<std::endl;
 
  Int_t ntracks;			NoPID_Tree->SetBranchAddress("nTracks",&ntracks); 
  Double_t p_lc_p;		NoPID_Tree->SetBranchAddress("LcP_P",&p_lc_p);
  Double_t p_lc_eta;		NoPID_Tree->SetBranchAddress("LcP_ETA",&p_lc_eta);
  Double_t k_lc_p; 		NoPID_Tree->SetBranchAddress("LcK_P",&k_lc_p);
  Double_t k_lc_eta;		NoPID_Tree->SetBranchAddress("LcK_ETA",&k_lc_eta);
  Double_t pi_lc_p; 		NoPID_Tree->SetBranchAddress("LcPi_P",&pi_lc_p);
  Double_t pi_lc_eta;		NoPID_Tree->SetBranchAddress("LcPi_ETA",&pi_lc_eta);
  Double_t pi_bach_p; 		NoPID_Tree->SetBranchAddress("LbPi_P",&pi_bach_p);
  Double_t pi_bach_eta;		NoPID_Tree->SetBranchAddress("LbPi_ETA",&pi_bach_eta);
  Double_t kp_bach_p; 		NoPID_Tree->SetBranchAddress("LbKp_P",&kp_bach_p);
  Double_t kp_bach_eta;		NoPID_Tree->SetBranchAddress("LbKp_ETA",&kp_bach_eta);
  Double_t km_bach_p; 		NoPID_Tree->SetBranchAddress("LbKm_P",&km_bach_p);
  Double_t km_bach_eta;		NoPID_Tree->SetBranchAddress("LbKm_ETA",&km_bach_eta);
  Double_t weight_pty; 		NoPID_Tree->SetBranchAddress("weight_pty",&weight_pty);
  Double_t weight_ntracks;	NoPID_Tree->SetBranchAddress("weight_ntracks",&weight_ntracks);
  Double_t total_weight;
  std::cout << "SetBranchAddress successful"<<std::endl;

  Float_t eff_p_lc, eff_k_lc, eff_pi_lc, eff_pi_bach, eff_kp_bach, eff_km_bach, eff_evt;
  Float_t eff_p_lc_mean, eff_p_bach_mean, eff_pbar_bach_mean, eff_k_lc_mean, eff_pi_bach_mean, eff_pi_lc_mean;
  Float_t eff_tot, eff_ave;
  Int_t binp_p_lc, bineta_p_lc, binp_k_lc, bineta_k_lc, binp_pi_lc,bineta_pi_lc, binp_pi_bach, bineta_pi_bach, binp_kp_bach, bineta_kp_bach, binp_km_bach, bineta_km_bach, binntrk, binp_p_bach, bineta_p_bach, binp_pbar_bach, bineta_pbar_bach;
  Float_t err_p_lc, err_k_lc, err_pi_lc, err_pi_bach, err_evt, err_kp_bach,err_km_bach;
  Float_t err_tot, err_ave;  
  Int_t bintrk;

  std::cout << "Event loop start: "<<std::endl; 
  

  double eff_mean_p_lc,eff_mean_k_lc,eff_mean_pi_lc,eff_mean_pi_bach,eff_err_p_lc,eff_err_k_lc,eff_err_pi_lc,eff_err_pi_bach,eff_p_lc_ran,eff_k_lc_ran,eff_pi_lc_ran,eff_pi_bach_ran;


  eff_tot = 0.;
  Int_t eff_evtnum = 0;
  err_tot = 0.;


  for(int j=0;j<nentry;j++){
     //cout << j << "/"<<nentry<<endl;  
     
     NoPID_Tree->GetEntry(j);
     bintrk = heff_p_lc->GetZaxis()->FindBin((float)ntracks);
#if 1
     binp_p_lc = heff_p_lc->GetXaxis()->FindBin(p_lc_p);
     bineta_p_lc = heff_p_lc->GetYaxis()->FindBin(p_lc_eta);

     binp_k_lc = heff_k_lc->GetXaxis()->FindBin(k_lc_p);
     bineta_k_lc = heff_k_lc->GetYaxis()->FindBin(k_lc_eta);
	  
     binp_pi_lc = heff_pi_lc->GetXaxis()->FindBin(pi_lc_p);
     bineta_pi_lc = heff_pi_lc->GetYaxis()->FindBin(pi_lc_eta);
	  
     binp_pi_bach = heff_pi_bach->GetXaxis()->FindBin(pi_bach_p);
     bineta_pi_bach = heff_pi_bach->GetYaxis()->FindBin(pi_bach_eta); 

     binp_kp_bach = heff_k_bach->GetXaxis()->FindBin(kp_bach_p);
     bineta_kp_bach = heff_k_bach->GetYaxis()->FindBin(kp_bach_eta); 

     binp_km_bach = heff_k_bach->GetXaxis()->FindBin(km_bach_p);
     bineta_km_bach = heff_k_bach->GetYaxis()->FindBin(km_bach_eta); 

     eff_p_lc = heff_p_lc->GetBinContent(binp_p_lc, bineta_p_lc, bintrk);
     eff_k_lc = heff_k_lc->GetBinContent(binp_k_lc, bineta_k_lc, bintrk);
     eff_pi_lc = heff_pi_lc->GetBinContent(binp_pi_lc, bineta_pi_lc, bintrk);
     eff_pi_bach = heff_pi_bach->GetBinContent(binp_pi_bach, bineta_pi_bach, bintrk);
     eff_kp_bach = heff_k_bach->GetBinContent(binp_kp_bach, bineta_kp_bach, bintrk);
     eff_km_bach = heff_k_bach->GetBinContent(binp_km_bach, bineta_km_bach, bintrk);

     err_p_lc = heff_p_lc->GetBinError(binp_p_lc, bineta_p_lc, bintrk);
     err_k_lc = heff_k_lc->GetBinError(binp_k_lc, bineta_k_lc, bintrk);
     err_pi_lc = heff_pi_lc->GetBinError(binp_pi_lc, bineta_pi_lc, bintrk);
     err_pi_bach = heff_pi_bach->GetBinError(binp_pi_bach, bineta_pi_bach, bintrk);
     err_kp_bach = heff_k_bach->GetBinError(binp_kp_bach, bineta_kp_bach, bintrk);
     err_km_bach = heff_k_bach->GetBinError(binp_km_bach, bineta_km_bach, bintrk);

     eff_evt = eff_p_lc*eff_k_lc*eff_pi_lc*eff_pi_bach*eff_kp_bach*eff_km_bach;
     pideff2 = eff_p_lc*eff_k_lc*eff_pi_lc*eff_pi_bach*eff_kp_bach*eff_km_bach;
    
     pideff_err = sqrt( (pow(err_p_lc/eff_p_lc,2))
		  	+ (pow(err_k_lc/eff_k_lc,2))
			+ (pow(err_pi_lc/eff_pi_lc,2))
			+ (pow(err_pi_bach/eff_pi_bach,2)) 
			+ (pow(err_kp_bach/eff_kp_bach,2)) 
			+ (pow(err_km_bach/eff_km_bach,2)) );
//     if(eff_p_lc>0. && eff_k_lc>0. && eff_pi_lc>0. && eff_pi_bach>0. &&eff_evt<1.) eff_evtnum += 1;
   
     if(eff_p_lc>0. && eff_k_lc>0. && eff_pi_lc>0. && eff_pi_bach>0.&& eff_kp_bach>0. && eff_km_bach>0. &&eff_evt<1.) eff_evtnum += 1;
     if(eff_evt<0){
	  pideff = 0;
     }
     else if(eff_evt>1){
	  pideff = 1;
     }
     else{
     pideff = eff_evt;
     }
     newtree->Fill();
#endif
  }

#if 0
  TString newTxt = "/home/xuzh/work/PentaQuark/Lb2Lc/Eff/tot/pid/Lb2Lckkpi_PID_up_" + name + ".txt";
  ofstream out(newTxt); 
  if (out.is_open()){ 
     out.clear();
     out << "pid eff: "<< eff_hist->GetMean() << "\n";
     out << "pid eff error: " << eff_hist->GetMeanError() <<"\n";
     out << "No weight pid eff : " << eff_hist_no->GetMean() << "\n";
     out << "No weight pid eff error: : " << eff_hist_no->GetMeanError() <<"\n";
  }
#endif

#if 1
  TString newfile = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/up11/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks_trigger_pid.root";
  TFile* f1 = new TFile( newfile ,"recreate");
  newtree->Write();
  f1->Close();

#endif

  delete chain;
  delete f1;
}
