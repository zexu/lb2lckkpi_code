#include <TChain.h>
#include <TString.h>
#include <TH1I.h>
#include <TFile.h>
#include <TROOT.h>
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include "TH2D.h"
#include "TH1.h"

using namespace std;

void tracks_kkpi( const char* choose ) {
//chain the root files 
	TFile f_track("/home/xuzh/work/PentaQuark/Lb2Lc/select/Lb2LcKKPi/mc/track/ratio2012S20.root");
	TH2D *ratio;
	ratio = (TH2D*)f_track.Get("Ratio");
	
	
	TString name = choose;
	TString filenew = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli_tracks.root";
      TString fileold = "/home/xuzh/work/PentaQuark/Lb2Lc/RootFiles/Lb2Lckkpi/sys_mc/"+name+"/Lb2Lckkpi_pre_clone_BDTG_pty_ntrack_dalitz_poli.root";

	TFile *file = new TFile(filenew,"recreate");
	TChain *chain = new TChain("DecayTree");
	chain->Add(fileold);
//define the object

	Double_t LcP_P;		chain->SetBranchAddress("LcP_P",&LcP_P);
	Double_t LcP_ETA;		chain->SetBranchAddress("LcP_ETA",&LcP_ETA);
	Double_t LcK_P;		chain->SetBranchAddress("LcK_P",&LcK_P);
	Double_t LcK_ETA;		chain->SetBranchAddress("LcK_ETA",&LcK_ETA);
	Double_t LcPi_P;		chain->SetBranchAddress("LcPi_P",&LcPi_P);
	Double_t LcPi_ETA;	chain->SetBranchAddress("LcPi_ETA",&LcPi_ETA);
	Double_t LbKp_P;		chain->SetBranchAddress("LbKp_P",&LbKp_P);
	Double_t LbKp_ETA;	chain->SetBranchAddress("LbKp_ETA",&LbKp_ETA);
	Double_t LbKm_P;		chain->SetBranchAddress("LbKm_P",&LbKm_P);
	Double_t LbKm_ETA;	chain->SetBranchAddress("LbKm_ETA",&LbKm_ETA);
	Double_t LbPi_P;		chain->SetBranchAddress("LbPi_P",&LbPi_P);
	Double_t LbPi_ETA;	chain->SetBranchAddress("LbPi_ETA",&LbPi_ETA);



#if 1
//create a new file and clone a tree, but do not copy entries.
	Long64_t nentries = chain->GetEntries();
	TTree *newtree = chain->CloneTree(0);
#endif

	Double_t LcP_track;	newtree->Branch("LcP_track",&LcP_track,"LcP_track/D");
	Double_t LcK_track;	newtree->Branch("LcK_track",&LcK_track,"LcK_track/D");
	Double_t LcPi_track;	newtree->Branch("LcPi_track",&LcPi_track,"LcPi_track/D");
	Double_t LbKp_track;	newtree->Branch("LbKp_track",&LbKp_track,"LbKp_track/D");
	Double_t LbKm_track;	newtree->Branch("LbKm_track",&LbKm_track,"LbKm_track/D");
	Double_t LbPi_track;	newtree->Branch("LbPi_track",&LbPi_track,"LbPi_track/D");
	
	Double_t LcP_track_err;	newtree->Branch("LcP_track_err",&LcP_track_err,"LcP_track_err/D");
	Double_t LcK_track_err;	newtree->Branch("LcK_track_err",&LcK_track_err,"LcK_track_err/D");
	Double_t LcPi_track_err;	newtree->Branch("LcPi_track_err",&LcPi_track_err,"LcPi_track_err/D");
	Double_t LbKp_track_err;	newtree->Branch("LbKp_track_err",&LbKp_track_err,"LbKp_track_err/D");
	Double_t LbKm_track_err;	newtree->Branch("LbKm_track_err",&LbKm_track_err,"LbKm_track_err/D");
	Double_t LbPi_track_err;	newtree->Branch("LbPi_track_err",&LbPi_track_err,"LbPi_track_err/D");
	
	Double_t rtrack;		newtree->Branch("rtrack",&rtrack,"rtrack/D");
	Double_t rtrack_err;	newtree->Branch("rtrack_err",&rtrack_err,"rtrack_err/D");

	Int_t pbin;
	Int_t etabin;

#if 1
	//for (Long64_t jentry=0; jentry<nentries; jentry++){
	for (Long64_t jentry=0; jentry<nentries; jentry++){

		chain->GetEntry(jentry);
//		if (jentry%100==0) cout << "jentry = " << jentry << endl;

		pbin = ratio->GetXaxis()->FindBin(LcP_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LcP_ETA);
		LcP_track = ratio->GetBinContent(pbin,etabin);
		LcP_track_err = ratio->GetBinError(pbin,etabin);
		//cout << pbin << "---" << etabin << endl;

		pbin = ratio->GetXaxis()->FindBin(LcK_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LcK_ETA);
		LcK_track = ratio->GetBinContent(pbin,etabin);
		LcK_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(LcPi_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LcPi_ETA);
		LcPi_track = ratio->GetBinContent(pbin,etabin);
		LcPi_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(LbKp_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LbKp_ETA);
		LbKp_track = ratio->GetBinContent(pbin,etabin);
		LbKp_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(LbKm_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LbKm_ETA);
		LbKm_track = ratio->GetBinContent(pbin,etabin);
		LbKm_track_err = ratio->GetBinError(pbin,etabin);

		pbin = ratio->GetXaxis()->FindBin(LbPi_P/1000);
		etabin = ratio->GetYaxis()->FindBin(LbPi_ETA);
		LbPi_track = ratio->GetBinContent(pbin,etabin);
		LbPi_track_err = ratio->GetBinError(pbin,etabin);
	
		rtrack = LcP_track * LcK_track * LcPi_track * LbKp_track * LbKm_track * LbPi_track ;
		rtrack_err =sqrt( (pow(LcP_track_err/LcP_track,2)	
		   		+pow(LcK_track_err/LcK_track,2)
		   		+pow(LcPi_track_err/LcPi_track,2)
		   		+pow(LbKp_track_err/LbKp_track,2)
		   		+pow(LbKm_track_err/LbKm_track,2)
		   		+pow(LbPi_track_err/LbPi_track,2) )*pow(rtrack,2));

		//if(t.BDTG>-0.2){
		   newtree->Fill();
		//}
	
	}	
	cout << "new number: " << newtree->GetEntries() << endl;
	newtree->Write();
      file->Close();
#endif
}

	





